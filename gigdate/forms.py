# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta
from common.models import Country, City, Genre
from account.models import ArtistProfile, ClubProfile
from gigdate.models import GigDate, GigBarter, GigTender


class GigDateForm(forms.ModelForm):
    """
    Форма Добавления гигдаты
    """
    secondary_data              = forms.BooleanField(required=False)
    dates                       = forms.DateField(initial=datetime.now()+timedelta(days=5), input_formats=('%m/%d/%Y',), widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}))
    second_date                 = forms.DateField(initial=datetime.now()+timedelta(days=10), required=False, input_formats=('%m/%d/%Y',), widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}))
    type_event_public           = forms.BooleanField(initial=True, required=False)
    type_event_private          = forms.BooleanField(initial=True, required=False)
    type_event_presentation     = forms.BooleanField(initial=True, required=False)

    event_public                = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)
    event_private               = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)
    event_presentation          = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)

    regulations_accept          = forms.BooleanField(initial=True, required=True)
    all_dopinfo                 = forms.BooleanField(initial=False, required=False)

    def __init__(self, *args, **kwargs):
        self.initial = kwargs.get('initial')
        self.user = self.initial.get('user', None)
        super(GigDateForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.second_country:
            self.initial['secondary_data'] = True

        # Города автозаполнения, что бы не выводить портянку
        if 0 == len(self.data) and not self.instance.second_country:
            self.fields['second_city'].queryset = City.objects.none()
        elif len(self.data) > 0 and self.data.get('second_country'):
            self.fields['second_city'].queryset = City.objects.filter(country=self.data.get('second_country'))
        else:
            self.fields['second_city'].queryset = City.objects.filter(country=self.instance.second_country)

        self.fields['artist'] = forms.ModelChoiceField(queryset=ArtistProfile.objects.filter(user=self.user), empty_label=None)
        self.fields['secondary_data'].widget.attrs['class'] = 'checkbox'
        self.fields['geography_queries'].widget.attrs['class'] = 'nostyle'
        self.fields['payment_method'].widget.attrs['class'] = 'nostyle'

        list_ot = {'class': 'input_money', 'placeholder': _("from")}
        list_do = {'class': 'input_money', 'placeholder': _("to")}
        self.fields['mony_publ_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_publ_oth_from'].widget.attrs = list_ot
        self.fields['mony_publ_oth_to'].widget.attrs = list_do

        self.fields['mony_priv_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_priv_oth_from'].widget.attrs = list_ot
        self.fields['mony_priv_oth_to'].widget.attrs = list_do

        self.fields['mony_pres_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_pres_oth_from'].widget.attrs = list_ot
        self.fields['mony_pres_oth_to'].widget.attrs = list_do

    def clean(self):
        cleaned_data = self.cleaned_data
        # Если стоит маркер "вторая дата", то проверяем чтобы было заполненоо три поля
        if cleaned_data.get('secondary_data'):
            if not cleaned_data.get('second_date'):
                self._errors["second_date"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('second_country'):
                self._errors["second_country"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('second_city'):
                self._errors["second_city"] = self.error_class([_('This field is required')])

        # выбрана хоть одна форма договора
        if not cleaned_data.get('contract_artist') and not cleaned_data.get('contract_site'):
            self._errors["contract_all"] = self.error_class([_('Form of contract is not selected')])

        if not cleaned_data.get('type_event_public') and not cleaned_data.get('type_event_private') and not cleaned_data.get('type_event_presentation'):
            self._errors["event_type"] = self.error_class([_('You have no type of event')])

        if not cleaned_data.get('type_event_public'):
            cleaned_data['event_public'] = None
        if not cleaned_data.get('type_event_private'):
            cleaned_data['event_private'] = None
        if not cleaned_data.get('type_event_presentation'):
            cleaned_data['event_presentation'] = None

        if cleaned_data.get('event_public') == None and cleaned_data.get('event_private') == None and cleaned_data.get('event_presentation') == None:
            self._errors["event_type"] = self.error_class([_('Fee type is not selected')])

        #Публичное
        if cleaned_data.get('event_public') and cleaned_data.get('event_public') == 1:
            if not cleaned_data.get('mony_publ_fix'):
                self._errors["mony_publ_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_public') and cleaned_data.get('event_public') == 2:
            if not cleaned_data.get('mony_publ_oth_from'):
                self._errors["mony_publ_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_publ_oth_to'):
                self._errors["mony_publ_oth_to"] = self.error_class([_('This field is required')])
        #Приватное
        if cleaned_data.get('event_private') and cleaned_data.get('event_private') == 1:
            if not cleaned_data.get('mony_priv_fix'):
                self._errors["mony_priv_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_private') and cleaned_data.get('event_private') == 2:
            if not cleaned_data.get('mony_priv_oth_from'):
                self._errors["mony_priv_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_priv_oth_to'):
                self._errors["mony_priv_oth_to"] = self.error_class([_('This field is required')])
        #Презентация
        if cleaned_data.get('event_presentation') and cleaned_data.get('event_presentation') == 1:
            if not cleaned_data.get('mony_pres_fix'):
                self._errors["mony_pres_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_presentation') and cleaned_data.get('event_presentation') == 2:
            if not cleaned_data.get('mony_pres_oth_from'):
                self._errors["mony_pres_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_pres_oth_to'):
                self._errors["mony_pres_oth_to"] = self.error_class([_('This field is required')])

        if not self.instance or not self.instance.dates == cleaned_data.get('dates'):
            if cleaned_data.get('dates') and cleaned_data.get('artist'):
                try:
                    GigDate.objects.get(artist=cleaned_data.get('artist'), dates=cleaned_data.get('dates'), type_date=GigDate.TYPEDATE0)
                except GigDate.DoesNotExist:
                    pass
                else:
                    self._errors["dates"] = self.error_class([_('This event is already taken')])

        return cleaned_data

    class Meta:
        model = GigDate
        exclude = ['user', 'view', 'banned', 'type_date', 'uuid_id']


class GigTenderForm(forms.ModelForm):
    """
    Форма добавления гиг тендера
    """
    regulations_accept          = forms.BooleanField(initial=True, required=True)
    dates                       = forms.DateField(initial=datetime.now()+timedelta(days=5), input_formats=('%m/%d/%Y',), widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}))
    stop_dates                  = forms.DateField(initial=datetime.now()+timedelta(days=10), input_formats=('%m/%d/%Y',), required=True, widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}))

    type_event_public           = forms.BooleanField(initial=True, required=False)
    type_event_private          = forms.BooleanField(initial=True, required=False)
    type_event_presentation     = forms.BooleanField(initial=True, required=False)

    event_public                = forms.IntegerField(widget=forms.RadioSelect(choices=GigTender.EVENT_TYPE), initial=GigTender.EVENT0, required=False)
    event_private               = forms.IntegerField(widget=forms.RadioSelect(choices=GigTender.EVENT_TYPE), initial=GigTender.EVENT0, required=False)
    event_presentation          = forms.IntegerField(widget=forms.RadioSelect(choices=GigTender.EVENT_TYPE), initial=GigTender.EVENT0, required=False)

    def __init__(self, *args, **kwargs):
        self.initial = kwargs.get('initial')
        self.user = self.initial.get('user', None)
        super(GigTenderForm, self).__init__(*args, **kwargs)

        self.fields['club'] = forms.ModelChoiceField(queryset=ClubProfile.objects.filter(user=self.user), empty_label=None)

        if self.instance.event_public == None:
            self.initial['type_event_public'] = False
        if self.instance.event_private == None:
            self.initial['type_event_private'] = False
        if self.instance.event_presentation == None:
            self.initial['type_event_presentation'] = False

        self.fields['type_format'].widget.attrs['class'] = 'nostyle'
        self.fields['geography_queries'].widget.attrs['class'] = 'nostyle'
        self.fields['payment_method'].widget.attrs['class'] = 'nostyle'
        self.fields['queries13'].widget.attrs['class'] = 'nostyle'

        list_ot = {'class': 'input_money', 'placeholder': _("from")}
        list_do = {'class': 'input_money', 'placeholder': _("to")}
        self.fields['mony_publ_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_publ_oth_from'].widget.attrs = list_ot
        self.fields['mony_publ_oth_to'].widget.attrs = list_do

        self.fields['mony_priv_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_priv_oth_from'].widget.attrs = list_ot
        self.fields['mony_priv_oth_to'].widget.attrs = list_do

        self.fields['mony_pres_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_pres_oth_from'].widget.attrs = list_ot
        self.fields['mony_pres_oth_to'].widget.attrs = list_do

    def clean(self):
        cleaned_data = self.cleaned_data

        # выбрана хоть одна форма договора
        if not cleaned_data.get('contract_artist') and not cleaned_data.get('contract_agent') and not cleaned_data.get('contract_site'):
            self._errors["contract_all"] = self.error_class([_('Form of contract is not selected')])

        if not cleaned_data.get('type_event_public') and not cleaned_data.get('type_event_private') and not cleaned_data.get('type_event_presentation'):
            self._errors["event_type"] = self.error_class([_('You have no type of event')])

        if not cleaned_data.get('type_event_public'):
            cleaned_data['event_public'] = None
        if not cleaned_data.get('type_event_private'):
            cleaned_data['event_private'] = None
        if not cleaned_data.get('type_event_presentation'):
            cleaned_data['event_presentation'] = None

        if cleaned_data.get('event_public') == None and cleaned_data.get('event_private') == None and cleaned_data.get('event_presentation') == None:
            self._errors["event_type"] = self.error_class([_('Fee type is not selected')])

        #Публичное
        if cleaned_data.get('event_public') and cleaned_data.get('event_public') == 1:
            if not cleaned_data.get('mony_publ_fix'):
                self._errors["mony_publ_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_public') and cleaned_data.get('event_public') == 2:
            if not cleaned_data.get('mony_publ_oth_from'):
                self._errors["mony_publ_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_publ_oth_to'):
                self._errors["mony_publ_oth_to"] = self.error_class([_('This field is required')])
        #Приватное
        if cleaned_data.get('event_private') and cleaned_data.get('event_private') == 1:
            if not cleaned_data.get('mony_priv_fix'):
                self._errors["mony_priv_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_private') and cleaned_data.get('event_private') == 2:
            if not cleaned_data.get('mony_priv_oth_from'):
                self._errors["mony_priv_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_priv_oth_to'):
                self._errors["mony_priv_oth_to"] = self.error_class([_('This field is required')])
        #Презентация
        if cleaned_data.get('event_presentation') and cleaned_data.get('event_presentation') == 1:
            if not cleaned_data.get('mony_pres_fix'):
                self._errors["mony_pres_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_presentation') and cleaned_data.get('event_presentation') == 2:
            if not cleaned_data.get('mony_pres_oth_from'):
                self._errors["mony_pres_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_pres_oth_to'):
                self._errors["mony_pres_oth_to"] = self.error_class([_('This field is required')])

        if not self.instance or not self.instance.dates == cleaned_data.get('dates'):
            if cleaned_data.get('dates') and cleaned_data.get('club'):
                try:
                    GigTender.objects.get(club=cleaned_data.get('club'), dates=cleaned_data.get('dates'), type_date=GigTender.TYPEDATE2)
                except GigTender.DoesNotExist:
                    pass
                else:
                    self._errors["dates"] = self.error_class([_('This event is already taken')])

        return cleaned_data

    class Meta:
        model = GigTender
        exclude = ['user', 'view', 'banned', 'type_date', 'uuid_id']


class GigBarterForm(forms.ModelForm):
    """
    Форма Добавления бартера
    """
    secondary_data              = forms.BooleanField(required=False)
    dates                       = forms.DateField(initial=datetime.now()+timedelta(days=5), input_formats=('%m/%d/%Y',), widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}))
    second_date                 = forms.DateField(initial=datetime.now()+timedelta(days=10), required=False, input_formats=('%m/%d/%Y',), widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}))
    type_event_public           = forms.BooleanField(initial=True, required=False)
    type_event_private          = forms.BooleanField(initial=True, required=False)
    type_event_presentation     = forms.BooleanField(initial=True, required=False)

    event_public                = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)
    event_private               = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)
    event_presentation          = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)

    barter_type_event_public           = forms.BooleanField(initial=True, required=False)
    barter_type_event_private          = forms.BooleanField(initial=True, required=False)
    barter_type_event_presentation     = forms.BooleanField(initial=True, required=False)

    barter_event_public                = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)
    barter_event_private               = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)
    barter_event_presentation          = forms.IntegerField(widget=forms.RadioSelect(choices=GigDate.EVENT_TYPE), initial=GigDate.EVENT0, required=False)

    regulations_accept          = forms.BooleanField(initial=True, required=True)
    all_dopinfo                 = forms.BooleanField(initial=False, required=False)

    def __init__(self, *args, **kwargs):
        self.initial = kwargs.get('initial')
        self.user = self.initial.get('user', None)
        super(GigBarterForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.second_country:
            self.initial['secondary_data'] = True

        # Города автозаполнения, что бы не выводить портянку
        if 0 == len(self.data) and not self.instance.second_country:
            self.fields['second_city'].queryset = City.objects.none()
        elif len(self.data) > 0 and self.data.get('second_country'):
            self.fields['second_city'].queryset = City.objects.filter(country=self.data.get('second_country'))
        else:
            self.fields['second_city'].queryset = City.objects.filter(country=self.instance.second_country)

        self.fields['artist'] = forms.ModelChoiceField(queryset=ArtistProfile.objects.filter(user=self.user), empty_label=None)
        self.fields['club'] = forms.ModelChoiceField(queryset=ClubProfile.objects.filter(user=self.user), empty_label=None)
        self.fields['secondary_data'].widget.attrs['class'] = 'checkbox'
        self.fields['geography_queries'].widget.attrs['class'] = 'nostyle'
        self.fields['payment_method'].widget.attrs['class'] = 'nostyle'
        self.fields['barter_payment_method'].widget.attrs['class'] = 'nostyle'

        list_ot = {'class': 'input_money', 'placeholder': _("from")}
        list_do = {'class': 'input_money', 'placeholder': _("to")}
        self.fields['mony_publ_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_publ_oth_from'].widget.attrs = list_ot
        self.fields['mony_publ_oth_to'].widget.attrs = list_do

        self.fields['mony_priv_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_priv_oth_from'].widget.attrs = list_ot
        self.fields['mony_priv_oth_to'].widget.attrs = list_do

        self.fields['mony_pres_fix'].widget.attrs['class'] = 'input_money'
        self.fields['mony_pres_oth_from'].widget.attrs = list_ot
        self.fields['mony_pres_oth_to'].widget.attrs = list_do

        self.fields['barter_mony_publ_fix'].widget.attrs['class'] = 'input_money'
        self.fields['barter_mony_publ_oth_from'].widget.attrs = list_ot
        self.fields['barter_mony_publ_oth_to'].widget.attrs = list_do

        self.fields['barter_mony_priv_fix'].widget.attrs['class'] = 'input_money'
        self.fields['barter_mony_priv_oth_from'].widget.attrs = list_ot
        self.fields['barter_mony_priv_oth_to'].widget.attrs = list_do

        self.fields['barter_mony_pres_fix'].widget.attrs['class'] = 'input_money'
        self.fields['barter_mony_pres_oth_from'].widget.attrs = list_ot
        self.fields['barter_mony_pres_oth_to'].widget.attrs = list_do

    def clean(self):
        cleaned_data = self.cleaned_data
        # Если стоит маркер "вторая дата", то проверяем чтобы было заполненоо три поля
        if cleaned_data.get('secondary_data'):
            if not cleaned_data.get('second_date'):
                self._errors["second_date"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('second_country'):
                self._errors["second_country"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('second_city'):
                self._errors["second_city"] = self.error_class([_('This field is required')])

        # выбрана хоть одна форма договора
        if not cleaned_data.get('contract_artist') and not cleaned_data.get('contract_site'):
            self._errors["contract_all"] = self.error_class([_('Form of contract is not selected')])
        if not cleaned_data.get('type_event_public') and not cleaned_data.get('type_event_private') and not cleaned_data.get('barter_type_event_presentation'):
            self._errors["event_type"] = self.error_class([_('You have no type of event')])

        # выбрана хоть одна форма договора бартера
        if not cleaned_data.get('barter_contract_artist') and not cleaned_data.get('barter_contract_site'):
            self._errors["barter_contract_all"] = self.error_class([_('Form of contract is not selected')])
        if not cleaned_data.get('barter_type_event_public') and not cleaned_data.get('barter_type_event_private') and not cleaned_data.get('barter_type_event_presentation'):
            self._errors["barter_event_type"] = self.error_class([_('You have no type of event')])

        if not cleaned_data.get('type_event_public'):
            cleaned_data['event_public'] = None
        if not cleaned_data.get('type_event_private'):
            cleaned_data['event_private'] = None
        if not cleaned_data.get('type_event_presentation'):
            cleaned_data['event_presentation'] = None

        if not cleaned_data.get('barter_type_event_public'):
            cleaned_data['barter_event_public'] = None
        if not cleaned_data.get('barter_type_event_private'):
            cleaned_data['barter_event_private'] = None
        if not cleaned_data.get('barter_type_event_presentation'):
            cleaned_data['barter_event_presentation'] = None

        if cleaned_data.get('event_public') == None and cleaned_data.get('event_private') == None and cleaned_data.get('event_presentation') == None:
            self._errors["event_type"] = self.error_class([_('Fee type is not selected')])

        if cleaned_data.get('barter_event_public') == None and cleaned_data.get('barter_event_private') == None and cleaned_data.get('barter_event_presentation') == None:
            self._errors["barter_event_type"] = self.error_class([_('Fee type is not selected')])

        #Публичное
        if cleaned_data.get('event_public') and cleaned_data.get('event_public') == 1:
            if not cleaned_data.get('mony_publ_fix'):
                self._errors["mony_publ_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_public') and cleaned_data.get('event_public') == 2:
            if not cleaned_data.get('mony_publ_oth_from'):
                self._errors["mony_publ_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_publ_oth_to'):
                self._errors["mony_publ_oth_to"] = self.error_class([_('This field is required')])
        #Приватное
        if cleaned_data.get('event_private') and cleaned_data.get('event_private') == 1:
            if not cleaned_data.get('mony_priv_fix'):
                self._errors["mony_priv_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_private') and cleaned_data.get('event_private') == 2:
            if not cleaned_data.get('mony_priv_oth_from'):
                self._errors["mony_priv_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_priv_oth_to'):
                self._errors["mony_priv_oth_to"] = self.error_class([_('This field is required')])
        #Презентация
        if cleaned_data.get('event_presentation') and cleaned_data.get('event_presentation') == 1:
            if not cleaned_data.get('mony_pres_fix'):
                self._errors["mony_pres_fix"] = self.error_class([_('This field is required')])
        if cleaned_data.get('event_presentation') and cleaned_data.get('event_presentation') == 2:
            if not cleaned_data.get('mony_pres_oth_from'):
                self._errors["mony_pres_oth_from"] = self.error_class([_('This field is required')])
            if not cleaned_data.get('mony_pres_oth_to'):
                self._errors["mony_pres_oth_to"] = self.error_class([_('This field is required')])

        if not self.instance or not self.instance.dates == cleaned_data.get('dates'):
            if cleaned_data.get('dates') and cleaned_data.get('artist'):
                try:
                    GigBarter.objects.get(artist=cleaned_data.get('artist'), dates=cleaned_data.get('dates'), type_date=GigBarter.TYPEDATE1)
                except GigBarter.DoesNotExist:
                    pass
                else:
                    self._errors["dates"] = self.error_class([_('This event is already taken')])

        return cleaned_data

    class Meta:
        model = GigBarter
        exclude = ['user', 'view', 'banned', 'type_date', 'uuid_id']


class GigDatesFilterForm(forms.Form):
    """
    Фильтры поиска даты в концерт дне
    """
    ganre           = forms.ModelChoiceField(label=_(u"Genre"), queryset=Genre.objects.all().only('title'), required=False, empty_label=_(u"- Select genre -"))
    country         = forms.ModelChoiceField(label=_(u"Country"), queryset=Country.objects.all().only('title'), required=False, empty_label=_(u"- Country of the venue -"))
    country_artist  = forms.ModelChoiceField(label=_(u"Country artist"), queryset=Country.objects.all().only('title'), required=False, empty_label=_(u"- Country of the artist -"))
    second_date     = forms.BooleanField(label=_(u"Second date"), required=False, widget=forms.CheckboxInput(attrs={'class': 'checkbox'}))