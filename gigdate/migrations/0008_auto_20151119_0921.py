# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gigdate', '0007_auto_20150811_1849'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dateall',
            name='payment_plan',
            field=models.IntegerField(default=0, verbose_name='#\u0413\u0440\u0430\u0444\u0438\u043a \u043e\u043f\u043b\u0430\u0442\u044b \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', choices=[(1, '100%'), (2, '50%-50%'), (3, '10%-90%'), (4, '10%-40%-50%')]),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_payment_plan',
            field=models.IntegerField(default=0, verbose_name='#\u0413\u0440\u0430\u0444\u0438\u043a \u043e\u043f\u043b\u0430\u0442\u044b \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', choices=[(1, '100%'), (2, '50%-50%'), (3, '10%-90%'), (4, '10%-40%-50%')]),
        ),
    ]
