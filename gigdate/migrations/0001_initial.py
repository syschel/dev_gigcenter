# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('common', '0002_paymentmethod'),
    ]

    operations = [
        migrations.CreateModel(
            name='DateAll',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e', null=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name='#\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e', null=True)),
                ('dates', models.DateField(null=True, verbose_name='#\u0414\u0430\u0442\u0430 \u043a\u0430\u043b\u0435\u043d\u0434\u0430\u0440\u044f', blank=True)),
                ('payment_plan', models.IntegerField(default=0, null=True, verbose_name='#\u0413\u0440\u0430\u0444\u0438\u043a \u043e\u043f\u043b\u0430\u0442\u044b \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', blank=True, choices=[(0, 'Contractual'), (1, 'Contractual'), (2, 'Contractual'), (3, 'Contractual'), (4, 'Contractual')])),
                ('event_public', models.IntegerField(default=0, null=True, verbose_name='#\u041c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u0435 \u043f\u0443\u0431\u043b\u0438\u0447\u043d\u043e\u0435', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, 'Another fee')])),
                ('event_private', models.IntegerField(default=0, null=True, verbose_name='#\u041c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u0435 \u043f\u0440\u0438\u0432\u0430\u0442\u043d\u043e\u0435', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, 'Another fee')])),
                ('event_presentation', models.IntegerField(default=0, null=True, verbose_name='#\u041c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u0435 \u043f\u0440\u0435\u0437\u0435\u043d\u0442\u0430\u0446\u0438\u044f', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, 'Another fee')])),
                ('mony_publ_fix', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0444\u0438\u043a\u0441\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u044b\u0439', blank=True)),
                ('mony_publ_oth_from', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True)),
                ('mony_publ_oth_to', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True)),
                ('mony_priv_fix', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0444\u0438\u043a\u0441\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u044b\u0439', blank=True)),
                ('mony_priv_oth_from', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True)),
                ('mony_priv_oth_to', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True)),
                ('mony_pres_fix', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0444\u0438\u043a\u0441\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u044b\u0439', blank=True)),
                ('mony_pres_oth_from', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True)),
                ('mony_pres_oth_to', models.IntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True)),
                ('currency', models.IntegerField(default=0, verbose_name='#\u0412\u0430\u043b\u044e\u0442\u0430 \u043e\u043f\u043b\u0430\u0442\u044b \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', choices=[(0, 'EUR'), (1, 'USD'), (2, 'National currency')])),
                ('rider_payments', models.IntegerField(default=0, verbose_name='#\u041e\u043f\u043b\u0430\u0442\u0430 \u0440\u0430\u0439\u0434\u0435\u0440\u043e\u0432', choices=[(0, 'Customer expenses'), (1, 'Artist expenses')])),
                ('transport_payments', models.IntegerField(default=0, verbose_name='#\u0422\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u043d\u044b\u0435 \u0440\u0430\u0441\u0445\u043e\u0434\u044b', choices=[(0, 'Customer expenses'), (1, 'Artist expenses')])),
                ('view', models.IntegerField(default=0, verbose_name='#\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u044b', blank=True)),
                ('banned', models.BooleanField(default=False, help_text='#\u0417\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u043d', verbose_name='#\u0417\u0430\u0431\u0430\u043d\u0435\u043d')),
                ('geography_queries', models.ManyToManyField(related_name='geography_dates', verbose_name='#\u0413\u0435\u043e\u0433\u0440\u0430\u0444\u0438\u044f \u0437\u0430\u043f\u0440\u043e\u0441\u043e\u0432', to='common.Country')),
                ('payment_method', models.ManyToManyField(related_name='payment_methods', verbose_name='#\u0424\u043e\u0440\u043c\u0430 \u043e\u043f\u043b\u0430\u0442\u044b \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', to='common.PaymentMethod')),
                ('user', models.ForeignKey(related_name='user_dateall', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['dates'],
                'verbose_name': 'Date',
                'verbose_name_plural': 'Dates',
            },
        ),
    ]
