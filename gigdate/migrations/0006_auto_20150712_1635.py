# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gigdate', '0005_auto_20150609_1545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dateall',
            name='mony_pres_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 fixed', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_pres_oth_from',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_pres_oth_to',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_priv_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 fixed', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_priv_oth_from',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_priv_oth_to',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_publ_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 fixed', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_publ_oth_from',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='mony_publ_oth_to',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_pres_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 fixed', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_pres_oth_from',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_pres_oth_to',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_priv_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 fixed', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_priv_oth_from',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_priv_oth_to',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_publ_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 fixed', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_publ_oth_from',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u043e\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_mony_publ_oth_to',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0413\u043e\u043d\u043e\u0440\u0430\u0440 \u0434\u0440\u0443\u0433\u043e\u0439 \u0434\u043e', blank=True),
        ),
    ]
