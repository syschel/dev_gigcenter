# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gigdate', '0004_auto_20150608_1346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dateall',
            name='type_date',
            field=models.IntegerField(default=0, blank=True, verbose_name='#\u0422\u0438\u043f \u0434\u0430\u0442\u044b', choices=[(0, 'gigdate'), (1, 'barter'), (2, 'tender')]),
        ),
    ]
