# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gigdate', '0003_auto_20150604_1355'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='dateall',
            options={'ordering': ['-create'], 'verbose_name': 'Date all', 'verbose_name_plural': 'Dates all'},
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_payment_plan',
            field=models.IntegerField(default=0, verbose_name='#\u0413\u0440\u0430\u0444\u0438\u043a \u043e\u043f\u043b\u0430\u0442\u044b \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', choices=[(0, 'Any'), (1, '100%'), (2, '50%-50%'), (3, '10%-90%'), (4, '10%-40%-50%')]),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_rider_payments',
            field=models.IntegerField(default=0, verbose_name='#\u041e\u043f\u043b\u0430\u0442\u0430 \u0440\u0430\u0439\u0434\u0435\u0440\u043e\u0432', choices=[(0, 'Customer expenses'), (1, 'Artist expenses')]),
        ),
        migrations.AlterField(
            model_name='gigbarter',
            name='barter_transport_payments',
            field=models.IntegerField(default=0, verbose_name='#\u0422\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u043d\u044b\u0435 \u0440\u0430\u0441\u0445\u043e\u0434\u044b', choices=[(0, 'Customer expenses'), (1, 'Artist expenses')]),
        ),
    ]
