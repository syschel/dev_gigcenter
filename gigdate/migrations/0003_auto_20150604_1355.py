# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0003_methodsadvert'),
        ('gigdate', '0002_auto_20150603_0754'),
    ]

    operations = [
        migrations.AddField(
            model_name='gigbarter',
            name='second_city',
            field=models.ForeignKey(verbose_name='#\u0413\u043e\u0440\u043e\u0434 \u0432\u0442\u043e\u0440\u043e\u0439 \u0434\u0430\u0442\u044b', blank=True, to='common.City', null=True),
        ),
        migrations.AddField(
            model_name='gigbarter',
            name='second_country',
            field=models.ForeignKey(verbose_name='#\u0421\u0442\u0440\u0430\u043d\u0430 \u0432\u0442\u043e\u0440\u043e\u0439 \u0434\u0430\u0442\u044b', blank=True, to='common.Country', null=True),
        ),
        migrations.AddField(
            model_name='gigbarter',
            name='second_date',
            field=models.DateField(null=True, verbose_name='#\u041f\u0435\u0440\u0432\u0430\u044f \u0434\u0430\u0442\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='payment_plan',
            field=models.IntegerField(default=0, verbose_name='#\u0413\u0440\u0430\u0444\u0438\u043a \u043e\u043f\u043b\u0430\u0442\u044b \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', choices=[(0, 'Any'), (1, '100%'), (2, '50%-50%'), (3, '10%-90%'), (4, '10%-40%-50%')]),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='rider_payments',
            field=models.IntegerField(default=0, verbose_name='#\u041e\u043f\u043b\u0430\u0442\u0430 \u0440\u0430\u0439\u0434\u0435\u0440\u043e\u0432', choices=[(0, 'Customer expenses'), (1, 'Artist expenses')]),
        ),
        migrations.AlterField(
            model_name='dateall',
            name='transport_payments',
            field=models.IntegerField(default=0, verbose_name='#\u0422\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u043d\u044b\u0435 \u0440\u0430\u0441\u0445\u043e\u0434\u044b', choices=[(0, 'Customer expenses'), (1, 'Artist expenses')]),
        ),
    ]
