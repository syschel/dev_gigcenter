# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gigdate', '0006_auto_20150712_1635'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gigtender',
            name='contract_club',
        ),
        migrations.AlterField(
            model_name='dateall',
            name='type_date',
            field=models.IntegerField(default=0, blank=True, verbose_name='#\u0422\u0438\u043f \u0434\u0430\u0442\u044b', choices=[(0, 'Gig-date'), (1, 'Barter-date'), (2, 'Tender-date')]),
        ),
        migrations.AlterField(
            model_name='gigtender',
            name='queries3',
            field=models.IntegerField(default=0, verbose_name='#\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, 'Commercial'), (1, 'Noncommercial'), (2, 'Charitable'), (3, 'Other')]),
        ),
    ]
