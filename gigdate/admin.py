#-*- coding:utf-8 -*-
from django.contrib import admin
from gigdate.models import DateAll, GigDate, GigTender, GigBarter


class SaveAsAdmin(admin.ModelAdmin):
    save_as = True
    list_display = ('uuid_id', )

admin.site.register(DateAll)
admin.site.register(GigDate, SaveAsAdmin)
admin.site.register(GigBarter, SaveAsAdmin)
admin.site.register(GigTender, SaveAsAdmin)
