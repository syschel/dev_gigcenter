# -*- coding:utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from gigdate.views import AddGigdateCreateView, AddGigTenderCreateView, AddGigBarterCreateView,\
    GigdateDetailView, GigbarterDetailView, GigtenderDetailView, FilterDayFormView, GoDateRedirect, GoTenderRedirect,\
    GigdateRedirect, GigTenderRedirect, GigBarterRedirect

urlpatterns = [
    url(r'^add_gigdate/$', login_required(AddGigdateCreateView.as_view()), name='add_gigdate'),
    url(r'^add_barter/$', login_required(AddGigBarterCreateView.as_view()), name='add_barter'),
    url(r'^add_tender/$', login_required(AddGigTenderCreateView.as_view()), name='add_tender'),


    url(r'^(?P<pk>\d+)/gigdate/$', GigdateRedirect.as_view(), name='gigdate_302'),
    url(r'^(?P<pk>\d+)/barter/$', GigBarterRedirect.as_view(), name='barter_302'),
    url(r'^(?P<pk>\d+)/tender/$', GigTenderRedirect.as_view(), name='tender_302'),

    url(r'^(?P<slug>[^/]+)/gigdate/$', GigdateDetailView.as_view(), name='gigdate_view'),
    url(r'^(?P<slug>[^/]+)/barter/$', GigbarterDetailView.as_view(), name='barter_view'),
    url(r'^(?P<slug>[^/]+)/tender/$', GigtenderDetailView.as_view(), name='tender_view'),

    url(r'^go/gigdate/(?P<artist>\d+)/(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})/$', GoDateRedirect.as_view(), name='go_gigdate'),
    url(r'^go/tender/(?P<venue>\d+)/(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})/$', GoTenderRedirect.as_view(), name='go_tender'),

    url(r'^filter_day/$', FilterDayFormView.as_view(), name='filter_day'),
]