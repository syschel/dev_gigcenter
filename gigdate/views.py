#-*- coding:utf-8 -*-
import uuid
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect, Http404
from django.conf import settings
from django.contrib import messages
from django.forms.utils import ErrorList
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import get_object_or_404
from django.db.models import Count
from django.views.generic.edit import FormMixin, FormView, UpdateView, CreateView
from django.views.generic import DetailView, ListView, RedirectView
from common.functions import send_mailbox
from account.models import ArtistProfile, ClubProfile
from gigdate.models import DateAll, GigDate, GigBarter, GigTender
from gigdate.forms import GigDateForm, GigTenderForm, GigBarterForm, GigDatesFilterForm
from trade.models import Trades
from trade.forms import TradesForm, TradesTenderForm, TradesBarterForm
import calendar
from itertools import groupby, chain
from datetime import timedelta, date, datetime
from dateutil.relativedelta import relativedelta


class GigCalendarMix(object):
    """
    Микс календаря
    """
    today = date.today()
    todatetime = datetime.utcnow()
    tomonth = False
    dates_one_day = 5
    type_date = None

    def get_list_days(self, year=None, month=None):
        """ Формируем список дней текущего """
        t = date.today()
        c = calendar.Calendar()
        if year and month:
            if t.year != year or t.month != month:
                t = date(int(year), int(month), 1)
                self.tomonth = t
        return list(chain(*c.monthdatescalendar(t.year, t.month)))

    def convert_dates_to_day(self, queryset,  year=None, month=None):
        list_day = self.get_list_days(year=year, month=month)
        result_day_list = {}
        counts = queryset.values('dates').annotate(count=Count('dates')).order_by()
        count_days = {}
        for count in counts:
            if count.get('dates') not in count_days:
                count_days[count.get('dates')] = count.get('count')
        for day in list_day:
            if day.weekday() in result_day_list:
                result_day_list[day.weekday()].append({
                    'dates': day,
                    'dcount': count_days.get(day, 0),
                    'myweeks': day.strftime("%a %-d"),
                    'obj': [],
                    'is_past': day <= date.today() - timedelta(days=1)  # or day.month > date.today().month
                })
            else:
                result_day_list[day.weekday()] = [{
                    'dates': day,
                    'dcount': count_days.get(day, 0),
                    'myweeks': day.strftime("%a %-d"),
                    'obj': [],
                    'is_past': day <= date.today() - timedelta(days=1)  # or day.month > date.today().month
                }]

        # for qu in queryset:
        #     print qu.dates
        queryset = groupby(queryset, lambda obj: obj.dates)
        # Наполняем список календаря объектами
        for k, group in queryset:
            for d_list in range(len(result_day_list[k.weekday()])):
                if result_day_list[k.weekday()][d_list]['dates'] == k:
                    result_day_list[k.weekday()][d_list]['obj'].append(list(group))
        return result_day_list


class Index(GigCalendarMix, ListView):
    """
    Календарь гиг-дат
    """
    model = DateAll
    template_name = 'index.html'
    ordering = '-create'
    calendar_days = None

    def get_queryset(self):

        self.type_date = self.kwargs.get('type_date')
        if self.type_date and self.type_date == 'barter':
            type_date_value = DateAll.TYPEDATE1
            select_rel = {'gigbarter__artist', 'gigbarter__artist__country'}
        elif self.type_date and self.type_date == 'tender':
            type_date_value = DateAll.TYPEDATE2
            select_rel = {'gigtender__club', 'gigtender__club__country'}
        else:
            type_date_value = DateAll.TYPEDATE0
            select_rel = {'gigdate__artist', 'gigdate__artist__country'}
        params = {'type_date': type_date_value}

        params.update({'dates__in': self.get_list_days(year=self.kwargs.get('year'), month=self.kwargs.get('month'))})

        ctd_filter = self.request.session.get('ctd_filter')
        if ctd_filter:
            if ctd_filter.get('country'):
                params.update({'geography_queries': int(ctd_filter.get('country'))})

            if ctd_filter.get('country_artist'):
                if not self.type_date:
                    params.update({'gigdate__artist__country': ctd_filter.get('country_artist')})
                if self.type_date == 'barter':
                    params.update({'gigbarter__artist__country': ctd_filter.get('country_artist')})

            if ctd_filter.get('second_date'):
                if self.type_date == 'barter':
                    params.update({'gigbarter__second_country__isnull': False})
                elif self.type_date == 'tender':
                    pass
                else:
                    params.update({'gigdate__second_country__isnull': False})

            if ctd_filter.get('ganre'):
                if self.type_date == 'tender':
                    params.update({'gigtender__club__genre': ctd_filter.get('ganre')})
                elif self.type_date == 'barter':
                    params.update({'gigbarter__artist__genre': ctd_filter.get('ganre')})
                else:
                    params.update({'gigdate__artist__genre': ctd_filter.get('ganre')})

        queryset = super(Index, self).get_queryset()
        return queryset.filter(**params).only('dates', 'type_date').select_related(*select_rel).order_by('-dates', '-create')

    def get(self, request, *args, **kwargs):
        if self.kwargs.get('type_date') and not self.kwargs.get('type_date') in ['barter', 'tender']:
            raise Http404
        return super(Index, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)

        self.calendar_days = self.convert_dates_to_day(queryset=self.get_queryset(), year=self.kwargs.get('year'),
                                                       month=self.kwargs.get('month'))
        context['day_list'] = self.calendar_days
        param = {}
        if self.type_date == 'barter':
            param.update({'gigbarter__second_country__isnull': False})
            select_rel = {'gigbarter__artist', 'gigbarter__artist__country', 'gigbarter__second_country'}
        elif self.type_date == 'tender':
            pass
        else:
            param.update({'gigdate__second_country__isnull': False})
            select_rel = {'gigdate__artist', 'gigdate__artist__country', 'gigdate__second_country'}
        if not self.type_date == 'tender':
            context['second_dates'] = self.get_queryset().filter(**param).select_related(*select_rel)
        ctd_filter = self.request.session.get('ctd_filter')
        context['type_date'] = self.type_date
        context['filterform'] = GigDatesFilterForm(initial=ctd_filter) if ctd_filter else GigDatesFilterForm()
        tomonth = self.tomonth if self.tomonth else date.today()
        context['tomonth'] = tomonth
        context['prev_month'] = tomonth - relativedelta(months=1)
        context['next_month'] = tomonth + relativedelta(months=1)
        context['prev_years'] = tomonth - relativedelta(years=1)
        context['next_years'] = tomonth + relativedelta(years=1)
        context['workaround'] = True if self.type_date == 'tender' else False
        workaround_day = {
            '012016': 411,
            '022016': 454,
            '032016': 492,
            '042016': 438,
            '052016': 511,
            '062016': 479,
            '072016': 583,
            '082016': 465,
            '092016': 374,
            '102016': 298,
            '112016': 203,
            '122016': 754,
            '012017': 82,
            '022017': 17,
            '032017': 4
        }
        context['workaround_day'] = workaround_day.get(tomonth.strftime("%m%Y"), 0)
        return context


class DateListView(ListView):
    """ Выводим список дат/бартеров/тендеров в выбраном дне """
    template_name = 'gigdate/concrete_day.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(DateListView, self).get_context_data(**kwargs)

        ctd_filter = self.request.session.get('ctd_filter')
        context['concrete_day'] = date(int(self.kwargs.get('year')), int(self.kwargs.get('month')), int(self.kwargs.get('day')))
        context['type_date'] = self.kwargs.get('type_date')
        context['filterform'] = GigDatesFilterForm(initial=ctd_filter) if ctd_filter else GigDatesFilterForm()
        return context

    def get_queryset(self):

        date_day = date(int(self.kwargs.get('year')), int(self.kwargs.get('month')), int(self.kwargs.get('day')))
        params = {'dates': date_day}

        type_dates = self.kwargs.get('type_date')
        if type_dates == 'gigdate':
            type_date = DateAll.TYPEDATE0
        elif type_dates == 'barter':
            type_date = DateAll.TYPEDATE1
        elif type_dates == 'tender':
            type_date = DateAll.TYPEDATE2
        else:
            raise Http404
        params.update({'type_date': type_date})
        ctd_filter = self.request.session.get('ctd_filter')
        if ctd_filter:
            if ctd_filter.get('country'):
                params.update({'geography_queries': int(ctd_filter.get('country'))})

            if ctd_filter.get('country_artist'):
                if type_dates == 'gigdate':
                    params.update({'gigdate__artist__country': ctd_filter.get('country_artist')})
                if type_dates == 'barter':
                    params.update({'gigbarter__artist__country': ctd_filter.get('country_artist')})

            if ctd_filter.get('second_date'):
                if type_dates == 'gigdate':
                    params.update({'gigdate__second_country__isnull': False})
                if type_dates == 'barter':
                    params.update({'gigbarter__second_country__isnull': False})

            if ctd_filter.get('ganre'):
                if type_dates == 'tender':
                    params.update({'gigtender__club__genre': ctd_filter.get('ganre')})
                if type_dates == 'gigdate':
                    params.update({'gigdate__artist__genre': ctd_filter.get('ganre')})
                if type_dates == 'barter':
                    params.update({'gigbarter__artist__genre': ctd_filter.get('ganre')})
        return DateAll.objects.filter(**params)


class AddGigdateCreateView(CreateView):
    """
    Добавление гигдаты
    """
    form_class = GigDateForm
    template_name = 'gigdate/add_gigdate.html'

    def get_initial(self):
        return {'user': self.request.user}

    def get_context_data(self, **kwargs):
        context = super(AddGigdateCreateView, self).get_context_data(**kwargs)
        artist = ArtistProfile.objects.filter(user=self.request.user)
        context['artist'] = artist[0] if artist else False
        context['coin_rules'] = True if self.request.user.gigcoin_sum() >= settings.ADD_GIGDATE else False
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        secondary_data = form.cleaned_data.get('secondary_data')
        if secondary_data and settings.ADD_GIGDATE_TWODAY > self.request.user.gigcoin_sum():
            from django.forms import ValidationError
            form._errors["secondary_data"] = ValidationError(_(u'Not enough GigCoins'), code='invalid')
            return self.form_invalid(form)
        obj.user = self.request.user
        obj.type_date = GigDate.TYPEDATE0
        obj.save()
        if secondary_data:
            coins = settings.ADD_GIGDATE_TWODAY
            message = _(u"Adding Gig-date and second date: <a href='%s'>%s</a>" % (obj.get_absolute_url(), obj.dates))
        else:
            coins = settings.ADD_GIGDATE
            message = _(u"Adding Gig-date: <a href='%s'>%s</a>" % (obj.get_absolute_url(), obj.dates))
        self.request.user.debit_coin(coins, messages=message)
        messages.info(self.request, _('Gig-date successfully created'))
        return super(AddGigdateCreateView, self).form_valid(form)


class AddGigBarterCreateView(CreateView):
    """
    Добавление бартера
    """
    form_class = GigBarterForm
    template_name = 'gigdate/add_gigbarter.html'

    def get_initial(self):
        return {'user': self.request.user}

    def get_context_data(self, **kwargs):
        context = super(AddGigBarterCreateView, self).get_context_data(**kwargs)
        artist = ArtistProfile.objects.filter(user=self.request.user)
        context['artist'] = artist[0] if artist else False
        context['club'] = ClubProfile.objects.filter(user=self.request.user).exists()
        context['coin_rules'] = True if self.request.user.gigcoin_sum() >= settings.ADD_BARTER else False
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        secondary_data = form.cleaned_data.get('secondary_data')
        if secondary_data and settings.ADD_GIGDATE_TWODAY > self.request.user.gigcoin_sum():
            from django.forms import ValidationError
            form._errors["secondary_data"] = ValidationError(_(u'Not enough GigCoins'), code='invalid')
            return self.form_invalid(form)
        obj.user = self.request.user
        obj.type_date = GigDate.TYPEDATE1
        obj.save()
        if secondary_data:
            coins = settings.ADD_BARTER_TWODAY
            message = _(u"Adding Barter-date and second date: <a href='%s'>%s</a>" % (obj.get_absolute_url(), obj.dates))
        else:
            coins = settings.ADD_BARTER
            message = _(u"Adding Barter-date: <a href='%s'>%s</a>" % (obj.get_absolute_url(), obj.dates))
        self.request.user.debit_coin(coins, messages=message)
        messages.info(self.request, _('Gig-barter successfully created'))
        return super(AddGigBarterCreateView, self).form_valid(form)


class AddGigTenderCreateView(CreateView):
    """
    Добавление тендера
    """
    form_class = GigTenderForm
    template_name = 'gigdate/add_gigtender.html'

    def get_initial(self):
        return {'user': self.request.user}

    def get_context_data(self, **kwargs):
        context = super(AddGigTenderCreateView, self).get_context_data(**kwargs)
        club = ClubProfile.objects.filter(user=self.request.user)
        context['club'] = club[0] if club else False
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.type_date = GigDate.TYPEDATE2
        obj.save()
        messages.info(self.request, _('Gig-tender successfully created'))
        return super(AddGigTenderCreateView, self).form_valid(form)


class GigdateDetailView(FormMixin, DetailView):
    """
    Вывод гигдаты
    """
    model = GigDate
    template_name = 'gigdate/view_gigdate.html'
    context_object_name = 'gigdate'
    form_class = TradesForm
    slug_field = 'uuid_id'
    success_url = './'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_authenticated() and not self.object.artist.user == self.request.user:
            self.object.view += 1
            self.object.save()
        return super(GigdateDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GigdateDetailView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['form'] = self.get_form()
            context['trands'] = Trades.objects.filter(user_trade=self.request.user, date_all=self.get_object()).exists()
        context['date_requests'] = context['object'].trade_date.count()
        date_contracts = context['object'].trade_date.filter(status_trade=Trades.STRADE2).count()
        context['date_contracts'] = date_contracts
        context['add_book'] = True if date_contracts >= context['object'].count_trade else False
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        if not self.request.user.is_authenticated():
            return {}
        data = {
            'user': self.request.user,
            'date_all': self.get_object().get_sub_model()
        }
        return data

    def form_valid(self, form):
        object = self.get_object()
        obj = form.save(commit=False)
        obj.date_all = object
        obj.type_date = GigDate.TYPEDATE0
        obj.user_date = object.user
        obj.user_trade = self.request.user
        obj.artist_date = object.get_sub_model().artist
        obj.save()
        form.save_m2m()
        send_mailbox(object.user, u"Gig-date request", u"Request for Your gig-date: <a href='"+reverse('account:trade_view', args=[obj.pk])+u"'>"+reverse('account:trade_view', args=[obj.pk])+u"<a/>")
        messages.info(self.request, _('Gig-date successfully requested'))
        return super(GigdateDetailView, self).form_valid(form)

    # def form_invalid(self, form):
    #     return self.render_to_response(self.get_context_data(form=form))


class GigbarterDetailView(FormMixin, DetailView):
    """
    Вывод бартера
    """
    model = GigBarter
    template_name = 'gigdate/view_gigbarter.html'
    context_object_name = 'gigdate'
    form_class = TradesBarterForm
    slug_field = 'uuid_id'
    success_url = './'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_authenticated() and not self.object.artist.user == self.request.user:
            self.object.view += 1
            self.object.save()
        return super(GigbarterDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GigbarterDetailView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['form'] = self.get_form()
            context['trands'] = Trades.objects.filter(user_trade=self.request.user, date_all=self.get_object()).exists()
        context['date_requests'] = context['object'].trade_date.count()
        date_contracts = context['object'].trade_date.filter(status_trade=Trades.STRADE2).count()
        context['date_contracts'] = date_contracts
        context['add_book'] = True if date_contracts >= context['object'].count_trade else False
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        if not self.request.user.is_authenticated():
            return {}
        data = {
            'user': self.request.user,
            'date_all': self.get_object().get_sub_model()
        }
        return data

    def form_valid(self, form):
        object = self.get_object()
        obj = form.save(commit=False)
        if settings.SEND_REQUEST_BARTER > self.request.user.gigcoin_sum():
            from django.forms import ValidationError
            form._errors["gigcoin_invalid"] = ValidationError(_(u'Not enough GigCoins'), code='invalid')
            return self.form_invalid(form)
        obj.date_all = object
        obj.type_date = GigTender.TYPEDATE1
        obj.user_date = object.user
        obj.user_trade = self.request.user
        obj.artist_date = object.get_sub_model().artist
        obj.club_date = object.get_sub_model().club
        obj.save()
        message = _(u"Sending Barter-date request: <a href='%s'>%s</a>" % (object.get_absolute_url(), object.dates))
        self.request.user.debit_coin(settings.SEND_REQUEST_BARTER, messages=message)
        send_mailbox(object.user, u"Gig-barter request", u"Request for Your gig-barter: <a href='"+reverse('account:trade_view', args=[obj.pk])+u"'>"+reverse('account:trade_view', args=[obj.pk])+u"<a/>")
        messages.info(self.request, _('Gig-barter requested'))
        return super(GigbarterDetailView, self).form_valid(form)

    # def form_invalid(self, form):
    #     return super(GigbarterDetailView, self).form_invalid(form)


class GigtenderDetailView(FormMixin, DetailView):
    """
    Вывод тендера
    """
    model = GigTender
    template_name = 'gigdate/view_gigtender.html'
    context_object_name = 'gigdate'
    form_class = TradesTenderForm
    slug_field = 'uuid_id'
    success_url = './'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_authenticated() and not self.object.club.user == self.request.user:
            self.object.view += 1
            self.object.save()
        return super(GigtenderDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GigtenderDetailView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            context['form'] = self.get_form()
            context['trands'] = Trades.objects.filter(user_trade=self.request.user, date_all=self.get_object()).exists()
        context['date_requests'] = context['object'].trade_date.count()
        date_contracts = context['object'].trade_date.filter(status_trade=Trades.STRADE2).count()
        context['date_contracts'] = date_contracts
        context['add_book'] = True if date_contracts >= context['object'].count_trade else False
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_initial(self):
        if not self.request.user.is_authenticated():
            return {}
        data = {
            'user': self.request.user,
            'date_all': self.get_object().get_sub_model()
        }
        return data

    def form_valid(self, form):
        object = self.get_object()
        obj = form.save(commit=False)
        if settings.SEND_REQUEST_TENDER > self.request.user.gigcoin_sum():
            from django.forms import ValidationError
            form._errors["gigcoin_invalid"] = ValidationError(_(u'Not enough GigCoins'), code='invalid')
            return self.form_invalid(form)
        obj.date_all = object
        obj.type_date = GigTender.TYPEDATE2
        obj.user_date = object.user
        obj.user_trade = self.request.user
        obj.club_date = object.get_sub_model().club
        obj.save()
        message = _(u"Sending Tender-date request: <a href='%s'>%s</a>" % (object.get_absolute_url(), object.dates))
        self.request.user.debit_coin(settings.SEND_REQUEST_TENDER, messages=message)
        send_mailbox(object.user, u"Gig-tender request", u"Request for Your gig-tender: <a href='"+reverse('account:trade_view', args=[obj.pk])+u"'>"+reverse('account:trade_view', args=[obj.pk])+u"<a/>")
        messages.info(self.request, _('Gig-tender requested'))
        return super(GigtenderDetailView, self).form_valid(form)

    # def form_invalid(self, form):
    #     return super(GigtenderDetailView, self).form_invalid(form)


class GigdateallListView(ListView):
    """
    Список дат в ЛК
    """
    template_name = 'gigdate/account/dates_list.html'
    context_object_name = 'gigdates'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(GigdateallListView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'dates'
        context['artists'] = ArtistProfile.objects.filter(user=self.request.user)
        context['clubs'] = ClubProfile.objects.filter(user=self.request.user)
        return context

    def get_queryset(self):
        return DateAll.objects.filter(user=self.request.user)


class GigdateallFilterListView(GigdateallListView):
    """
        Список дат в ЛК Filter club/artist
    """
    def get_context_data(self, **kwargs):
        context = super(GigdateallFilterListView, self).get_context_data(**kwargs)
        slug = self.kwargs.get('slug')
        if slug == 'artist':
            context['art_id'] = int(self.kwargs.get('pk'))
        else:
            context['clb_id'] = int(self.kwargs.get('pk'))
        return context

    def get_queryset(self):
        usre_id = self.kwargs.get('pk')
        slug = self.kwargs.get('slug')
        if slug == 'artist':
            return DateAll.objects.filter(Q(gigdate__artist=usre_id) | Q(gigbarter__artist=usre_id), user=self.request.user)
        else:
            return DateAll.objects.filter(gigtender__club=usre_id, user=self.request.user)


class GigdateUpdateView(UpdateView):
    """
    Изменение гигдаты
    """
    model = GigDate
    form_class = GigDateForm
    template_name = 'gigdate/account/edit_gigdate.html'

    def get_object(self, queryset=None):
        objects = get_object_or_404(self.model, user=self.request.user, uuid_id=self.kwargs.get("slug"))
        if 'cloning' in self.request.POST:
            objects.pk = None
        return objects

    def get_initial(self):
        return {'user': self.request.user}

    # def post(self, request, *args, **kwargs):
    #     self.object = self.get_object()
    #     form = self.get_form()
    #     if form.is_valid():
    #         if "cloning" in request.POST:
    #             return self.form_invalid(form)
    #         return self.form_valid(form)
    #     else:
    #         return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(GigdateUpdateView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'dates'
        context['artist'] = context['object'].artist
        context['artists'] = ArtistProfile.objects.filter(user=self.request.user)
        context['clubs'] = ClubProfile.objects.filter(user=self.request.user)
        context['id_obect'] = self.kwargs.get("slug")
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.type_date = GigDate.TYPEDATE0

        if 'cloning' in self.request.POST:
            dates = form.cleaned_data.get('dates')
            date = GigDate.objects.filter(artist=form.cleaned_data.get('artist'), dates=dates, type_date=GigDate.TYPEDATE0).exists()
            if date:
                form._errors['dates'] = ErrorList([_('This event is already taken')])
                return self.form_invalid(form)
            obj.id = None
            obj.uuid_id = uuid.uuid4().hex
        obj.save()
        form.save_m2m()
        if 'cloning' in self.request.POST:
            messages.info(self.request, _('Gig-date cloning'))
        else:
            messages.info(self.request, _('Gig-date updated'))
        return HttpResponseRedirect(reverse('account:edit_gigdate', args=[obj.uuid_id]))

    # def form_invalid(self, form):
    #     if 'cloning' in self.request.POST:
    #         print form
    #     return self.render_to_response(self.get_context_data())


class GigBarterUpdateView(UpdateView):
    """
    Добавление бартера
    """
    model = GigBarter
    form_class = GigBarterForm
    template_name = 'gigdate/account/edit_gigbarter.html'

    def get_object(self, queryset=None):
        objects = get_object_or_404(self.model, user=self.request.user, uuid_id=self.kwargs.get("slug"))
        if 'cloning' in self.request.POST:
            objects.pk = None
        return objects

    def get_initial(self):
        return {'user': self.request.user}

    def get_context_data(self, **kwargs):
        context = super(GigBarterUpdateView, self).get_context_data(**kwargs)
        context['artist'] = context['object'].artist
        context['club'] = ClubProfile.objects.filter(user=self.request.user).exists()
        context['submenu_url'] = 'dates'
        context['artists'] = ArtistProfile.objects.filter(user=self.request.user)
        context['clubs'] = ClubProfile.objects.filter(user=self.request.user)
        context['id_obect'] = self.kwargs.get("slug")
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.type_date = GigDate.TYPEDATE1
        if 'cloning' in self.request.POST:
            dates = form.cleaned_data.get('dates')
            date = GigBarter.objects.filter(artist=form.cleaned_data.get('artist'), dates=dates, type_date=GigDate.TYPEDATE1).exists()
            if date:
                form._errors['dates'] = ErrorList([_('This event is already taken')])
                return self.form_invalid(form)
            obj.id = None
            obj.uuid_id = uuid.uuid4().hex
        obj.save()
        form.save_m2m()
        if 'cloning' in self.request.POST:
            messages.info(self.request, _('Gig-barter cloning'))
        else:
            messages.info(self.request, _('Gig-barter updated'))
        return HttpResponseRedirect(reverse('account:edit_barter', args=[obj.uuid_id]))


class GigTenderUpdateView(UpdateView):
    """
    Добавление тендера
    """
    model = GigTender
    form_class = GigTenderForm
    template_name = 'gigdate/account/edit_gigtender.html'

    def get_object(self, queryset=None):
        objects = get_object_or_404(self.model, user=self.request.user, uuid_id=self.kwargs.get("slug"))
        if 'cloning' in self.request.POST:
            objects.pk = None
        return objects

    def get_initial(self):
        return {'user': self.request.user}

    def get_context_data(self, **kwargs):
        context = super(GigTenderUpdateView, self).get_context_data(**kwargs)
        context['club'] = context['object'].club
        context['submenu_url'] = 'dates'
        context['artists'] = ArtistProfile.objects.filter(user=self.request.user)
        context['clubs'] = ClubProfile.objects.filter(user=self.request.user)
        context['id_obect'] = self.kwargs.get("slug")
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.type_date = GigDate.TYPEDATE2
        if 'cloning' in self.request.POST:
            dates = form.cleaned_data.get('dates')
            date = GigTender.objects.filter(club=form.cleaned_data.get('club'), dates=dates, type_date=GigTender.TYPEDATE2).exists()
            if date:
                form._errors['dates'] = ErrorList([_('This event is already taken')])
                return self.form_invalid(form)
            obj.id = None
            obj.uuid_id = uuid.uuid4().hex
        obj.save()
        form.save_m2m()
        if 'cloning' in self.request.POST:
            messages.info(self.request, _('Gig-tender updated'))
        else:
            messages.info(self.request, _('Gig-tender updated'))
        return HttpResponseRedirect(reverse('account:edit_tender', args=[obj.uuid_id]))


class FilterDayFormView(FormView):
    """ Форма фильтров """
    form_class = GigDatesFilterForm

    def form_valid(self, form):
        filters = {
            'ganre': int(self.request.POST.get("ganre")) if self.request.POST.get("ganre") and int(self.request.POST.get("ganre")) != 0 else False,
            'country': int(self.request.POST.get("country")) if self.request.POST.get("country") and int(self.request.POST.get("country")) != 0 else False,
            'country_artist': int(self.request.POST.get("country_artist")) if self.request.POST.get("country_artist") and int(
                self.request.POST.get("country_artist")) != 0 else False,
            'second_date': True if self.request.POST.get("second_date") and self.request.POST.get("second_date") != 0 else False
        }
        self.request.session['ctd_filter'] = filters
        return HttpResponseRedirect(self.request.POST.get('httpreferer', '/'))


class LastListView(ListView):
    """ last all dates"""
    model = DateAll
    template_name = 'gigdate/last_all_list.html'
    paginate_by = 30


class GoDateRedirect(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        dates = date(int(kwargs['year']), int(kwargs['month']), int(kwargs['day']))
        gigdate = get_object_or_404(GigDate, artist=kwargs['artist'], dates=dates)
        self.url = gigdate.get_absolute_url()
        return super(GoDateRedirect, self).get_redirect_url(*args, **kwargs)


class GoTenderRedirect(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        dates = date(int(kwargs['year']), int(kwargs['month']), int(kwargs['day']))
        tender = get_object_or_404(GigTender, club=kwargs['venue'], dates=dates)
        self.url = tender.get_absolute_url()
        return super(GoTenderRedirect, self).get_redirect_url(*args, **kwargs)


class GigdateRedirect(RedirectView):
    permanent = False
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        dates = get_object_or_404(GigDate, pk=kwargs.get('pk'))
        return HttpResponseRedirect(reverse('gigdate:gigdate_view', kwargs={'slug': dates.uuid_id}))


class GigTenderRedirect(RedirectView):
    permanent = False
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        dates = get_object_or_404(GigTender, pk=kwargs.get('pk'))
        return HttpResponseRedirect(reverse('gigdate:tender_view', kwargs={'slug': dates.uuid_id}))


class GigBarterRedirect(RedirectView):
    permanent = False
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        dates = get_object_or_404(GigBarter, pk=kwargs.get('pk'))
        return HttpResponseRedirect(reverse('gigdate:barter_view', kwargs={'slug': dates.uuid_id}))