#-*- coding:utf-8 -*-
import uuid
from datetime import date
from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from common.models import Country, City, PaymentMethod, FormatPerformance, Genre, MethodsAdvert
from account.models import User, ArtistProfile, ClubProfile, AgentProfile


class DateAll(models.Model):
    """
    Общаяя модель объявлений (даты, бартеры, тендеры)
    """

    uuid_id         = models.UUIDField(default=uuid.uuid4, editable=False)
    create          = models.DateTimeField(_(u"#Создано"), auto_now_add=True, blank=True, null=True, editable=True )
    update          = models.DateTimeField(_(u"#Обновлено"), auto_now=True, blank=True, null=True)
    user            = models.ForeignKey(User, verbose_name=_(u'User'), blank=True, related_name="user_dateall")

    dates           = models.DateField(_(u'#Дата календаря'), blank=False)
    geography_queries = models.ManyToManyField(Country, verbose_name=_(u'geography queries'), blank=False,
                                               related_name="geography_dates")
    PLAN0, PLAN1, PLAN2, PLAN3, PLAN4 = range(0, 5)
    PLANS = (
        # (PLAN0, _(u'Any')),
        (PLAN1, _(u'100%')),
        (PLAN2, _(u'50%-50%')),
        (PLAN3, _(u'10%-90%')),
        (PLAN4, _(u'10%-40%-50%')),
    )
    payment_plan        = models.IntegerField(verbose_name=_(u'#График оплаты гонорара'), default=PLAN0, choices=PLANS, blank=False)
    EVENT0, EVENT1, EVENT2 = range(0, 3)
    EVENT_TYPE = (
        (EVENT0, _(u'Contractual fee')),
        (EVENT1, _(u'Fixed fee')),
        (EVENT2, _(u'Another fee')),
    )
    event_public        = models.IntegerField(_(u'#Мероприятие публичное'), default=EVENT0, choices=EVENT_TYPE, blank=True, null=True)
    event_private       = models.IntegerField(_(u'#Мероприятие приватное'), default=EVENT0, choices=EVENT_TYPE, blank=True, null=True)
    event_presentation  = models.IntegerField(_(u'#Мероприятие презентация'), default=EVENT0, choices=EVENT_TYPE, blank=True, null=True)
    mony_publ_fix       = models.PositiveIntegerField(_(u"#Гонорар fixed"), blank=True, null=True)
    mony_publ_oth_from  = models.PositiveIntegerField(_(u"#Гонорар другой от"), blank=True, null=True)
    mony_publ_oth_to    = models.PositiveIntegerField(_(u"#Гонорар другой до"), blank=True, null=True)
    mony_priv_fix       = models.PositiveIntegerField(_(u"#Гонорар fixed"), blank=True, null=True)
    mony_priv_oth_from  = models.PositiveIntegerField(_(u"#Гонорар другой от"), blank=True, null=True)
    mony_priv_oth_to    = models.PositiveIntegerField(_(u"#Гонорар другой до"), blank=True, null=True)
    mony_pres_fix       = models.PositiveIntegerField(_(u"#Гонорар fixed"), blank=True, null=True)
    mony_pres_oth_from  = models.PositiveIntegerField(_(u"#Гонорар другой от"), blank=True, null=True)
    mony_pres_oth_to    = models.PositiveIntegerField(_(u"#Гонорар другой до"), blank=True, null=True)
    CY0, CY1, CY2 = range(0, 3)
    CURRENCY = (
        (CY0, _(u'EUR')),
        (CY1, _(u'USD')),
        (CY2, _(u'National currency')),
    )
    currency            = models.IntegerField(_(u'#Валюта оплаты гонорара'), default=CY0, choices=CURRENCY)
    payment_method      = models.ManyToManyField(PaymentMethod, verbose_name=_(u'#Форма оплаты гонорара'), blank=False, related_name="payment_methods")
    WHOPAY0, WHOPAY1 = range(0, 2)
    PAY_RIDER = (
        (WHOPAY0, _(u'Customer expenses')),
        (WHOPAY1, _(u'Artist expenses')),
    )
    rider_payments      = models.IntegerField(_(u'#Оплата райдеров'), default=WHOPAY0, choices=PAY_RIDER)
    transport_payments  = models.IntegerField(_(u'#Транспортные расходы'), default=WHOPAY0, choices=PAY_RIDER)

    TYPEDATE0, TYPEDATE1, TYPEDATE2,  = range(0, 3)
    TYPE_DATES = (
        (TYPEDATE0, _(u'Gig-date')),
        (TYPEDATE1, _(u'Barter-date')),
        (TYPEDATE2, _(u'Tender-date')),
    )
    type_date       = models.IntegerField(_(u'#Тип даты'), default=TYPEDATE0, choices=TYPE_DATES, blank=True)
    view            = models.IntegerField(_(u"#Просмотры"), default=0, blank=True)
    banned          = models.BooleanField(_(u'#Забанен'), default=False, help_text=_(u"#Заблокирован"))

    def __unicode__(self):
        return u"%s: %s" % (self.get_type_date_display(), self.dates)

    def is_date_today(self):
        if date.today() == self.create.date():
            return True
        return False

    def username_child(self):
        if self.type_date == self.TYPEDATE0:
            return self.gigdate.artist
        elif self.type_date == self.TYPEDATE1:
            return self.gigbarter.artist
        elif self.type_date == self.TYPEDATE2:
            return self.gigtender.club
        else:
            return None

    def get_sub_model(self):
        if self.type_date == self.TYPEDATE0:
            return self.gigdate
        elif self.type_date == self.TYPEDATE1:
            return self.gigbarter
        elif self.type_date == self.TYPEDATE2:
            return self.gigtender
        else:
            return None

    def second_country(self):
        if self.type_date == self.TYPEDATE0:
            return self.gigdate.second_country
        elif self.type_date == self.TYPEDATE1:
            return self.gigbarter.second_country
        else:
            return None

    def get_absolute_url(self):
        if self.type_date == self.TYPEDATE0:
            return reverse('account:edit_gigdate', args=[self.uuid_id])
        elif self.type_date == self.TYPEDATE1:
            return reverse('account:edit_barter', args=[self.uuid_id])
        elif self.type_date == self.TYPEDATE2:
            return reverse('account:edit_tender', args=[self.uuid_id])
        else:
            return reverse('account:dates')

    def get_absolute_url_view(self):
        if self.type_date == self.TYPEDATE0:
            return reverse('gigdate:gigdate_view', args=[self.uuid_id])
        elif self.type_date == self.TYPEDATE1:
            return reverse('gigdate:barter_view', args=[self.uuid_id])
        elif self.type_date == self.TYPEDATE2:
            return reverse('gigdate:tender_view', args=[self.uuid_id])
        else:
            return reverse('home')

    class Meta:
        verbose_name = _(u'Date all')
        verbose_name_plural = _(u'Dates all')
        ordering = ['-create']


class GigDate(DateAll):
    """
    Модель гиг даты
    """
    artist              = models.ForeignKey(ArtistProfile, verbose_name=_(u'#Артист'), related_name="gigdate_artist", blank=False)

    COUNT_CHOICES = [(i, i) for i in range(1, settings.COUNT_GIGDATE + 1)]
    count_trade         = models.IntegerField(_(u'Count venue'), default=1, choices=COUNT_CHOICES, blank=False, null=True)

    second_date         = models.DateField(_(u'#Первая дата'), blank=True, null=True)
    second_country      = models.ForeignKey(Country, verbose_name=_(u'#Страна второй даты'), blank=True, null=True)
    second_city         = models.ForeignKey(City, verbose_name=_(u'#Город второй даты'), blank=True, null=True)

    contract_artist     = models.BooleanField(_(u'#Договор артиста'), default=False)
    contract_site       = models.BooleanField(_(u'#Договора gigcenter'), default=True)

    """ Дополнительная информация о мероприятии Которую нужно получить от площадки """
    queries1        = models.BooleanField(_(u'#Название мероприятия'), default=False)
    queries2        = models.BooleanField(_(u'#Тип мероприятия'), default=False)
    queries3        = models.BooleanField(_(u'#Характер мероприятия'), default=False)
    queries4        = models.BooleanField(_(u'#Закрытое или открытой место выступления'), default=False)
    queries5        = models.BooleanField(_(u'#Размер сцены'), default=False)
    queries6        = models.BooleanField(_(u'#Наличие спонсоров'), default=False)
    queries7        = models.BooleanField(_(u'#Цена входного билета'), default=False)
    queries8        = models.BooleanField(_(u'#Места распространения билетов'), default=False)
    queries9        = models.BooleanField(_(u'#Время начала и конца'), default=False)
    queries10       = models.BooleanField(_(u'#Время выступления артиста'), default=False)
    queries11       = models.BooleanField(_(u'#Время саундчека'), default=False)
    queries12       = models.BooleanField(_(u'#Лайнап мероприятия'), default=False)
    queries13       = models.BooleanField(_(u'#Методы рекламы'), default=False)
    queries14       = models.BooleanField(_(u'#Наличие танцоров/go go/перформанс'), default=False)
    queries15       = models.BooleanField(_(u'#Возраст аудитории'), default=False)
    queries16       = models.BooleanField(_(u'#Тип аудитории'), default=False)
    queries17       = models.BooleanField(_(u'#Аренда площадки'), default=False)
    queries18       = models.BooleanField(_(u'#Артисты и агенты, с которыми заказчик уже работал'), default=False)
    queries19       = models.BooleanField(_(u'#Артисты, которые подтверждены на выступление в ближайшем будущем на мероприятиях заказчика'), default=False)
    queries20       = models.BooleanField(_(u'#Артисты, которые уже выступали на площадке проведения мероприятия'), default=False)
    queries21       = models.BooleanField(_(u'#Артисты, которые подтверждены на выступление в ближайшем будущем на площадке проведения мероприятия'), default=False)

    def __unicode__(self):
        return u"%s" % self.dates

    def get_absolute_url(self):
        return reverse('gigdate:gigdate_view', args=[self.uuid_id])

    class Meta:
        verbose_name = _(u'Gig Date')
        verbose_name_plural = _(u'Gig Dates')


class GigTender(DateAll):
    """
    Модель гиг тендера
    """
    club                = models.ForeignKey(ClubProfile, verbose_name=_(u'#Площадка'), related_name="tender_club", blank=True, null=True)
    COUNT_CHOICES = [(i, i) for i in range(1, settings.COUNT_TENDER + 1)]
    count_trade         = models.IntegerField(_(u'Count artist'), default=1, choices=COUNT_CHOICES, blank=False, null=True)

    stop_dates          = models.DateField(_(u'#Окончание тендера'), blank=True, null=True)
    type_format         = models.ManyToManyField(FormatPerformance, _(u'Format Performance+'), blank=True)
    genre               = models.ManyToManyField(Genre, verbose_name=_(u'Жанр'), blank=True)

    contract_artist     = models.BooleanField(_(u'#Договор артиста'), default=False)
    # contract_club  = models.BooleanField(_(u'#Договор заказчика'), default=False)
    contract_site       = models.BooleanField(_(u'#Договора gigcenter'), default=True)

    ACT0, ACT1, ACT2, ACT3 = range(0, 4)
    TYPE_ACTIONS = (
        (ACT0, _(u'Commercial')),
        (ACT1, _(u'Noncommercial')),
        (ACT2, _(u'Charitable')),
        (ACT3, _(u'Other'))
    )

    VE0, VE1 = range(0, 2)
    VENUES = (
        (VE0, _(u'Indoor')),
        (VE1, _(u'Outdoor'))
    )
    RENT0, RENT1, RENT2 = range(0, 3)
    RENTS = (
        (RENT0, _(u'In property')),
        (RENT1, _(u'Long-term rent')),
        (RENT2, _(u'One-time rent'))
    )
    queries1        = models.CharField(_(u'#Название мероприятия'), max_length=255, null=True, blank=True)
    queries3        = models.IntegerField(_(u'#Характер мероприятия'), default=ACT0, choices=TYPE_ACTIONS)
    queries4        = models.IntegerField(_(u'#Закрытое или открытой место выступления'), default=VE0, choices=VENUES)
    queries5        = models.CharField(_(u'#Размер сцены'), max_length=255, null=True, blank=True)
    queries6        = models.CharField(_(u'#Наличие спонсоров'), max_length=255, null=True, blank=True)
    queries7        = models.CharField(_(u'#Цена входного билета'), max_length=255, null=True, blank=True)
    queries8        = models.CharField(_(u'#Места распространения билетов'), max_length=255, null=True, blank=True)
    queries9        = models.CharField(_(u'#Время начала и конца'), max_length=255, null=True, blank=True)
    queries10       = models.CharField(_(u'#Время выступления артиста'), max_length=255, null=True, blank=True)
    queries11       = models.CharField(_(u'#Время саундчека'), max_length=255, null=True, blank=True)
    queries12       = models.CharField(_(u'#Лайнап мероприятия'), max_length=255, null=True, blank=True)
    queries13       = models.ManyToManyField(MethodsAdvert, verbose_name=_(u'#Методы рекламы'), blank=False)
    queries14       = models.CharField(_(u'#Наличие танцоров/go go/перформанс'), max_length=255, null=True, blank=True)
    queries15       = models.CharField(_(u'#Возраст аудитории'), max_length=255, null=True, blank=True)
    queries16       = models.CharField(_(u'#Тип аудитории'), max_length=255, null=True, blank=True)
    queries17       = models.IntegerField(_(u'#Аренда площадки'), default=RENT0, choices=RENTS)
    queries18       = models.TextField(_(u'#Артисты и агенты, с которыми заказчик уже работал'), null=True, blank=True)
    queries19       = models.TextField(_(u'#Артисты, которые подтверждены на выступление в ближайшем будущем на мероприятиях заказчика'), null=True, blank=True)
    queries20       = models.TextField(_(u'#Артисты, которые уже выступали на площадке проведения мероприятия'), null=True, blank=True)
    queries21       = models.TextField(_(u'#Артисты, которые подтверждены на выступление в ближайшем будущем на площадке проведения мероприятия'), null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.dates

    def get_absolute_url(self):
        return reverse('gigdate:tender_view', args=[self.uuid_id])

    class Meta:
        verbose_name = _(u'Gig Tender')
        verbose_name_plural = _(u'Gig Tender')


class GigBarter(DateAll):
    """
    Модель гиг тендера
    """
    artist              = models.ForeignKey(ArtistProfile, verbose_name=_(u'#Артист'), related_name="barter_artist", blank=False)
    COUNT_CHOICES = [(i, i) for i in range(1, settings.COUNT_BARTER + 1)]
    count_trade         = models.IntegerField(_(u'Count artist'), default=1, choices=COUNT_CHOICES, blank=False, null=True)

    second_date         = models.DateField(_(u'#Первая дата'), blank=True, null=True)
    second_country      = models.ForeignKey(Country, verbose_name=_(u'#Страна второй даты'), blank=True, null=True)
    second_city         = models.ForeignKey(City, verbose_name=_(u'#Город второй даты'), blank=True, null=True)

    club                = models.ForeignKey(ClubProfile, verbose_name=_(u'#Площадка'), related_name="barter_club", blank=True, null=True)
    SCORE_CHOICES = [(i, i) for i in range(1, 101)]
    reply_speech        = models.IntegerField(_(u'#Ответное выступление'), default=1, choices=SCORE_CHOICES, blank=False, null=True)
    contract_artist     = models.BooleanField(_(u'#Договор артиста'), default=False)
    contract_site       = models.BooleanField(_(u'#Договора gigcenter'), default=True)

    """ Инфо ответного выступления """
    barter_contract_artist     = models.BooleanField(_(u'#Договор артиста'), default=False)
    barter_contract_site       = models.BooleanField(_(u'#Договора gigcenter'), default=True)

    PLAN0, PLAN1, PLAN2, PLAN3, PLAN4 = range(0, 5)
    PLANS = (
        # (PLAN0, _(u'Any')),
        (PLAN1, _(u'100%')),
        (PLAN2, _(u'50%-50%')),
        (PLAN3, _(u'10%-90%')),
        (PLAN4, _(u'10%-40%-50%')),
    )
    barter_payment_plan        = models.IntegerField(verbose_name=_(u'#График оплаты гонорара'), default=PLAN0, choices=PLANS)
    EVENT0, EVENT1, EVENT2 = range(0, 3)
    EVENT_TYPE = (
        (EVENT0, _(u'Contractual fee')),
        (EVENT1, _(u'Fixed fee')),
        (EVENT2, _(u'Another fee')),
    )
    barter_event_public        = models.IntegerField(_(u'#Мероприятие публичное'), default=EVENT0, choices=EVENT_TYPE, blank=True, null=True)
    barter_event_private       = models.IntegerField(_(u'#Мероприятие приватное'), default=EVENT0, choices=EVENT_TYPE, blank=True, null=True)
    barter_event_presentation  = models.IntegerField(_(u'#Мероприятие презентация'), default=EVENT0, choices=EVENT_TYPE, blank=True, null=True)
    barter_mony_publ_fix       = models.PositiveIntegerField(_(u"#Гонорар fixed"), blank=True, null=True)
    barter_mony_publ_oth_from  = models.PositiveIntegerField(_(u"#Гонорар другой от"), blank=True, null=True)
    barter_mony_publ_oth_to    = models.PositiveIntegerField(_(u"#Гонорар другой до"), blank=True, null=True)
    barter_mony_priv_fix       = models.PositiveIntegerField(_(u"#Гонорар fixed"), blank=True, null=True)
    barter_mony_priv_oth_from  = models.PositiveIntegerField(_(u"#Гонорар другой от"), blank=True, null=True)
    barter_mony_priv_oth_to    = models.PositiveIntegerField(_(u"#Гонорар другой до"), blank=True, null=True)
    barter_mony_pres_fix       = models.PositiveIntegerField(_(u"#Гонорар fixed"), blank=True, null=True)
    barter_mony_pres_oth_from  = models.PositiveIntegerField(_(u"#Гонорар другой от"), blank=True, null=True)
    barter_mony_pres_oth_to    = models.PositiveIntegerField(_(u"#Гонорар другой до"), blank=True, null=True)
    CY0, CY1, CY2 = range(0, 3)
    CURRENCY = (
        (CY0, _(u'EUR')),
        (CY1, _(u'USD')),
        (CY2, _(u'National currency')),
    )
    barter_currency            = models.IntegerField(_(u'#Валюта оплаты гонорара'), default=CY0, choices=CURRENCY)
    barter_payment_method      = models.ManyToManyField(PaymentMethod, verbose_name=_(u'#Форма оплаты гонорара'), blank=False, related_name="barter_payment_methods")
    WHOPAY0, WHOPAY1 = range(0, 2)
    PAY_RIDER = (
        (WHOPAY0, _(u'Customer expenses')),
        (WHOPAY1, _(u'Artist expenses')),
    )
    barter_rider_payments      = models.IntegerField(_(u'#Оплата райдеров'), default=WHOPAY0, choices=PAY_RIDER)
    barter_transport_payments  = models.IntegerField(_(u'#Транспортные расходы'), default=WHOPAY0, choices=PAY_RIDER)

    def __unicode__(self):
        return u"%s" % self.dates

    def get_absolute_url(self):
        return reverse('gigdate:barter_view', args=[self.uuid_id])

    class Meta:
        verbose_name = _(u'Gig Barter')
        verbose_name_plural = _(u'Gig Barter')