# -*- coding:utf-8 -*-
from django.views.generic import DetailView, ListView, View
from django.views.generic.edit import FormView, UpdateView, DeleteView
from django.views.generic.base import RedirectView, TemplateView
from django.utils.translation import ugettext_lazy as _
from django.http import JsonResponse
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages

from contents.models import Pages, FAQ, News, Favorites, Bookmarks
from common.models import FaqCategory
from crm.forms import TicketForm
from contents.forms import TestSendMailForm


class PageDetailView(DetailView):
    """
    Выводим статичную страницу с текстом
    """
    model = Pages
    context_object_name = 'page'
    slug_field = 'url'
    query_pk_and_slug = True
    template_name = 'contents/page.html'


class FAQListView(ListView):
    """
    Выводим список FAQ
    """
    model = FaqCategory
    template_name = 'contents/faqcategory_list.html'


class FAQDetailView(DetailView):
    """
    Выводим статичную FAQ
    """
    model = FAQ
    context_object_name = 'page'
    template_name = 'contents/faq.html'


class NewsDetailView(DetailView):
    model = News
    slug_field = 'url'
    query_pk_and_slug = True


class NewsListView(ListView):
    model = News
    paginate_by = 10


class AjaxFavorites(View):
    """
    GET: /add_favorites/user_id_type_data/
    model: Favorites
    """

    def get(self, request, *args, **kwargs):
        user_id = int(kwargs.get('user_id'))
        type_data = kwargs.get('type_data')
        status = 'error'
        if type_data == 'artist':
            status = 'ok'
            if Favorites.objects.filter(user=request.user, artist_id=user_id, type_data=Favorites.TYPEACCOUNTS0).exists():
                message = u'old_user'
            else:
                favorites = Favorites(user=request.user, artist_id=user_id, type_data=Favorites.TYPEACCOUNTS0)
                favorites.save()
                message = u'new_user'
        elif type_data == 'agent':
            status = 'ok'
            if Favorites.objects.filter(user=request.user, agent_id=user_id, type_data=Favorites.TYPEACCOUNTS1).exists():
                message = u'old_user'
            else:
                favorites = Favorites(user=request.user, agent_id=user_id, type_data=Favorites.TYPEACCOUNTS1)
                favorites.save()
                message = u'new_user'
        elif type_data == 'club':
            status = 'ok'
            if Favorites.objects.filter(user=request.user, club_id=user_id, type_data=Favorites.TYPEACCOUNTS2).exists():
                message = u'old_user'
            else:
                favorites = Favorites(user=request.user, club_id=user_id, type_data=Favorites.TYPEACCOUNTS2)
                favorites.save()
                message = u'new_user'
        return JsonResponse({'status': status, 'message': message})


class AjaxBookmarks(View):
    """
    GET: /add_bookmarks/user_id_type_data/
    model: Bookmarks
    """

    def get(self, request, *args, **kwargs):
        user_id = int(kwargs.get('user_id'))
        type_data = kwargs.get('type_data')
        if type_data == 'gigdate':
            type_data = Bookmarks.TYPEDATE0
        elif type_data == 'barter':
            type_data = Bookmarks.TYPEDATE1
        elif type_data == 'tender':
            type_data = Bookmarks.TYPEDATE2
        else:
            type_data = None
        if Bookmarks.objects.filter(user=request.user, date_all_id=user_id, type_data=type_data).exists():
            message = u'old_user'
        else:
            bookmarks = Bookmarks(user=request.user, date_all_id=user_id, type_data=type_data)
            bookmarks.save()
            message = u'new_user'
        return JsonResponse({'status': 'ok', 'message': message})


class FavoritesListView(ListView):
    paginate_by = 20

    def get_queryset(self):
        if self.request.user.is_authenticated():
            return Favorites.objects.filter(user=self.request.user)
        else:
            return Favorites.objects.none()


class FavoritesDeleteView(DeleteView):
    model = Favorites
    success_url = reverse_lazy('contents:favorite_list')


class BookmarksListView(ListView):
    paginate_by = 20

    def get_queryset(self):
        if self.request.user.is_authenticated():
            return Bookmarks.objects.filter(user=self.request.user)
        else:
            return Bookmarks.objects.none()


class BookmarksDeleteView(DeleteView):
    model = Bookmarks
    success_url = reverse_lazy('contents:bookmark_list')


class TicketFormView(FormView):
    template_name = 'elements/ticket_view.html'
    form_class = TicketForm
    success_url = reverse_lazy('contents:support_final')

    def tiket_email(self, subject, message, to_email, from_email=None):
            send_mail(subject, message, from_email, [to_email])

    def form_valid(self, form):
        f = form.save(commit=False)
        if self.request.user.is_authenticated():
            f.user = self.request.user
        f.page = self.request.get_full_path()
        f.save()
        email = form.cleaned_data['email']
        message = u"%s: %s\n\r%s: %s\n\r%s: %s\n\r%s: %s" % (
            _(u"From"), self.request.user.email if self.request.user.is_authenticated() else email,
            _(u"Subject"), form.cleaned_data['subject'],
            _(u"Related Issue"), f.get_category_display(),
            _(u"Message"), form.cleaned_data['text'])
        subject = u"%s #%s" % (_('Ticket'), f.pk)
        to_email = settings.EMAIL_TICKETS
        # to_email = settings.EMAIL_TICKETS
        self.tiket_email(subject, message, to_email)
        # send_mail(subject=subject, message=message, from_email=from_email, recipient_list=[to_email])
        return super(TicketFormView, self).form_valid(form)


class FinalView(TemplateView):
    template_name = 'elements/ticket_end.html'


class TestSendMail(FormView):
    template_name = 'elements/test_send_mail.html'
    form_class = TestSendMailForm
    success_url = reverse_lazy('contents:test_send_email')

    def form_valid(self, form):
        subject = form.cleaned_data['subject']
        message = form.cleaned_data['message']
        email_from = form.cleaned_data['email_from']
        email_from = email_from if email_from and not 'False' == email_from else None
        if form.cleaned_data['raise_error']:
            return Exception
        email_to = form.cleaned_data['email_to']
        send_mail(subject, message, email_from, [email_to])
        messages.info(self.request, _('OK message send - from: %s' % email_from))
        return super(TestSendMail, self).form_valid(form)


class LandingPage(TemplateView):
    template_name = 'contents/landingpage.html'


class LandingPage2(TemplateView):
    template_name = 'contents/landingpage2.html'