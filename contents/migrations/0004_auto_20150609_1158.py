# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0003_auto_20150609_1016'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='news',
            options={'ordering': ('-create',), 'verbose_name': 'News', 'verbose_name_plural': 'News'},
        ),
    ]
