# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0004_faqcategory'),
        ('contents', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='faq',
            name='category',
            field=models.ForeignKey(default=1, verbose_name='Category', blank=True, to='common.FaqCategory'),
            preserve_default=False,
        ),
    ]
