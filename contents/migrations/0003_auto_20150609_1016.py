# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
import common.files


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0002_faq_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='#\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e')),
                ('show', models.BooleanField(default=True, verbose_name='#\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c')),
                ('name', models.CharField(max_length=255, null=True, verbose_name='#\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('images', models.ImageField(upload_to=common.files.get_file_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('min_text', models.TextField(null=True, verbose_name='\u041f\u0440\u0435\u0432\u044c\u044e', blank=True)),
                ('text', tinymce.models.HTMLField(null=True, verbose_name='#\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('url', models.SlugField(max_length=255, null=True, verbose_name='#\u0427\u041f\u0423 URL', blank=True)),
                ('title', models.CharField(max_length=255, null=True, verbose_name='#\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043e\u043a\u043d\u0430', blank=True)),
                ('keywords', models.CharField(max_length=255, null=True, verbose_name='#\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435', blank=True)),
                ('description', models.CharField(max_length=255, null=True, verbose_name='#\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'ordering': ('-create',),
                'verbose_name': '\u041d\u043e\u0432\u043e\u0441\u0442m',
                'verbose_name_plural': '\u041d\u043e\u0432\u043e\u0441\u0442\u0438',
            },
        ),
        migrations.CreateModel(
            name='UploadFiles',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0444\u0430\u0439\u043b\u0430')),
                ('file', models.FileField(upload_to=common.files.get_file_path, null=True, verbose_name='\u0424\u0430\u0439\u043b', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
            options={
                'verbose_name': '#\u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0444\u0430\u0439\u043b\u043e\u0432',
                'verbose_name_plural': '#\u0417\u0430\u0433\u0440\u0443\u0437\u043a\u0430 \u0444\u0430\u0439\u043b\u043e\u0432',
            },
        ),
        migrations.AlterField(
            model_name='faq',
            name='category',
            field=models.ForeignKey(related_name='faq_category', verbose_name='Category', to='common.FaqCategory'),
        ),
        migrations.AlterUniqueTogether(
            name='news',
            unique_together=set([('url',)]),
        ),
    ]
