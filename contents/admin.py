# -*- coding:utf-8 -*-
from django.contrib import admin
from django.db import models
from django.forms import TextInput
from contents.models import SeoMixin, Pages, FAQ, News, UploadFiles, Favorites, Bookmarks


class SeoMixinAdmin(admin.ModelAdmin):
    list_display = ('seo_title',)
    search_fields = ('seo_title',)
    formfield_overrides = {models.CharField: {'widget': TextInput(attrs={'size': '94'})}}


class PagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'show', 'create', 'update')
    list_display_links = ('name',)
    list_filter = ('show',)
    search_fields = ['name']
    formfield_overrides = {models.CharField: {'widget': TextInput(attrs={'size': '94'})}}

    fieldsets = (
        (u"Общее", {'fields': ('name', 'min_text', 'text')}),
        (u"SEO", {'fields': ('seo_title', 'seo_keywords', 'seo_description', 'url')}),
        (u"Настройки", {'fields': ('show',)}),
    )


class FAQAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort')
    search_fields = ('name',)
    list_editable = ('sort',)
    formfield_overrides = {models.CharField: {'widget': TextInput(attrs={'size': '94'})}}

admin.site.register(SeoMixin, SeoMixinAdmin)
admin.site.register(Pages, PagesAdmin)
admin.site.register(FAQ, FAQAdmin)
admin.site.register(News)
admin.site.register(UploadFiles)

admin.site.register(Favorites)
admin.site.register(Bookmarks)