# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class TestSendMailForm(forms.Form):
    email_from = forms.ChoiceField(choices=(
        (settings.EMAIL_TICKETS, settings.EMAIL_TICKETS),
        (False, settings.DEFAULT_FROM_EMAIL)
    ), required=False,)
    email_to = forms.EmailField()
    subject = forms.CharField(initial='default subject')
    message = forms.CharField(initial='default text')
    raise_error = forms.BooleanField(required=False)