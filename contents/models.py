# -*- coding:utf-8 -*-
import re
import unidecode
from django.db import models
from tinymce.models import HTMLField
from PIL import Image
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from common.models import FaqCategory
from common.files import get_file_path
from account.models import User, ArtistProfile, AgentProfile, ClubProfile


class SeoMixin(models.Model):
    """
    Модель настроек СЕО
    """
    create          = models.DateTimeField(_(u"Создано"), auto_now_add=True)
    update          = models.DateTimeField(_(u"Обновлено"), auto_now=True)

    seo_title = models.CharField(_('title'), max_length=255, blank=True)
    seo_keywords = models.CharField(_('keywords'), max_length=255, blank=True)
    seo_description = models.CharField(_('description'), max_length=1000,
                                       blank=True)

    def __unicode__(self):
        return self.seo_title

    class Meta:
        verbose_name = _('SeoMixin')
        verbose_name_plural = _('SeoMixin')


class Pages(models.Model):
    """
    Статичные страницы
    """

    create          = models.DateTimeField(_(u"Создано"), auto_now_add=True)
    update          = models.DateTimeField(_(u"Обновлено"), auto_now=True)
    show            = models.BooleanField(_(u'Отображать'), default=True)

    name            = models.CharField(_(u'Название'), max_length=255, null=True, blank=False)
    min_text        = models.TextField(_(u'Краткое описание'), null=True, blank=True)
    text            = HTMLField(_(u'Подробное описание'), null=True, blank=True)
    # images          = models.ImageField(verbose_name=u'Изображение', upload_to=get_file_path, blank=True, null=True)

    url             = models.SlugField(_(u'ЧПУ URL'), max_length=255, null=True, blank=True)
    seo_title       = models.CharField(_(u'Заголовок окна'), max_length=255, blank=True, null=True)
    seo_keywords    = models.CharField(_(u'Ключевые'), max_length=255, blank=True, null=True)
    seo_description = models.CharField(_(u'Описание'), max_length=255, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('contents:page', args=[self.url, self.pk])

    def save(self, size=(100, 100), *args, **kwargs):
        # Если нет титлы, воруем из названия категории
        if not self.seo_title:
            self.seo_title = self.name
        # Если нет ключевиков, воруем из названия категории
        if not self.seo_keywords:
            self.seo_keywords = self.name
        # Если нет описания, воруем из названия категории
        if not self.seo_description:
            self.seo_description = self.name
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(Pages, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'Pages'
        verbose_name_plural = u'Pages'
        ordering = ('-create',)


class FAQ(models.Model):
    """
    Статичные страницы
    """

    create          = models.DateTimeField(_(u"Создано"), auto_now_add=True)
    update          = models.DateTimeField(_(u"Обновлено"), auto_now=True)
    show            = models.BooleanField(_(u'Отображать'), default=True)
    sort            = models.IntegerField(_(u"#Порядок сортировки"), default=100, blank=True,
                                        help_text=_(u"Возможны отрицательные значения"))

    category        = models.ForeignKey(FaqCategory, verbose_name=_(u'Category'), blank=False, related_name='faq_category')

    name            = models.CharField(_(u'Вопрос'), max_length=255, null=True, blank=False,
                                        help_text=_(u"Вопрос, на который будет ответ ниже"))
    min_text        = models.TextField(_(u'Краткий ответ'), null=True, blank=True)
    text            = HTMLField(_(u'Подробный ответ'), null=True, blank=True)

    seo_title       = models.CharField(_(u'Заголовок окна'), max_length=255, blank=True, null=True)
    seo_keywords    = models.CharField(_(u'Ключевые'), max_length=255, blank=True, null=True)
    seo_description = models.CharField(_(u'Описание'), max_length=255, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('contents:faq', args=[self.pk])

    def save(self, size=(100, 100), *args, **kwargs):
        # Если нет титлы, воруем из названия категории
        if not self.seo_title:
            self.seo_title = self.name
        # Если нет ключевиков, воруем из названия категории
        if not self.seo_keywords:
            self.seo_keywords = self.name
        # Если нет описания, воруем из названия категории
        if not self.seo_description:
            self.seo_description = self.name
        super(FAQ, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'FAQ'
        verbose_name_plural = u'FAQ'
        ordering = ('-sort',)


class News(models.Model):
    """
    Статичные страницы
    """
    create          = models.DateTimeField(_(u'#Создано'), auto_now_add=True)
    update          = models.DateTimeField(_(u'#Обновлено'), auto_now=True)
    show            = models.BooleanField(_(u'#Показывать'), default=True)
    name            = models.CharField(_(u'#Название'), max_length=255, null=True, blank=False)
    images          = models.ImageField(_(u'Изображение'), upload_to=get_file_path, blank=True, null=True)
    min_text        = models.TextField(_(u'Превью'), null=True, blank=True)
    text            = HTMLField(_(u'#Подробное описание'), null=True, blank=True)

    url             = models.SlugField(_(u'#ЧПУ URL'), max_length=255, null=True, blank=True)
    title           = models.CharField(_(u'#Заголовок окна'), max_length=255, blank=True, null=True)
    keywords        = models.CharField(_(u'#Ключевые'), max_length=255, blank=True, null=True)
    description     = models.CharField(_(u'#Описание'), max_length=255, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.name

    def get_absolute_url(self):
        return reverse('contents:news_index', args=[self.url, self.pk])

    def save(self, size=(150, 150), *args, **kwargs):
        # Если нет титлы, воруем из названия категории
        if not self.title:
            self.title = self.name
        # Если нет ключевиков, воруем из названия категории
        if not self.keywords:
            self.keywords = self.name
        # Если нет описания, воруем из названия категории
        if not self.description:
            self.description = self.name
        # Если нет ЧПУи
        if not self.url:
            self.url = re.sub("[^0-9a-zA-Z_]", "", unidecode.unidecode(self.name.replace(" ", "_")))
        super(News, self).save(*args, **kwargs)

        if self.images:
            # Ресайзим картинку если она пришла
            # print u"-= Алярм, картынка ресайзится при каждом сохранении категории =-"
            filename = self.images.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    class Meta:
        verbose_name = _(u'News')
        verbose_name_plural = _(u'News')
        ordering = ('-create',)
        unique_together = ('url',)


class UploadFiles(models.Model):
    """ Модель загрузки файлов """

    name            = models.CharField(_(u'Название файла'), max_length=255)
    file            = models.FileField(_(u'Файл'), upload_to=get_file_path, blank=True, null=True)
    created         = models.DateTimeField(_(u'Дата создания'), auto_now_add=True,)

    def get_absolute_url(self):
        return u"%s%s" % (settings.MEDIA_URL, self.file)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = _(u'#Загрузка файлов')
        verbose_name_plural = _(u'#Загрузка файлов')


class Favorites(models.Model):
    """ Избранное профилей """

    user = models.ForeignKey(User, verbose_name=_(u'User'), null=True, related_name="users_favorites")
    artist = models.ForeignKey(ArtistProfile, verbose_name=_(u'#Артист'), blank=True, null=True, related_name="favorites_artist")
    agent = models.ForeignKey(AgentProfile, verbose_name=_(u'#Агент'), blank=True, null=True, related_name="favorites_agent")
    club = models.ForeignKey(ClubProfile, verbose_name=_(u'#Клуб'), blank=True, null=True, related_name="favorites_club")

    TYPEACCOUNTS0, TYPEACCOUNTS1, TYPEACCOUNTS2 = range(0, 3)
    TYPEACCOUNTS = (
        (TYPEACCOUNTS0, _(u'Artist')),
        (TYPEACCOUNTS1, _(u'Agent')),
        (TYPEACCOUNTS2, _(u'Venue '))
    )
    type_data = models.IntegerField(_(u'#Тип'), choices=TYPEACCOUNTS)

    create = models.DateTimeField(_(u"#Создано"), auto_now_add=True, blank=True, null=True)
    update = models.DateTimeField(_(u"#Обновлено"), auto_now=True, blank=True, null=True)

    def __unicode__(self):
        return u"%s %s" % (self.user, self.get_type_data_display())

    def get_profile(self):
        if self.type_data == self.TYPEACCOUNTS0:
            return self.artist
        elif self.type_data == self.TYPEACCOUNTS1:
            return self.agent
        elif self.type_data == self.TYPEACCOUNTS2:
            return self.club
        else:
            return None

    class Meta:
        verbose_name = _(u'#Избранное профилей')
        verbose_name_plural = _(u'Избранное профилей')


class Bookmarks(models.Model):
    """ Закладки на даты """
    from gigdate.models import DateAll
    user = models.ForeignKey(User, verbose_name=_(u'User'), null=True, related_name="users_bookmarks")
    date_all = models.ForeignKey(DateAll, verbose_name=_(u'#Дата'), blank=True, null=True, related_name="bookmarks_date")

    TYPEDATE0, TYPEDATE1, TYPEDATE2 = range(0, 3)
    TYPEDATES = (
        (TYPEDATE0, _(u'#Гиг Дата')),
        (TYPEDATE1, _(u'#Бартер')),
        (TYPEDATE2, _(u'#Тендер '))
    )
    type_data = models.IntegerField(_(u'#Тип'), choices=TYPEDATES)
    create = models.DateTimeField(_(u"#Создано"), auto_now_add=True, blank=True, null=True)
    update = models.DateTimeField(_(u"#Обновлено"), auto_now=True, blank=True, null=True)

    def __unicode__(self):
        return u"%s - %s" % (self.user, self.get_type_data_display())

    class Meta:
        verbose_name = _(u'#Закладки на даты')
        verbose_name_plural = _(u'Закладки на даты')