# -*- coding:utf-8 -*-
from django.conf.urls import url
from contents.views import PageDetailView, FAQDetailView, FAQListView, NewsDetailView, NewsListView, AjaxFavorites,\
    AjaxBookmarks, FavoritesListView, FavoritesDeleteView, BookmarksListView, BookmarksDeleteView,\
    TicketFormView, FinalView, TestSendMail


urlpatterns = [
    url(r'^page/(?P<slug>\w+)_(?P<pk>\d+)/$', PageDetailView.as_view(), name='page'),
    url(r'^faq/(?P<pk>\d+)/$', FAQDetailView.as_view(), name='faq'),
    url(r'^faq/$', FAQListView.as_view(), name='faqs'),

    url(r'^news/$', NewsListView.as_view(), name='news'),
    url(r'^news/(?P<slug>\w+)_(?P<pk>\d+)/$', NewsDetailView.as_view(), name='news_index'),

    url(r'^add_favorites/(?P<user_id>[0-9]+)_(?P<type_data>\w+)/$', AjaxFavorites.as_view(), name='ajax_favorite'),
    url(r'^add_bookmarks/(?P<user_id>[0-9]+)_(?P<type_data>\w+)/$', AjaxBookmarks.as_view(), name='ajax_bookmark'),
    url(r'^favorite/$', FavoritesListView.as_view(), name='favorite_list'),
    url(r'^favorite/(?P<pk>\d+)/$', FavoritesDeleteView.as_view(), name='favorite_delete'),
    url(r'^bookmarks/$', BookmarksListView.as_view(), name='bookmark_list'),
    url(r'^bookmarks/(?P<pk>\d+)/$', BookmarksDeleteView.as_view(), name='bookmark_delete'),

    url(r'^support/$', TicketFormView.as_view(), name='support'),
    url(r'^support/final/$', FinalView.as_view(), name='support_final'),
    url(r'^test_send_email/$', TestSendMail.as_view(), name='test_send_email'),
]