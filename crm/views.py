# -*- coding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic import DetailView, ListView, View
from django.views.generic.edit import FormView, UpdateView
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db.models import Q
from django.core.mail import send_mail
from account.models import User, AgentProfile, ArtistProfile, ClubProfile
from gigdate.models import DateAll
from trade.models import Trades
from crm.models import Ticket
from crm.forms import ReTicketForm


class HomeCRM(TemplateView):
    """ Main CRM """
    template_name = 'crm/home.html'

    def get_context_data(self, **kwargs):
        kwargs['count_user'] = User.objects.all().count()
        kwargs['count_dates'] = DateAll.objects.all().count()
        kwargs['count_request'] = Trades.objects.all().count()
        kwargs['count_deals'] = Trades.objects.filter(status_trade=Trades.STRADE2).count()
        return kwargs


class TicketListView(ListView):
    """ Tikets """
    template_name = 'crm/tickets/ticket_list.html'
    model = Ticket
    paginate_by = 40

    def get_context_data(self, **kwargs):
        context = super(TicketListView, self).get_context_data(**kwargs)
        context['ticket_site'] = True
        return context

    def get_queryset(self):
        return self.model.objects.filter(parent__isnull=True)


class TicketDetailView(FormView, DetailView):
    model = Ticket
    template_name = 'crm/tickets/ticket_view.html'
    form_class = ReTicketForm

    def get_success_url(self):
        return reverse('crm:ticket_views', kwargs={'pk': self.object.pk})

    # def get(self, request, *args, **kwargs):
    #     self.object = self.get_object()
    #     if self.object.read:
    #         self.object.read = False
    #         self.object.save()
    #     return super(TicketDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TicketDetailView, self).get_context_data(**kwargs)
        context['form'] = self.get_form()
        context['ticket_site'] = True
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        # Here, we would record the user's interest using the message
        # passed in form.cleaned_data['message']
        f = form.save(commit=False)
        self.object = self.get_object()
        f.parent = self.object
        f.subject = u"%s %s" % (_(u"Re:"), self.object.subject)
        f.category = self.object.category
        f.user = self.request.user
        f.save()

        message = u">%s: %s \n\r>%s: %s \n\r>%s: %s\n\r\n\r-------\n\r%s: %s" % (_(u"Subject"), f.parent.subject,
                                                    _(u"Related Issue"), f.parent.get_category_display(),
                                                    _(u"Message"), f.parent.text,
                                                    _(u"The answer"), f.text)
        subject = u"%s #%s" % (_('Ticket'), f.pk)
        to_email = f.user.email if f.user else f.email
        from_email = settings.EMAIL_TICKETS
        send_mail(subject, message, from_email, [to_email])

        return super(TicketDetailView, self).form_valid(form)


class ChartUserStaticJS(TemplateView):
    #  https://google-developers.appspot.com/chart/interactive/docs/gallery/linechart
    content_type = 'text/plain; charset=utf-8'
    template_name = 'crm/chart/user_static.html'

    def get_users_statistics(self):
        users = User.objects.all().only('date_joined', 'is_active')
        # print users.query

    def get_context_data(self, **kwargs):
        context = super(ChartUserStaticJS, self).get_context_data(**kwargs)
        # context['content'] = robots[0] if robots else None
        self.get_users_statistics()
        return context


class UsersVenuesListView(ListView):
    """ Tikets """
    template_name = 'crm/users/venues_list.html'
    model = ClubProfile
    paginate_by = 40
    ordering = '-create'

    def get_context_data(self, **kwargs):
        context = super(UsersVenuesListView, self).get_context_data(**kwargs)
        context['users_venues'] = True
        return context


class UsersVenuesDetailView(DetailView):
    template_name = 'crm/users/venues_detail.html'
    model = ClubProfile

    def get_context_data(self, **kwargs):
        context = super(UsersVenuesDetailView, self).get_context_data(**kwargs)
        context['users_venues'] = True
        context['trades_inbox'] = Trades.objects.filter(club_date=context['object'])
        context['trades_outbox'] = Trades.objects.filter(club_trade=context['object'])
        context['trades_deals'] = Trades.objects.filter(Q(club_date=context['object']) | Q(club_trade=context['object']), Q(status_trade=Trades.STRADE2))
        context['trades_rejected'] = Trades.objects.filter(Q(club_date=context['object']) | Q(club_trade=context['object']), Q(status_trade=Trades.STRADE4))
        return context