# -*- coding:utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import user_passes_test
from crm.views import HomeCRM, TicketListView, TicketDetailView, ChartUserStaticJS, UsersVenuesListView,\
    UsersVenuesDetailView


urlpatterns = [
    url(r'^$', user_passes_test(lambda u: u.is_superuser or u.is_moderator)(HomeCRM.as_view()), name='home'),
    url(r'^tickets/$', user_passes_test(lambda u: u.is_superuser or u.is_moderator)(TicketListView.as_view()),
        name='tickets'),
    url(r'^ticket/(?P<pk>\d+)/$',
        user_passes_test(lambda u: u.is_superuser or u.is_moderator)(TicketDetailView.as_view()), name='ticket_views'),
    url(r'^chart/user_static.js$', user_passes_test(lambda u: u.is_superuser or u.is_moderator)(ChartUserStaticJS.as_view()),
        name='chart_user_static'),

    url(r'^users/venues/$', user_passes_test(lambda u: u.is_superuser or u.is_moderator)(UsersVenuesListView.as_view()),
        name='users_venues'),
    url(r'^users/venues/(?P<pk>\d+)/$', user_passes_test(lambda u: u.is_superuser or u.is_moderator)(UsersVenuesDetailView.as_view()),
        name='users_venues_view'),
]