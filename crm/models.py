# -*- coding:utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from account.models import User


class Ticket(models.Model):
    """ Tickets """
    create = models.DateTimeField(_(u'Create'), auto_now_add=True, blank=True, null=True)
    read = models.BooleanField(_(u"Read"), default=True, blank=True)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='ticket_children')
    page = models.CharField(_(u'Page url'), max_length=250, null=True, blank=True)

    user = models.ForeignKey(User, verbose_name=_(u'User'), null=True, blank=True)
    email = models.EmailField(_(u'email'), null=True, blank=True)
    subject = models.CharField(_(u'Subject'), max_length=100, null=True, blank=False)
    text = models.TextField(_(u'Text'), null=True, blank=False)

    SPCT0, SPCT1, SPCT2, SPCT3, SPCT4, SPCT5 = range(0, 6)
    SPCTS = (
        (SPCT0, _(u'Advertising / Partnerships / Sponsorships')),
        (SPCT1, _(u'Deals')),
        (SPCT2, _(u'Registration & Profiles')),
        (SPCT3, _(u'Requests')),
        (SPCT4, _(u'Vacancies')),
        (SPCT5, _(u'Other')),
    )
    category = models.IntegerField(verbose_name=_(u'Support category'), choices=SPCTS, blank=False)

    def __unicode__(self):
        return u"%s" % self.subject

    def get_absolute_url(self):
        return reverse('crm:ticket_views', args=[self.pk])

    class Meta:
        verbose_name = _(u'Tickets')
        verbose_name_plural = _(u'Tickets')
        ordering = ('-create',)