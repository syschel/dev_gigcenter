# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0005_auto_20150930_1902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='subject',
            field=models.CharField(max_length=100, null=True, verbose_name='Subject'),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='text',
            field=models.TextField(null=True, verbose_name='Text'),
        ),
    ]
