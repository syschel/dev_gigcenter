# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0003_auto_20150928_1141'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='category',
            field=models.IntegerField(default=0, verbose_name='Support category', choices=[(0, 'Advertising / Partnerships / Sponsorships'), (1, 'Deals'), (2, 'Registration & Profiles'), (3, 'Requests'), (4, 'Vacancies'), (5, 'Other')]),
            preserve_default=False,
        ),
    ]
