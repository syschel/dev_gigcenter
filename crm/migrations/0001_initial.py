# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='Create', null=True)),
                ('read', models.BooleanField(default=True, verbose_name='Read')),
                ('subject', models.CharField(max_length=250, null=True, verbose_name='Subject', blank=True)),
                ('text', models.TextField(null=True, verbose_name='Text', blank=True)),
                ('page', models.CharField(max_length=250, null=True, verbose_name='Page url', blank=True)),
                ('user', models.ForeignKey(verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Tickets',
                'verbose_name_plural': 'Tickets',
            },
        ),
    ]
