# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0006_auto_20150930_2329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='parent',
            field=models.ForeignKey(related_name='ticket_children', blank=True, to='crm.Ticket', null=True),
        ),
    ]
