# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from crm.models import Ticket


class TicketForm(forms.ModelForm):
    """
    Форма Отправки текета
    """

    def __init__(self, *args, **kwargs):
        super(TicketForm, self).__init__(*args, **kwargs)
        self.fields['category'].widget.attrs['class'] = 'nostyle'

    class Meta:
        model = Ticket
        fields = ('user', 'email', 'subject', 'text', 'category', 'page')


class ReTicketForm(forms.ModelForm):
    """
    Форма Отправки текета
    """
    class Meta:
        model = Ticket
        fields = ('user', 'text')