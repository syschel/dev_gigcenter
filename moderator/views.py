# -*- coding:utf-8 -*-
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.contrib import messages
from django.conf import settings
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic.edit import FormView, UpdateView
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic import DetailView, ListView, View


class HomeView(TemplateView):
    template_name = 'moderator/main.html'