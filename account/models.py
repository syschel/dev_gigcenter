#-*- coding:utf-8 -*-
from django.db import models
from django.db.models import Q
import uuid
from PIL import Image
from django.conf import settings
from django.utils import timezone
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from common.models import Country, City, Genre, FormatPerformance, TypeClub, AgentType
from common.fields import validate_nickname, ValidateImageField
from common.files import get_file_path
from gigcoin.models import TransactionCoin


class UserManager(BaseUserManager):

    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError(_('The given email must be set'))
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            last_login=now,
            date_joined=now,
            **extra_fields
        )
        user.set_password(password)
        user.base_coin = settings.REGISTRATION_BONUS
        user.save(using=self._db)
        message = _(u"GigCoins received for registration")
        TransactionCoin.objects.transaction_credit(user=user, coin=settings.REGISTRATION_BONUS,
                                                   status=TransactionCoin.STAT2, messages=message)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        user = self._create_user(email, password, True, True, **extra_fields)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_site_user(self, email, password, get_host=None, **extra_fields):
        user = self._create_user(email, password, False, False, **extra_fields)
        user.is_active = False
        user.save(using=self._db)

        user_uuids = Registration(user=user)
        user_uuids.save()
        message = u"Dear guest,\n\r" \
                  u" to complete the registration on gigcenter.com please CLICK HERE, http://%s/user_activation/?key=%s" % (get_host, user_uuids.uuid)
        user.email_user(_('Gigcenter: Validation email'), message)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    """
    Базовый профиль пользователя
    """
    email = models.EmailField(_('email address'), max_length=255, unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True, null=True, validators=[validate_nickname])
    last_name = models.CharField(_('last name'), max_length=30, blank=True, null=True, validators=[validate_nickname])

    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('active'), default=False,
        help_text=_('Designates whether this user should be treated as active. Unselect this instead of deleting accounts.'))
    is_moderator = models.BooleanField(_('moderator'), default=False, help_text=_(u'#Права доступа как модератор'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    birthday = models.DateField(_(u'Birthday'), blank=True, null=True)
    phone = models.CharField(_(u'#Телефон'), max_length=250, blank=True, null=True)
    skype = models.CharField(_(u'#skype'), max_length=50, blank=True, null=True)
    icq = models.CharField(_(u'#icq'), max_length=20, blank=True, null=True)
    country = models.ForeignKey(Country, verbose_name=_(u'country'), null=True)
    city = models.ForeignKey(City, verbose_name=_(u'city'), null=True)

    first_in = models.BooleanField(_(u'first in'), default=True, help_text=_(u"#Первое посящение - профиль не дозаполнен"))
    banned = models.BooleanField(_(u'banned'), default=False, help_text=_(u"#Заблокирован доступ к сайту"))

    base_coin = models.PositiveIntegerField(_(u"Base coin"), default=settings.BASE_GIGCOIN, blank=False, null=False)
    paid_coin = models.PositiveIntegerField(_(u"Paid coin"), default=settings.PAID_GIGCOIN, blank=False, null=False)

    #email spam
    MAILTIME0, MAILTIME1, MAILTIME2 = range(0, 3)
    MAILTIMES = (
        (MAILTIME0, _(u"I don't want to receive notifications")),
        (MAILTIME1, _(u'Once a day')),
        (MAILTIME2, _(u'Once a month')),
    )
    mail_messages = models.BooleanField(_(u'Messages'), default=True)
    mail_trade = models.BooleanField(_(u'Requests'), default=True)
    mail_dates = models.BooleanField(_(u'Dates'), default=True)
    mail_time = models.IntegerField(_(u'#График получения'), choices=MAILTIMES, default=MAILTIME1)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()
    get_full_name.short_description = _('Full name')

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    objects = UserManager()

    def is_agent(self):
        return AgentProfile.objects.filter(user=self).exists()

    def is_artist(self):
        return ArtistProfile.objects.filter(user=self).exists()

    def is_club(self):
        return ClubProfile.objects.filter(user=self).exists()

    def gigcoin_sum(self):
        return self.base_coin + self.paid_coin

    def debit_coin(self, coin, messages):
        if coin <= self.base_coin:
            self.base_coin = self.base_coin - coin
            self.save(update_fields=['base_coin'])
            TransactionCoin.objects.transaction_debit(user=self, coin=coin, messages=messages)
            return coin
        elif coin <= (self.base_coin + self.paid_coin):
            self.paid_coin -= coin - self.base_coin
            self.base_coin = 0
            self.save(update_fields=['base_coin', 'paid_coin'])
            TransactionCoin.objects.transaction_debit(user=self, coin=coin, messages=messages)
            return coin
        else:
            raise ValueError(_('The need %s more coins') % coin)

    class Meta:
        verbose_name = _(u'User')
        verbose_name_plural = _(u'1. Users')
        ordering = ['-date_joined']


def get_uuid_str():
    return uuid.uuid4().__str__()


class Registration(models.Model):
    uuid = models.CharField(max_length=36, default=get_uuid_str)
    user = models.OneToOneField(User, related_name='registration')
    create = models.DateTimeField(_(u"#Создано"), auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.uuid

    class Meta:
        verbose_name = _(u'Регистрации')
        verbose_name_plural = _(u'Подтверждение регистрации')


class AgentProfile(models.Model):
    """
    Под профиль агента
    """
    create          = models.DateTimeField(_(u"#Создано"), auto_now_add=True, blank=True, null=True)
    update          = models.DateTimeField(_(u"#Обновлено"), auto_now=True, blank=True, null=True)

    TYPE0, TYPE1 = range(0, 2)
    TYPES = (
        (TYPE0, _(u'Private agent')),
        (TYPE1, _(u'Agency')),
    )
    uuid_id         = models.UUIDField(default=uuid.uuid4, editable=False)

    user            = models.ForeignKey(User, verbose_name=_(u'User'), blank=True, related_name=r"user_agent")
    nickname        = models.CharField(_(u'Псевдоним'), max_length=250, null=True, validators=[validate_nickname])
    type_agent      = models.ManyToManyField(AgentType, verbose_name=_(u"type agent"), blank=False)
    jobs            = models.TextField(_(u'#текущее место работы'), null=True)
    avatar          = ValidateImageField(_(u'#Аватар'), upload_to=get_file_path, blank=True, null=True)
    country         = models.ForeignKey(Country, verbose_name=_(u'Country'), null=True)
    city            = models.ForeignKey(City, verbose_name=_(u'City'), null=True)
    address         = models.CharField(_(u'#Адрес'), max_length=250, null=True)
    phone           = models.CharField(_(u'#Телефон'), max_length=250, null=True, blank=True)
    email           = models.CharField(_(u'#email'), max_length=250, null=True, blank=True)
    site            = models.ManyToManyField('AgentSite', verbose_name=_(u'#Сайты'), blank=True)
    info            = models.TextField(_(u'Информация'), null=True, blank=True)

    view            = models.IntegerField(_(u"#Просмотры"), default=0, blank=True)
    banned          = models.BooleanField(_(u'#Забанен'), default=False, help_text=_(u"#Заблокирован"))
    visibility      = models.BooleanField(_(u'Visibility'), default=True, help_text=_(u"Public or Private"), choices=((True, 'Public',), (False, 'Private',)))

    # def get_absolute_url(self):
    #     return reverse('account_agent', args=[self.id])

    def __unicode__(self):
        return u"%s" % self.nickname

    def save(self, size=(120, 120)):
        super(AgentProfile, self).save()
        # Ресайзим картинку(каждый раз при новом сохранении)
        if self.avatar:
            filename = self.avatar.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    def gigdetes_count(self):
        return self.user.user_dateall.count()

    def trade_count(self):
        return 0

    def get_absolute_url(self):
        return reverse('users:agent', args=[self.uuid_id])

    class Meta:
        verbose_name = _(u'Agent')
        verbose_name_plural = _(u'2. Agents')
        ordering = ['nickname']


class AgentSite(models.Model):
    """ Сайты агента """
    site            = models.CharField(_(u'#Сайт'), max_length=250,)
    agent           = models.ForeignKey(AgentProfile, verbose_name=_(u'#Агент'))

    def __unicode__(self):
        return u"%s" % self.site

    class Meta:
        verbose_name = _(u'#Сайт агента')
        verbose_name_plural = _(u'#Сайт агента')
        unique_together = ('site', 'agent',)


class ArtistProfile(models.Model):
    """
    Под профиль артиста
    """
    create          = models.DateTimeField(_(u"#Создано"), auto_now_add=True, blank=True, null=True)
    update          = models.DateTimeField(_(u"#Обновлено"), auto_now=True, blank=True, null=True)

    TYPE0, TYPE1 = range(0, 2)
    TYPES = (
        (TYPE0, _(u'DJ set')),
        (TYPE1, _(u'LIVE PA')),
    )
    uuid_id         = models.UUIDField(default=uuid.uuid4, editable=False)

    user            = models.ForeignKey(User, verbose_name=_(u'User'), blank=True, related_name="user_artist")
    agent           = models.ForeignKey(AgentProfile, verbose_name=_(u'Agent'), null=True, blank=True, related_name="agent_artist")
    nickname        = models.CharField(_(u'Псевдоним'), max_length=250, null=True, validators=[validate_nickname])
    country         = models.ForeignKey(Country, verbose_name=_(u'Country'), null=True)
    city            = models.ForeignKey(City, verbose_name=_(u'City'), null=True)
    avatar          = ValidateImageField(_(u'#Аватар'), upload_to=get_file_path, blank=True, null=True)
    type_format     = models.ManyToManyField(FormatPerformance, _(u'type formats+'), blank=False)
    genre           = models.ManyToManyField(Genre, verbose_name=_(u'Жанры'), blank=False)
    label           = models.TextField(_(u'Лейбл'), null=True, blank=True)

    phone           = models.CharField(_(u'#Телефон'), max_length=250, null=True, blank=True)
    email           = models.CharField(_(u'#email'), max_length=250, null=True, blank=True)
    site            = models.ManyToManyField('ArtistSite', verbose_name=_(u'#Сайты'), blank=True)

    biography       = models.TextField(_(u'#Биография'), null=True, blank=True)
    rider           = models.ManyToManyField('RiderAll', verbose_name=_(u'#Райдеры'), blank=True,)
    dogovor         = models.FileField(_(u'#Файл Договора'), upload_to=get_file_path, blank=True, null=True)

    video           = models.CharField(_(u'#Видео'), max_length=255, null=True, blank=True)
    mix             = models.CharField(_(u'#Микс'), max_length=255, null=True, blank=True)
    track           = models.CharField(_(u'#Трек'), max_length=255, null=True, blank=True)
    reliz           = models.CharField(_(u'#Релиз'), max_length=255, null=True, blank=True)

    images          = models.ManyToManyField('ArtistImages', verbose_name=_(u'Images'), blank=True,)

    view            = models.IntegerField(_(u"#Просмотры"), default=0, blank=True)
    banned          = models.BooleanField(_(u'#Забанен'), default=False, help_text=_(u"#Заблокирован"))
    visibility      = models.BooleanField(_(u'Visibility'), default=True, help_text=_(u"Public or Private"), choices=((True, 'Public',), (False, 'Private',)))

    # def get_absolute_url(self):
    #     return reverse('account_agent', args=[self.id])

    def __unicode__(self):
        return u"%s" % self.nickname

    def save(self, size=(120, 120)):
        super(ArtistProfile, self).save()
        # Ресайзим картинку(каждый раз при новом сохранении)
        if self.avatar:
            filename = self.avatar.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    def get_absolute_url(self):
        return reverse('users:artist', args=[self.uuid_id])

    def gigdetes_count(self):
        return self.user.user_dateall.count()

    def dates_count(self):
        return self.gigdate_artist.count() + self.barter_artist.count()

    def trade_count(self):
        return 0

    class Meta:
        verbose_name = _(u'Artist')
        verbose_name_plural = _(u'3. Artist')
        ordering = ['nickname']


class ArtistSite(models.Model):
    """ Сайты Артиса """
    artist          = models.ForeignKey(ArtistProfile, verbose_name=_(u'artist'))
    site            = models.CharField(_(u'#Сайт'), max_length=250,)

    def __unicode__(self):
        return u"%s" % self.site

    class Meta:
        verbose_name = _(u'Artist site')
        verbose_name_plural = _(u'Artist site')
        unique_together = ('site', 'artist',)


class ArtistImages(models.Model):

    artist = models.ForeignKey(ArtistProfile, verbose_name=_(u'artist'), blank=True)
    img = ValidateImageField(_(u'Изображение'), upload_to=get_file_path, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.img

    class Meta:
        verbose_name = _(u'Artist photo')
        verbose_name_plural = _(u'Artist photo')


class RiderAll(models.Model):
    """ Технические райдер артиста """

    artist          = models.ForeignKey(ArtistProfile, verbose_name=_(u'artist'), blank=True)
    file            = models.FileField(_(u'#Файл райдера'), upload_to=get_file_path, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.file

    class Meta:
        verbose_name = _(u'Artist riders')
        verbose_name_plural = _(u'Artist riders')


class ClubProfile(models.Model):
    """
    Под профиль клуба
    """
    create          = models.DateTimeField(_(u"#Создано"), auto_now_add=True, blank=True, null=True)
    update          = models.DateTimeField(_(u"#Обновлено"), auto_now=True, blank=True, null=True)
    uuid_id         = models.UUIDField(default=uuid.uuid4, editable=False)

    user            = models.ForeignKey(User, verbose_name=_(u'User'), blank=True, related_name="user_club")
    agent           = models.ForeignKey(AgentProfile, verbose_name=_(u'Agent'), null=True, blank=True, related_name="agent_club")
    nickname        = models.CharField(_(u'Псевдоним'), max_length=250, null=True, validators=[validate_nickname])
    avatar          = ValidateImageField(_(u'#Аватар'), upload_to=get_file_path, blank=True, null=True)
    country         = models.ForeignKey(Country, verbose_name=_(u'Country'), null=True)
    city            = models.ForeignKey(City, verbose_name=_(u'City'), null=True)

    address         = models.CharField(_(u'#Адрес'), max_length=250, null=True)
    phone           = models.CharField(_(u'#Телефон'), max_length=250, null=True, blank=True)
    email           = models.CharField(_(u'#email'), max_length=250, null=True, blank=True)
    site            = models.ManyToManyField('ClubSite', verbose_name=_(u'#Сайты'), blank=True)

    type_club       = models.ManyToManyField(TypeClub, verbose_name=_(u'Тип площадки'), blank=False)
    place_street    = models.BooleanField(_(u'#Место выступления на улице'), default=True)
    place_indoors   = models.BooleanField(_(u'#Место выступления в помешении'), default=True)
    music_zone      = models.PositiveIntegerField(_(u"#Музыкальных зон"), default=1, blank=True)
    capacity_total  = models.PositiveIntegerField(_(u"#Общая вместимость"), default=1, blank=True)
    capacity_seat   = models.PositiveIntegerField(_(u"#Посадочная вместимость"), default=1, blank=True)
    genre           = models.ManyToManyField(Genre, verbose_name=_(u'Жанры'), blank=False)
    nearest_airport = models.CharField(_(u'#Ближайший аэропорт '), max_length=250, null=True, blank=True)
    nearest_station = models.CharField(_(u'#Ближайший жд вокзал'), max_length=250, null=True, blank=True)
    artists_club    = models.CharField(_(u'#Артисты, которые уже выступали на площадке'), max_length=250, null=True, blank=True)
    info            = models.TextField(_(u'Информация'), null=True, blank=True)
    music             = models.CharField(_(u'#Музыка'), max_length=255, null=True, blank=True)
    video           = models.CharField(_(u'#Видео'), max_length=255, null=True, blank=True)

    images          = models.ManyToManyField('ClubImages', verbose_name=_(u'Images'), blank=True,)

    view            = models.IntegerField(_(u"#Просмотры"), default=0, blank=True)
    banned          = models.BooleanField(_(u'#Забанен'), default=False, help_text=_(u"#Заблокирован"))
    visibility      = models.BooleanField(_(u'Visibility'), default=True, help_text=_(u"Public or Private"), choices=((True, 'Public',), (False, 'Private',)))

    def __unicode__(self):
        return u"%s" % self.nickname

    def save(self, size=(120, 120)):
        super(ClubProfile, self).save()
        # Ресайзим картинку(каждый раз при новом сохранении)
        if self.avatar:
            filename = self.avatar.path
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename, image.format)

    def get_absolute_url(self):
        return reverse('users:club', args=[self.uuid_id])

    def gigdetes_count(self):
        return self.user.user_dateall.count()

    def dates_count(self):
        return self.tender_club.count() + self.barter_club.count()

    def trade_count(self):
        from trade.models import Trades
        trades_count = Trades.objects.filter(Q(club_date=self.pk) | Q(club_trade=self.pk))
        return trades_count.count()

    def deals_count(self):
        from trade.models import Trades
        deals_count = Trades.objects.filter(Q(club_date=self.pk) | Q(club_trade=self.pk), Q(status_trade=Trades.STRADE2))
        return deals_count.count()

    class Meta:
        verbose_name = _(u'Venue')
        verbose_name_plural = _(u'4. Venues')
        ordering = ['nickname']


class ClubSite(models.Model):
    """ Сайты Артиса """
    site            = models.CharField(_(u'#Сайт'), max_length=250,)
    club            = models.ForeignKey(ClubProfile, verbose_name=_(u'Venue'))

    def __unicode__(self):
        return u"%s" % self.site

    class Meta:
        verbose_name = _(u'#Сайт клуба')
        verbose_name_plural = _(u'#Сайты кдуба')
        unique_together = ('site', 'club',)


class ClubImages(models.Model):

    club            = models.ForeignKey(ClubProfile, verbose_name=_(u'Venue'))
    img = ValidateImageField(_(u'images'), upload_to=get_file_path, blank=True, null=True)

    def __unicode__(self):
        return u"%s" % self.img

    class Meta:
        verbose_name = _(u'Venue photo')
        verbose_name_plural = _(u'Venue photo')


class Mailbox(models.Model):
    """
        Сообщения
    """

    parent          = models.ForeignKey('self', verbose_name=_(u'parent'), blank=True, null=True, related_name='sub_mailbox')
    sender          = models.ForeignKey(User, verbose_name=_(u'sender(from)'), related_name='from_sender',
                                        null=True, blank=True)
    to              = models.ForeignKey(User, verbose_name=_(u'recipient(to)'), related_name='to_sender',
                                        null=True, blank=True)
    title           = models.CharField(max_length=128, verbose_name=_(u'subject'))
    text            = models.TextField(verbose_name=_(u'Message'))
    date            = models.DateTimeField(verbose_name=_(u'Created'), auto_now_add=True)
    file_f          = models.FileField(verbose_name=_(u'File'), blank=True, upload_to=get_file_path)
    read_to         = models.BooleanField(_(u'#Просмотрено получателем'), default=False)

    def __unicode__(self):
        return u"from: %s, to: %s" % (self.sender, self.to)

    def get_absolute_url(self):
        return reverse('account:mailbox_view', args=[self.pk])

    class Meta:
        verbose_name = _(u'Сообщения')
        verbose_name_plural = _(u'Сообщения')
        ordering = ('-date',)


class SocialAuth(models.Model):
    """
        Social auth
    """
    create = models.DateTimeField(verbose_name=_(u'Created'), auto_now_add=True)
    user = models.ForeignKey(User, verbose_name=_(u'User'), blank=True, null=True, related_name='social_user')
    provider = models.CharField(max_length=32, verbose_name=_(u'Provider'), null=True)
    uid = models.CharField(max_length=255, verbose_name=_(u'Uid'), unique=True)

    def __unicode__(self):
        return self.uid

    class Meta:
        verbose_name = _(u'Social auth')
        verbose_name_plural = _(u'Social auth')
        ordering = ('-create',)
        unique_together = ('user', 'uid')