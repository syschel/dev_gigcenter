# -*- coding:utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import login_required, user_passes_test
from account.views import MainView, AjaxCity, AgentFormView, ArtistFormView, ClubFormView, AgentArtistListView,\
    AgentClubListView, AgentArtistCreateView, AgentClubCreateView, AgentClubUpdateView, AgentArtistUpdateView,\
    AjaxArtist, AjaxClub, MailboxAjax, MailBoxListView, MailBoxOutListView, MailboxDetailView, SendMail
from gigdate.views import GigdateallListView, GigdateallFilterListView, GigdateUpdateView, GigBarterUpdateView,\
    GigTenderUpdateView
from trade.views import TradeListView, tradedetai, DealsDetailView, DealsDetailViewPrint, DealsDetailViewPDF
from gigcoin.views import TransactionCoinList, PromoCodeFormView, PaidView


urlpatterns = [
    url(r'^agent/$', login_required(AgentFormView.as_view()), name='agent'),
    url(r'^artist/$', login_required(ArtistFormView.as_view()), name='artist'),
    url(r'^venue/$', login_required(ClubFormView.as_view()), name='club'),
    url(r'^agent/artist/$', login_required(AgentArtistListView.as_view()), name='agent_artist'),
    url(r'^agent/venue/$', login_required(AgentClubListView.as_view()), name='agent_club'),
    url(r'^agent/artist/add/$', login_required(AgentArtistCreateView.as_view()), name='agent_artist_add'),
    url(r'^agent/venue/add/$', login_required(AgentClubCreateView.as_view()), name='agent_club_add'),
    url(r'^agent/artist/edit/(?P<slug>[^/]+)/$', login_required(AgentArtistUpdateView.as_view()), name='agent_artist_edit'),
    url(r'^agent/venue/edit/(?P<slug>[^/]+)/$', login_required(AgentClubUpdateView.as_view()), name='agent_club_edit'),

    url(r'^dates/$', login_required(GigdateallListView.as_view()), name='dates'),
    url(r'^dates/(?P<pk>\d+)/(?P<slug>\w+)/$', login_required(GigdateallFilterListView.as_view()), name='dates_filter'),
    url(r'^dates/edit_(?P<slug>[^/]+)/date/$', login_required(GigdateUpdateView.as_view()), name='edit_gigdate'),
    url(r'^dates/edit_(?P<slug>[^/]+)/barter/$', login_required(GigBarterUpdateView.as_view()), name='edit_barter'),
    url(r'^dates/edit_(?P<slug>[^/]+)/tender/$', login_required(GigTenderUpdateView.as_view()), name='edit_tender'),

    url(r'^ajax_city/(?P<id>[0-9]+)/$', AjaxCity.as_view(), name='ajax_city'),
    url(r'^get_artist/(?P<id>[0-9]+)/$', AjaxArtist.as_view(), name='ajax_artist'),
    url(r'^get_club/(?P<id>[0-9]+)/$', AjaxClub.as_view(), name='ajax_club'),

    url(r'^mailbox_ajax/$', MailboxAjax.as_view(), name='mailbox_ajax'),
    url(r'^message/$', MailBoxListView.as_view(), name='mailbox'),
    url(r'^message/out/$', MailBoxOutListView.as_view(), name='mailbox_out'),
    url(r'^message/(?P<pk>\d+)/$', MailboxDetailView.as_view(), name='mailbox_view'),

    url(r'^trade/$', TradeListView.as_view(), name='trade_list'),
    url(r'^trade/(?P<pk>\d+)/$', tradedetai, name='trade_view'),
    url(r'^trade/(?P<pk>\d+)/deals/$', DealsDetailView.as_view(), name='deals_view'),
    url(r'^trade/(?P<pk>\d+)/deals/print/$', DealsDetailViewPrint.as_view(), name='deals_print'),
    url(r'^trade/(?P<pk>\d+)/deals/pdf/$', DealsDetailViewPDF.as_view(), name='deals_pdf'),

    url(r'^send_mail/$', user_passes_test(lambda u: u.is_superuser or u.is_moderator)(SendMail.as_view()), name='send_mail'),

    url(r'^billing/$', TransactionCoinList.as_view(), name='billing'),
    url(r'^promo_code/$', PromoCodeFormView.as_view(), name='promo_code'),
    url(r'^paid_coin/$', PaidView.as_view(), name='paid_coin'),

    url(r'^$', login_required(MainView.as_view()), name='main'),
]