# -*- coding: utf-8 -*-
from django import template
register = template.Library()
from account.forms import MailboxAjaxForm


@register.inclusion_tag('account/pop_up.html', takes_context=True)
def add_mailbox(context, to_id):
    """
        Форма обратной связи
    """
    form = MailboxAjaxForm(initial={'to': to_id.pk})
    return {'form': form}