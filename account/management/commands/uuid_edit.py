# -*- coding: utf-8 -*-
import uuid
from django.core.management.base import BaseCommand

from gigdate.models import DateAll
from account.models import ArtistProfile, AgentProfile, ClubProfile


class Command(BaseCommand):
    help = 'UUidFiels update default values in db'

    def handle(self, *args, **options):
        print u"-= start =-"
        print u"_gigdetes: ", self._gigdetes()
        print u"_gartist: ", self._gartist()
        print u"_gagent: ", self._gagent()
        print u"_gclub: ", self._gclub()
        print u"-= stop =-"

    def _gigdetes(self):
        dates = DateAll.objects.all()
        for dat in dates:
            dat.uuid_id = uuid.uuid4().hex
            dat.save()
        return dates.count()

    def _gartist(self):
        artists = ArtistProfile.objects.all()
        for art in artists:
            art.uuid_id = uuid.uuid4().hex
            art.save()
        return artists.count()

    def _gagent(self):
        agents = AgentProfile.objects.all()
        for agt in agents:
            agt.uuid_id = uuid.uuid4().hex
            agt.save()
        return agents.count()

    def _gclub(self):
        clubs = ClubProfile.objects.all()
        for clb in clubs:
            clb.uuid_id = uuid.uuid4().hex
            clb.save()
        return clubs.count()