# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.template.loader import render_to_string
from datetime import timedelta, datetime
from django.conf import settings
from django.db.models import Q

from account.models import User, Mailbox
from gigdate.models import DateAll
from trade.models import Trades


class Command(BaseCommand):
    help = 'Sending mailings with dates, messages, requests'

    def handle(self, *args, **options):
        print _(u"Subscriptions:"), len(self.get_users())

    def get_period(self):
        return {
            1: datetime.now() - timedelta(days=1),
            2: datetime.now() - timedelta(days=30)
        }

    def last_dates(self, period):
        return DateAll.objects.filter(create__gte=period)[:10]

    def last_trades(self, user, period):
        return Trades.objects.filter(Q(update__gte=period), Q(user_date=user) | Q(user_trade=user))[:10]

    def last_messges(self, user, period):
        return Mailbox.objects.filter(date__gte=period, to=user)[:10]

    def start_mail(self, from_email, to_email, content):
        msg_plain = render_to_string('account/send_mails.txt', {'user': content, 'SITE_URL': settings.SITE_URL})
        msg_html = render_to_string('account/send_mails.html', {'user': content, 'SITE_URL': settings.SITE_URL})
        return send_mail(
            _(u'[GigCenter.com] Recent activity in Your account'),
            msg_plain,
            from_email,
            [to_email],
            html_message=msg_html
        )

    def get_users(self):
        users = User.objects.filter(~Q(mail_time=User.MAILTIME0))
        period = self.get_period()
        for user in users:
            send = 0
            if user.mail_dates:
                user.dates = self.last_dates(period.get(user.mail_time))
                send += 1
            if user.mail_trade:
                user.trades = self.last_trades(user, period.get(user.mail_time))
                send += 1
            if user.mail_messages:
                user.messages = self.last_messges(user, period.get(user.mail_time))
                send += 1
            if send:
                self.start_mail(settings.DEFAULT_FROM_EMAIL, user.email, user)
        return users