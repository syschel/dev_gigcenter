# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.management.base import BaseCommand
from django.db import connection
cursor = connection.cursor()
from account.models import SocialAuth


class Command(BaseCommand):
    help = 'Clean Python Social Auth and migration old user'

    def handle(self, *args, **options):
        self.migrate_user()
        self.truncate()
        # self.drop()

    def migrate_user(self):
        sql = u"SELECT `provider`, `uid`, `user_id` FROM `social_auth_usersocialauth`;"
        cursor.execute(sql)
        sql_query = self.dictfetchall(cursor)
        for psa in sql_query:
            print psa
            SocialAuth.objects.create(
                provider=psa.get('provider'),
                uid=psa.get('uid'),
                user_id=psa.get('user_id')
            )
        return sql_query

    def dictfetchall(self, c):
        "Returns all rows from a cursor as a dict"
        desc = c.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in c.fetchall()
        ]

    def truncate(self):
        sql = u"TRUNCATE `social_auth_association`;" \
              u"TRUNCATE `social_auth_code`;" \
              u"TRUNCATE `social_auth_nonce`;" \
              u"TRUNCATE `social_auth_usersocialauth`;"
        return cursor.execute(sql)

    def drop(self):
        sql = u"DROP TABLE `social_auth_association`, `social_auth_code`, `social_auth_nonce`, `social_auth_usersocialauth`;"
        return cursor.execute(sql)