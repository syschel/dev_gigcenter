# -*- coding:utf-8 -*-
import re
from django.contrib.auth.forms import UserCreationForm as AuthUserCreationForm,\
    UserChangeForm as AuthUserChangeForm, AuthenticationForm
from django import forms
from django.utils.translation import ugettext_lazy as _
from account.models import User, AgentProfile, ArtistProfile, ClubProfile, Mailbox
from common.models import City


class UserCreationForm(AuthUserCreationForm):

    class Meta:
        model = User
        fields = '__all__'

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
            )


class UserChangeForm(AuthUserChangeForm):
    """
    форма редактирование пользователя в админке
    """
    class Meta:
        model = User
        fields = '__all__'


class LoginFormMixin(object):

    # This is where our logic will take place

    username = forms.CharField(label='E-mail', required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=True)

    def clean(self):
        email = self.cleaned_data.get('username', '')
        password = self.cleaned_data.get('password', '')
        self.user = None
        users = User.objects.filter(email=email)
        for user in users:
            if user.is_active and user.check_password(password):
                self.user = user
        if self.user is None:
            raise forms.ValidationError('Invalid email or password')
        # We are setting the backend here so you may (and should) remove the line setting it in myauth.views
        self.user.backend = 'django.contrib.auth.backends.ModelBackend'
        return self.cleaned_data


class LoginForm(LoginFormMixin, forms.Form):

    login = forms.CharField(label='E-mail', required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=True)

    def clean(self):
        login = self.cleaned_data.get('login', '')
        password = self.cleaned_data.get('password', '')
        self.user = None
        users = User.objects.filter(email=login)
        for user in users:
            if user.is_active and user.check_password(password):
                self.user = user
        if self.user is None:
            raise forms.ValidationError('Invalid email or password')
        return self.cleaned_data


class AuthLoginForm(LoginFormMixin, AuthenticationForm):

    # Because of the way Python's MRO works we have to redefine email which has been overridden by AuthenticationForm
    username = forms.CharField(label=_('E-mail'), required=True)
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput, required=True)

    def clean(self):
        cleaned_data = super(AuthLoginForm, self).clean()
        if self.user is not None:
            self.user_cache = self.user
        return cleaned_data


class SignInForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=True)

    def __init__(self, *args, **kwargs):
        super(SignInForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = 'your_email@me.com'
        self.fields['password'].widget.attrs['placeholder'] = 'password'

    def clean(self):
        email = self.cleaned_data.get('email', '')
        password = self.cleaned_data.get('password', '')
        users = User.objects.filter(email=email)

        if 'login' in self.data:
            self.user = None
            for user in users:
                self.user = user
                # elif not user.is_active:
                #     raise forms.ValidationError('Email not confirmed', code='is_active')
            if self.user is None:
                raise forms.ValidationError('User with this email is not found. Click the registration button.')
            if not self.user.check_password(password):
                raise forms.ValidationError('Wrong password.')
        elif 'registration' in self.data:
            if users:
                raise forms.ValidationError('This email is already used.', code='duplicate_email')
        else:
            raise forms.ValidationError('Invalid form Sign In')
        return self.cleaned_data


class RePasswordForm(forms.Form):
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super(RePasswordForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = 'youremail@me.com'

    def clean(self):
        email = self.cleaned_data.get('email', '')
        self.user = None
        users = User.objects.filter(email=email)
        for user in users:
            self.user = user
        if self.user is None:
            raise forms.ValidationError('Email not found')
        return self.cleaned_data


class AccountForm(forms.ModelForm):
    """
    Форма редактирования профиля
    """
    birthday = forms.DateField(required=True)
    first_name = forms.CharField(max_length=30, required=True)
    last_name = forms.CharField(max_length=30, required=True)

    old_pass = forms.CharField(label=_('Old Password'), max_length=128, required=False,
                               widget=forms.PasswordInput(render_value=True))
    new_pass = forms.CharField(label=_('New Password'), max_length=128, required=False,
                               widget=forms.PasswordInput(render_value=True))
    new_pass2 = forms.CharField(label=_('Retype New Password'), max_length=128, required=False,
                                widget=forms.PasswordInput(render_value=True))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.get('instance')
        super(AccountForm, self).__init__(*args, **kwargs)
        self.fields['mail_messages'].widget.attrs['class'] = 'checkbox'
        self.fields['mail_trade'].widget.attrs['class'] = 'checkbox'
        self.fields['mail_dates'].widget.attrs['class'] = 'checkbox'
        self.fields['mail_time'].widget.attrs['class'] = 'radio'
        self.fields['phone'].widget.attrs['autocomplete'] = 'off'
        self.fields['skype'].widget.attrs['autocomplete'] = 'off'
        # self.fields['icq'].widget.attrs['autocomplete'] = 'off'
        self.fields['old_pass'].widget.attrs['autocomplete'] = 'off'
        self.fields['new_pass'].widget.attrs['autocomplete'] = 'off'
        self.fields['new_pass2'].widget.attrs['autocomplete'] = 'off'

        # Города автозаполнения, что бы не выводить портянку
        if 0 == len(self.data) and not self.instance.country:
            self.fields['city'].queryset = City.objects.none()
        elif len(self.data) > 0 and self.data.get('country'):
            self.fields['city'].queryset = City.objects.filter(country=self.data.get('country'))
        else:
            self.fields['city'].queryset = City.objects.filter(country=self.instance.country)

    def clean(self):
        cleaned_data = self.cleaned_data
        #Изменение пароля
        if cleaned_data['old_pass']:
            if not self.user.check_password(cleaned_data['old_pass']):
                raise forms.ValidationError({'old_pass': _('Password incorrect')}, code='invalid')
            else:
                if not cleaned_data['new_pass'] and not cleaned_data['new_pass2']:
                    raise forms.ValidationError({'new_pass': _('This field is required.'),
                                                 'new_pass2': _('This field is required.')}, code='required')
                elif not cleaned_data['new_pass']:
                    raise forms.ValidationError({'new_pass': _('This field is required.')}, code='required')
                elif not cleaned_data['new_pass2']:
                    raise forms.ValidationError({'new_pass2': _('This field is required.')}, code='required')
                elif cleaned_data['new_pass'] != cleaned_data['new_pass2']:
                    raise forms.ValidationError({'new_pass2': _('Passwords do not match')}, code='invalid')
        return cleaned_data

    class Meta:
        model = User
        widgets = {'mail_time': forms.RadioSelect()}
        exclude = ('is_staff', 'is_active', 'is_moderator', 'is_superuser', 'groups', 'user_permissions', 'banned',
                   'password', 'last_login', 'date_joined', 'first_in', 'email', 'icq', 'base_coin', 'paid_coin')


class AgentForm(forms.ModelForm):
    """
    Форма редактирования агента
    """
    jobs = forms.CharField(max_length=255)
    avatar = forms.ImageField(widget=forms.FileInput, required=False)

    def __init__(self, *args, **kwargs):
        # self.user = kwargs.get('instance')
        super(AgentForm, self).__init__(*args, **kwargs)

        # Города автозаполнения, что бы не выводить портянку
        if 0 == len(self.data) and not self.instance.country:
            self.fields['city'].queryset = City.objects.none()
        elif len(self.data) > 0 and self.data.get('country'):
            self.fields['city'].queryset = City.objects.filter(country=self.data.get('country'))
        else:
            self.fields['city'].queryset = City.objects.filter(country=self.instance.country)

    class Meta:
        model = AgentProfile
        exclude = ['user', 'view', 'banned', 'site', 'uuid_id']


class ArtistForm(forms.ModelForm):
    """
    Форма редактирования артиста
    """
    avatar = forms.ImageField(widget=forms.FileInput, required=False)

    def __init__(self, *args, **kwargs):
        # self.user = kwargs.get('instance')
        super(ArtistForm, self).__init__(*args, **kwargs)

        # Города автозаполнения, что бы не выводить портянку
        if 0 == len(self.data) and not self.instance.country:
            self.fields['city'].queryset = City.objects.none()
        elif len(self.data) > 0 and self.data.get('country'):
            self.fields['city'].queryset = City.objects.filter(country=self.data.get('country'))
        else:
            self.fields['city'].queryset = City.objects.filter(country=self.instance.country)

    def clean_video(self):
        video = self.cleaned_data.get("video")
        return re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)", "", video)

    def clean_mix(self):
        mix = self.cleaned_data.get("mix")
        return re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)", "", mix)

    def clean_track(self):
        track = self.cleaned_data.get("track")
        return re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)", "", track)

    def clean_reliz(self):
        reliz = self.cleaned_data.get("reliz")
        return re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)", "", reliz)

    class Meta:
        model = ArtistProfile
        exclude = ['user', 'agent', 'view', 'banned', 'site', 'rider', 'images', 'uuid_id']


class ClubForm(forms.ModelForm):
    """
    Форма редактирования клуба
    """
    avatar = forms.ImageField(widget=forms.FileInput, required=False)

    def __init__(self, *args, **kwargs):
        # self.user = kwargs.get('instance')
        super(ClubForm, self).__init__(*args, **kwargs)
        self.fields['place_street'].widget.attrs['class'] = 'checkbox'
        self.fields['place_indoors'].widget.attrs['class'] = 'checkbox'

        # Города автозаполнения, что бы не выводить портянку
        if 0 == len(self.data) and not self.instance.country:
            self.fields['city'].queryset = City.objects.none()
        elif len(self.data) > 0 and self.data.get('country'):
            self.fields['city'].queryset = City.objects.filter(country=self.data.get('country'))
        else:
            self.fields['city'].queryset = City.objects.filter(country=self.instance.country)

    def clean_music(self):
        music = self.cleaned_data.get("music")
        return re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)", "", music)

    def clean_video(self):
        video = self.cleaned_data.get("video")
        return re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)", "", video)

    class Meta:
        model = ClubProfile
        exclude = ['user', 'agent', 'view', 'banned', 'site', 'images', 'uuid_id']


class MailboxAjaxForm(forms.ModelForm):
    """
    Форма ответа на письмо
    """

    class Meta:
        model = Mailbox
        fields = ('title', 'text', 'to')
        widgets = {'to': forms.HiddenInput()}


class MailboxFromForm(forms.ModelForm):
    """
    Форма ответа на письмо
    """

    class Meta:
        model = Mailbox
        fields = ('text', 'to')
        widgets = {'to': forms.HiddenInput()}