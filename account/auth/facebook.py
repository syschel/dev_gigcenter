# -*- coding: utf-8 -*-
import urllib
import simplejson
import urlparse
from django.conf import settings
from django.contrib.auth import get_user_model
from account.models import SocialAuth, User


class FacebookBackend(object):
    """
    Authenticates against settings.AUTH_USER_MODEL and facebook oAuth.
    """

    def authenticate(self, fb_id=None):
        try:
            user = SocialAuth.objects.get(uid=fb_id)
            return user.user
        except SocialAuth.DoesNotExist:
            return None

    def get_user(self, user_id):
        UserModel = get_user_model()
        try:
            return UserModel._default_manager.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None


class FacebookAuth(object):
    """
    login and registration with facebook API
    https://developers.facebook.com/
    """
    valid_url = '/'
    invalid_url = '/'

    def login_fb(self, request):
        if request.GET:
            if 'code' in request.GET:
                token = self._get_token(request.GET['code'])
                get_user_info = self._get_user_info(token)
                user = self._get_user(get_user_info)
                return user
        raise

    @staticmethod
    def _get_token(code):
        args = {
            'client_id': settings.AUTH_FACEBOOK_KEY,
            'redirect_uri': r'http://%s%s' % (settings.SITE_URL, settings.AUTH_FACEBOOK_REDIRECT_URI),
            'client_secret': settings.AUTH_FACEBOOK_SECRET,
            'code': code,
        }
        url = 'https://graph.facebook.com/oauth/access_token?' + urllib.urlencode(args)
        response = urlparse.parse_qs(urllib.urlopen(url).read())
        return response.get('access_token')[0]

    @staticmethod
    def _get_user_info(token):
        url = 'https://graph.facebook.com/me'
        params = {'access_token': token}
        url += '?' + urllib.urlencode(params)
        response = simplejson.load(urllib.urlopen(url))

        return {
            'first_name': response.get('first_name'),
            'last_name': response.get('last_name'),
            'fb_user_id': response.get('id'),
            'email': response.get('email') if 'email' in response else 'fblogin_%s@gigcenter.com' % response.get('id'),
            'link': response.get('link'),
            'timezone': response.get('timezone'),
        }

    @staticmethod
    def _get_user(user_info):
        social, created = SocialAuth.objects.get_or_create(uid=user_info.get('fb_user_id'))
        if created:
            user = User.objects.filter(email=user_info.get('email'))
            if user:
                user = user[0]
            else:
                user = User.objects.create_user(email=user_info.get('email'))
                user.set_unusable_password()
                user.first_name = user_info.get('first_name')
                user.last_name = user_info.get('last_name')
                user.is_active = True
                user.save()
            social.provider = u'facebook'
            social.user = user
            social.save()
        else:
            user = social.user
        return user