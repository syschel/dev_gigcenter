# -*- coding:utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.decorators import login_required, user_passes_test
from account.views import ArtistListView, ClubsListView, AgentsListView, ArtistDetailView, ClubDetailView,\
    AgentDetailView, ArtistRedirectView, VenueRedirectView, AgentRedirectView


urlpatterns = [
    url(r'^venues/$', login_required(ClubsListView.as_view()), name='club_list'),
    url(r'^agents/$', login_required(AgentsListView.as_view()), name='agent_list'),
    url(r'^artist/(?P<pk>\d+)/$', ArtistRedirectView.as_view(), name='artist_302'),
    url(r'^venue/(?P<pk>\d+)/$', VenueRedirectView.as_view(), name='venue_302'),
    url(r'^agent/(?P<pk>\d+)/$', AgentRedirectView.as_view(), name='agent_302'),

    url(r'^artist/(?P<slug>[^/]+)/$', ArtistDetailView.as_view(), name='artist'),
    url(r'^venue/(?P<slug>[^/]+)/$', ClubDetailView.as_view(), name='club'),
    url(r'^agent/(?P<slug>[^/]+)/$', AgentDetailView.as_view(), name='agent'),

    url(r'^$', ArtistListView.as_view(), name='main'),
]