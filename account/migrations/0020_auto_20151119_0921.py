# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0019_auto_20151017_1915'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mailbox',
            options={'ordering': ('date',), 'verbose_name': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', 'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f'},
        ),
        migrations.AlterField(
            model_name='agentprofile',
            name='visibility',
            field=models.BooleanField(default=True, help_text='Public or Private', verbose_name='Visibility', choices=[(True, b'Public'), (False, b'Private')]),
        ),
        migrations.AlterField(
            model_name='artistprofile',
            name='visibility',
            field=models.BooleanField(default=True, help_text='Public or Private', verbose_name='Visibility', choices=[(True, b'Public'), (False, b'Private')]),
        ),
        migrations.AlterField(
            model_name='clubprofile',
            name='visibility',
            field=models.BooleanField(default=True, help_text='Public or Private', verbose_name='Visibility', choices=[(True, b'Public'), (False, b'Private')]),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='parent',
            field=models.ForeignKey(related_name='sub_mailbox', verbose_name='parent', blank=True, to='account.Mailbox', null=True),
        ),
    ]
