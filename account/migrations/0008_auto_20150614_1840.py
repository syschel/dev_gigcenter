# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0007_auto_20150614_1817'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailbox',
            name='status_sender',
        ),
        migrations.RemoveField(
            model_name='mailbox',
            name='status_to',
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='sender',
            field=models.ForeignKey(related_name='from_sender', verbose_name='sender(from)', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='to',
            field=models.ForeignKey(related_name='to_sender', verbose_name='recipient(to)', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
