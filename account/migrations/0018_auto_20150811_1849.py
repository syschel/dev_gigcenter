# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0017_auto_20150717_1756'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artistprofile',
            name='genre',
            field=models.ManyToManyField(to='common.Genre', verbose_name='\u0416\u0430\u043d\u0440\u044b'),
        ),
        migrations.AlterField(
            model_name='artistprofile',
            name='type_format',
            field=models.ManyToManyField(db_constraint='\u0424\u043e\u0440\u043c\u0430\u0442 \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f', to='common.FormatPerformance'),
        ),
    ]
