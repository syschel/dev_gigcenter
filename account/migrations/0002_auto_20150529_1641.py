# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agentprofile',
            name='user',
            field=models.ForeignKey(related_name='user_agent', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='artistprofile',
            name='user',
            field=models.ForeignKey(related_name='user_artist', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='clubprofile',
            name='user',
            field=models.ForeignKey(related_name='user_club', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
