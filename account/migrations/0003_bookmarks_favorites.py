# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('gigdate', '0004_auto_20150608_1346'),
        ('account', '0002_auto_20150529_1641'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bookmarks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_data', models.IntegerField(verbose_name='#\u0422\u0438\u043f \u0437\u0430\u043a\u043b\u0430\u0434\u043a\u0438', choices=[(0, '#\u0413\u0438\u0433\u0414\u0430\u0442\u0430'), (1, '#\u0411\u0430\u0440\u0442\u0435\u0440'), (2, '#\u0422\u0435\u043d\u0434\u0435\u0440 ')])),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e', null=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name='#\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e', null=True)),
                ('date_all', models.ForeignKey(related_name='bookmarks_date', verbose_name='#\u0414\u0430\u0442\u0430', blank=True, to='gigdate.DateAll', null=True)),
                ('user', models.ForeignKey(related_name='users_bookmarks', verbose_name='#\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': '#\u0417\u0430\u043a\u043b\u0430\u0434\u043a\u0438 \u043d\u0430 \u0434\u0430\u0442\u044b',
                'verbose_name_plural': '\u0417\u0430\u043a\u043b\u0430\u0434\u043a\u0438 \u043d\u0430 \u0434\u0430\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Favorites',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_data', models.IntegerField(verbose_name='#\u0422\u0438\u043f \u0438\u0437\u0431\u0440\u0430\u043d\u043d\u043e\u0433\u043e', choices=[(0, '#\u0410\u0440\u0442\u0438\u0441\u0442'), (1, '#\u0410\u0433\u0435\u043d\u0442'), (2, '#\u041a\u043b\u0443\u0431 ')])),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e', null=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name='#\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e', null=True)),
                ('agent', models.ForeignKey(related_name='favorites_agent', verbose_name='#\u0410\u0433\u0435\u043d\u0442', blank=True, to='account.AgentProfile', null=True)),
                ('artist', models.ForeignKey(related_name='favorites_artist', verbose_name='#\u0410\u0440\u0442\u0438\u0441\u0442', blank=True, to='account.ArtistProfile', null=True)),
                ('club', models.ForeignKey(related_name='favorites_club', verbose_name='#\u041a\u043b\u0443\u0431', blank=True, to='account.ClubProfile', null=True)),
                ('user', models.ForeignKey(related_name='users_favorites', verbose_name='#\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': '#\u0418\u0437\u0431\u0440\u0430\u043d\u043d\u043e\u0435 \u043f\u0440\u043e\u0444\u0438\u043b\u0435\u0439',
                'verbose_name_plural': '\u0418\u0437\u0431\u0440\u0430\u043d\u043d\u043e\u0435 \u043f\u0440\u043e\u0444\u0438\u043b\u0435\u0439',
            },
        ),
    ]
