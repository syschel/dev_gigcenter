# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0014_auto_20150717_1012'),
    ]

    operations = [
        migrations.AddField(
            model_name='artistimages',
            name='artist',
            field=models.ForeignKey(default=None, verbose_name='artist', to='account.ArtistProfile'),
            preserve_default=False,
        ),
    ]
