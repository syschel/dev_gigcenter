# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import common.files


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0004_auto_20150609_1410'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mailbox',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128, verbose_name='#\u0422\u0435\u043c\u0430')),
                ('text', models.TextField(verbose_name='#\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='#\u0414\u0430\u0442\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438')),
                ('file_f', models.FileField(upload_to=common.files.get_file_path, verbose_name='#\u0424\u0430\u0439\u043b', blank=True)),
                ('status_sender', models.IntegerField(default=0, verbose_name='#\u0421\u0442\u0430\u0442\u0443\u0441 \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044f', choices=[(0, '#\u0410\u043a\u0442\u0438\u0432\u043d\u043e'), (1, '#\u0412 \u043a\u043e\u0440\u0437\u0438\u043d\u0435'), (2, '#\u0423\u0434\u0430\u043b\u0435\u043d\u043e'), (3, '#\u041f\u0440\u043e\u0447\u0438\u0442\u0430\u043d\u043e')])),
                ('status_to', models.IntegerField(default=0, verbose_name='#\u0421\u0442\u0430\u0442\u0443\u0441 \u043f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044f', choices=[(0, '#\u0410\u043a\u0442\u0438\u0432\u043d\u043e'), (1, '#\u0412 \u043a\u043e\u0440\u0437\u0438\u043d\u0435'), (2, '#\u0423\u0434\u0430\u043b\u0435\u043d\u043e'), (3, '#\u041f\u0440\u043e\u0447\u0438\u0442\u0430\u043d\u043e')])),
                ('read_to', models.BooleanField(default=False, verbose_name='#\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u043d\u043e \u043f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u0435\u043c')),
                ('parent', models.ForeignKey(verbose_name='#\u041e\u0442\u0432\u0435\u0442 \u043d\u0430', blank=True, to='account.Mailbox', null=True)),
                ('sender', models.ForeignKey(related_name='from_sender', verbose_name='#\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('to', models.ForeignKey(related_name='to_sender', verbose_name='#\u041f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-date',),
                'verbose_name': '#\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
                'verbose_name_plural': '#\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
            },
        ),
    ]
