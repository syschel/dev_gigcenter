# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0010_auto_20150716_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agentprofile',
            name='nickname',
            field=models.CharField(max_length=250, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'Please use English language', code=b'invalid')]),
        ),
        migrations.AlterField(
            model_name='artistprofile',
            name='nickname',
            field=models.CharField(max_length=250, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'Please use English language', code=b'invalid')]),
        ),
        migrations.AlterField(
            model_name='clubprofile',
            name='nickname',
            field=models.CharField(max_length=250, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'Please use English language', code=b'invalid')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='first name', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'Please use English language', code=b'invalid')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='last name', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'Please use English language', code=b'invalid')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='mail_dates',
            field=models.BooleanField(default=True, verbose_name='Dates'),
        ),
    ]
