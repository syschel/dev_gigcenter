# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0005_agenttype'),
        ('account', '0009_auto_20150712_1635'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agentprofile',
            name='type_agent',
        ),
        migrations.AddField(
            model_name='agentprofile',
            name='type_agent',
            field=models.ManyToManyField(db_constraint='#\u0422\u0438\u043f \u0430\u0433\u0435\u043d\u0442\u0430', to='common.AgentType', blank=True),
        ),
    ]
