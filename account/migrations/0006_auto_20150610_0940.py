# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import common.files


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_mailbox'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='agentprofile',
            options={'ordering': ['nickname'], 'verbose_name': 'Agent', 'verbose_name_plural': '2. Agents'},
        ),
        migrations.AlterModelOptions(
            name='artistprofile',
            options={'ordering': ['nickname'], 'verbose_name': 'Artist', 'verbose_name_plural': '3. Artist'},
        ),
        migrations.AlterModelOptions(
            name='clubprofile',
            options={'ordering': ['nickname'], 'verbose_name': 'Venue', 'verbose_name_plural': '4. Venues'},
        ),
        migrations.AlterModelOptions(
            name='mailbox',
            options={'ordering': ('-date',), 'verbose_name': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', 'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f'},
        ),
        migrations.AlterModelOptions(
            name='registration',
            options={'verbose_name': '\u0420\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438', 'verbose_name_plural': '\u041f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u0435 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'User', 'verbose_name_plural': '1. Users'},
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='file_f',
            field=models.FileField(upload_to=common.files.get_file_path, verbose_name='File', blank=True),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='parent',
            field=models.ForeignKey(verbose_name='parent', blank=True, to='account.Mailbox', null=True),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='sender',
            field=models.ForeignKey(related_name='from_sender', verbose_name='sender', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='text',
            field=models.TextField(verbose_name='Message'),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='title',
            field=models.CharField(max_length=128, verbose_name='subject'),
        ),
        migrations.AlterField(
            model_name='mailbox',
            name='to',
            field=models.ForeignKey(related_name='to_sender', verbose_name='recipient', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
