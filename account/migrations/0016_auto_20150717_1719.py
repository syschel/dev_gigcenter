# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import common.files


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0015_artistimages_artist'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='artistsite',
            options={'verbose_name': 'Artist site', 'verbose_name_plural': 'Artist site'},
        ),
        migrations.AlterModelOptions(
            name='riderall',
            options={'verbose_name': 'Artist riders', 'verbose_name_plural': 'Artist riders'},
        ),
        migrations.AlterField(
            model_name='artistimages',
            name='artist',
            field=models.ForeignKey(verbose_name='artist', blank=True, to='account.ArtistProfile'),
        ),
        migrations.AlterField(
            model_name='artistimages',
            name='img',
            field=models.ImageField(upload_to=common.files.get_file_path, null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
        ),
    ]
