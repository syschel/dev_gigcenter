# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0008_auto_20150614_1840'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clubprofile',
            name='capacity_seat',
            field=models.PositiveIntegerField(default=1, verbose_name='#\u041f\u043e\u0441\u0430\u0434\u043e\u0447\u043d\u0430\u044f \u0432\u043c\u0435\u0441\u0442\u0438\u043c\u043e\u0441\u0442\u044c', blank=True),
        ),
        migrations.AlterField(
            model_name='clubprofile',
            name='capacity_total',
            field=models.PositiveIntegerField(default=1, verbose_name='#\u041e\u0431\u0449\u0430\u044f \u0432\u043c\u0435\u0441\u0442\u0438\u043c\u043e\u0441\u0442\u044c', blank=True),
        ),
        migrations.AlterField(
            model_name='clubprofile',
            name='music_zone',
            field=models.PositiveIntegerField(default=1, verbose_name='#\u041c\u0443\u0437\u044b\u043a\u0430\u043b\u044c\u043d\u044b\u0445 \u0437\u043e\u043d', blank=True),
        ),
    ]
