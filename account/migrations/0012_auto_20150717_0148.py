# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0011_auto_20150716_2132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agentprofile',
            name='type_agent',
            field=models.ManyToManyField(db_constraint='#\u0422\u0438\u043f \u0430\u0433\u0435\u043d\u0442\u0430', null=True, to='common.AgentType'),
        ),
    ]
