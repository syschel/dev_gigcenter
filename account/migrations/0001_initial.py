# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import re
import common.files
import account.models
import django.utils.timezone
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('common', '__first__'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name='email address')),
                ('first_name', models.CharField(blank=True, max_length=30, null=True, verbose_name='first name', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'The value must contain only letters, digits, underscores, hyphens or spaces.', code=b'invalid')])),
                ('last_name', models.CharField(blank=True, max_length=30, null=True, verbose_name='last name', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'The value must contain only letters, digits, underscores, hyphens or spaces.', code=b'invalid')])),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=False, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('is_moderator', models.BooleanField(default=False, help_text='#\u041f\u0440\u0430\u0432\u0430 \u0434\u043e\u0441\u0442\u0443\u043f\u0430 \u043a\u0430\u043a \u043c\u043e\u0434\u0435\u0440\u0430\u0442\u043e\u0440', verbose_name='moderator')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('birthday', models.DateField(null=True, verbose_name='#\u0414\u0430\u0442\u0430 \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f', blank=True)),
                ('phone', models.CharField(max_length=250, null=True, verbose_name='#\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('skype', models.CharField(max_length=50, null=True, verbose_name='#skype', blank=True)),
                ('icq', models.CharField(max_length=20, null=True, verbose_name='#icq', blank=True)),
                ('first_in', models.BooleanField(default=True, help_text='#\u041f\u0435\u0440\u0432\u043e\u0435 \u043f\u043e\u0441\u044f\u0449\u0435\u043d\u0438\u0435 - \u043f\u0440\u043e\u0444\u0438\u043b\u044c \u043d\u0435 \u0434\u043e\u0437\u0430\u043f\u043e\u043b\u043d\u0435\u043d', verbose_name='#\u041f\u0435\u0440\u0432\u044b\u0439 \u0432\u0445\u043e\u0434')),
                ('banned', models.BooleanField(default=False, help_text='#\u0417\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u043d \u0434\u043e\u0441\u0442\u0443\u043f \u043a \u0441\u0430\u0439\u0442\u0443', verbose_name='#\u0417\u0430\u0431\u0430\u043d\u0435\u043d')),
                ('mail_messages', models.BooleanField(default=False, verbose_name='Messages')),
                ('mail_trade', models.BooleanField(default=False, verbose_name='Requests')),
                ('mail_dates', models.BooleanField(default=False, verbose_name='Gig-dates')),
                ('mail_time', models.IntegerField(default=0, verbose_name='#\u0413\u0440\u0430\u0444\u0438\u043a \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f', choices=[(0, "I don't want to receive notifications"), (1, 'Once a day'), (2, 'Once a month')])),
                ('city', models.ForeignKey(verbose_name='#\u0413\u043e\u0440\u043e\u0434', to='common.City', null=True)),
                ('country', models.ForeignKey(verbose_name='#\u0421\u0442\u0440\u0430\u043d\u0430', to='common.Country', null=True)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'User',
                'verbose_name_plural': 'Users',
            },
        ),
        migrations.CreateModel(
            name='AgentProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e', null=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name='#\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e', null=True)),
                ('nickname', models.CharField(max_length=250, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'The value must contain only letters, digits, underscores, hyphens or spaces.', code=b'invalid')])),
                ('type_agent', models.IntegerField(default=0, verbose_name='#\u0422\u0438\u043f \u0430\u0433\u0435\u043d\u0442\u0430', choices=[(0, 'Private agent'), (1, 'Agency')])),
                ('jobs', models.TextField(null=True, verbose_name='#\u0442\u0435\u043a\u0443\u0449\u0435\u0435 \u043c\u0435\u0441\u0442\u043e \u0440\u0430\u0431\u043e\u0442\u044b')),
                ('avatar', models.ImageField(upload_to=common.files.get_file_path, null=True, verbose_name='#\u0410\u0432\u0430\u0442\u0430\u0440', blank=True)),
                ('address', models.CharField(max_length=250, null=True, verbose_name='#\u0410\u0434\u0440\u0435\u0441')),
                ('phone', models.CharField(max_length=250, null=True, verbose_name='#\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('email', models.CharField(max_length=250, null=True, verbose_name='#email', blank=True)),
                ('info', models.TextField(null=True, verbose_name='\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', blank=True)),
                ('view', models.IntegerField(default=0, verbose_name='#\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u044b', blank=True)),
                ('banned', models.BooleanField(default=False, help_text='#\u0417\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u043d', verbose_name='#\u0417\u0430\u0431\u0430\u043d\u0435\u043d')),
                ('city', models.ForeignKey(verbose_name='City', to='common.City', null=True)),
                ('country', models.ForeignKey(verbose_name='Country', to='common.Country', null=True)),
            ],
            options={
                'ordering': ['nickname'],
                'verbose_name': 'Agent',
                'verbose_name_plural': 'Agents',
            },
        ),
        migrations.CreateModel(
            name='AgentSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site', models.CharField(max_length=250, verbose_name='#\u0421\u0430\u0439\u0442')),
                ('agent', models.ForeignKey(verbose_name='#\u0410\u0433\u0435\u043d\u0442', to='account.AgentProfile')),
            ],
            options={
                'verbose_name': '#\u0421\u0430\u0439\u0442 \u0430\u0433\u0435\u043d\u0442\u0430',
                'verbose_name_plural': '#\u0421\u0430\u0439\u0442 \u0430\u0433\u0435\u043d\u0442\u0430',
            },
        ),
        migrations.CreateModel(
            name='ArtistProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e', null=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name='#\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e', null=True)),
                ('nickname', models.CharField(max_length=250, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'The value must contain only letters, digits, underscores, hyphens or spaces.', code=b'invalid')])),
                ('avatar', models.ImageField(upload_to=common.files.get_file_path, null=True, verbose_name='#\u0410\u0432\u0430\u0442\u0430\u0440', blank=True)),
                ('label', models.TextField(null=True, verbose_name='\u041b\u0435\u0439\u0431\u043b', blank=True)),
                ('phone', models.CharField(max_length=250, null=True, verbose_name='#\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('email', models.CharField(max_length=250, null=True, verbose_name='#email', blank=True)),
                ('biography', models.TextField(null=True, verbose_name='#\u0411\u0438\u043e\u0433\u0440\u0430\u0444\u0438\u044f', blank=True)),
                ('dogovor', models.FileField(upload_to=common.files.get_file_path, null=True, verbose_name='#\u0424\u0430\u0439\u043b \u0414\u043e\u0433\u043e\u0432\u043e\u0440\u0430', blank=True)),
                ('video', models.CharField(max_length=255, null=True, verbose_name='#\u0412\u0438\u0434\u0435\u043e', blank=True)),
                ('mix', models.CharField(max_length=255, null=True, verbose_name='#\u041c\u0438\u043a\u0441', blank=True)),
                ('track', models.CharField(max_length=255, null=True, verbose_name='#\u0422\u0440\u0435\u043a', blank=True)),
                ('reliz', models.CharField(max_length=255, null=True, verbose_name='#\u0420\u0435\u043b\u0438\u0437', blank=True)),
                ('view', models.IntegerField(default=0, verbose_name='#\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u044b', blank=True)),
                ('banned', models.BooleanField(default=False, help_text='#\u0417\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u043d', verbose_name='#\u0417\u0430\u0431\u0430\u043d\u0435\u043d')),
                ('agent', models.ForeignKey(related_name='agent_artist', verbose_name='Agent', blank=True, to='account.AgentProfile', null=True)),
                ('city', models.ForeignKey(verbose_name='City', to='common.City', null=True)),
                ('country', models.ForeignKey(verbose_name='Country', to='common.Country', null=True)),
                ('genre', models.ManyToManyField(to='common.Genre', verbose_name='\u0416\u0430\u043d\u0440\u044b', blank=True)),
            ],
            options={
                'ordering': ['nickname'],
                'verbose_name': 'Artist',
                'verbose_name_plural': 'Artist',
            },
        ),
        migrations.CreateModel(
            name='ArtistSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site', models.CharField(max_length=250, verbose_name='#\u0421\u0430\u0439\u0442')),
                ('artist', models.ForeignKey(verbose_name='artist', to='account.ArtistProfile')),
            ],
            options={
                'verbose_name': '#\u0421\u0430\u0439\u0442 \u0430\u0440\u0442\u0438\u0441\u0442\u0430',
                'verbose_name_plural': '#\u0421\u0430\u0439\u0442\u044b \u0430\u0440\u0442\u0438\u0441\u0442\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='ClubProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e', null=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name='#\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u043e', null=True)),
                ('nickname', models.CharField(max_length=250, null=True, verbose_name='\u041f\u0441\u0435\u0432\u0434\u043e\u043d\u0438\u043c', validators=[django.core.validators.RegexValidator(re.compile(b'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$'), 'The value must contain only letters, digits, underscores, hyphens or spaces.', code=b'invalid')])),
                ('avatar', models.ImageField(upload_to=common.files.get_file_path, null=True, verbose_name='#\u0410\u0432\u0430\u0442\u0430\u0440', blank=True)),
                ('address', models.CharField(max_length=250, null=True, verbose_name='#\u0410\u0434\u0440\u0435\u0441')),
                ('phone', models.CharField(max_length=250, null=True, verbose_name='#\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('email', models.CharField(max_length=250, null=True, verbose_name='#email', blank=True)),
                ('place_street', models.BooleanField(default=True, verbose_name='#\u041c\u0435\u0441\u0442\u043e \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f \u043d\u0430 \u0443\u043b\u0438\u0446\u0435')),
                ('place_indoors', models.BooleanField(default=True, verbose_name='#\u041c\u0435\u0441\u0442\u043e \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f \u0432 \u043f\u043e\u043c\u0435\u0448\u0435\u043d\u0438\u0438')),
                ('music_zone', models.IntegerField(default=1, verbose_name='#\u041c\u0443\u0437\u044b\u043a\u0430\u043b\u044c\u043d\u044b\u0445 \u0437\u043e\u043d', blank=True)),
                ('capacity_total', models.IntegerField(default=1, verbose_name='#\u041e\u0431\u0449\u0430\u044f \u0432\u043c\u0435\u0441\u0442\u0438\u043c\u043e\u0441\u0442\u044c', blank=True)),
                ('capacity_seat', models.IntegerField(default=1, verbose_name='#\u041f\u043e\u0441\u0430\u0434\u043e\u0447\u043d\u0430\u044f \u0432\u043c\u0435\u0441\u0442\u0438\u043c\u043e\u0441\u0442\u044c', blank=True)),
                ('nearest_airport', models.CharField(max_length=250, null=True, verbose_name='#\u0411\u043b\u0438\u0436\u0430\u0439\u0448\u0438\u0439 \u0430\u044d\u0440\u043e\u043f\u043e\u0440\u0442 ', blank=True)),
                ('nearest_station', models.CharField(max_length=250, null=True, verbose_name='#\u0411\u043b\u0438\u0436\u0430\u0439\u0448\u0438\u0439 \u0436\u0434 \u0432\u043e\u043a\u0437\u0430\u043b', blank=True)),
                ('artists_club', models.CharField(max_length=250, null=True, verbose_name='#\u0410\u0440\u0442\u0438\u0441\u0442\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0443\u0436\u0435 \u0432\u044b\u0441\u0442\u0443\u043f\u0430\u043b\u0438 \u043d\u0430 \u043f\u043b\u043e\u0449\u0430\u0434\u043a\u0435', blank=True)),
                ('info', models.TextField(null=True, verbose_name='\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', blank=True)),
                ('music', models.CharField(max_length=255, null=True, verbose_name='#\u041c\u0443\u0437\u044b\u043a\u0430', blank=True)),
                ('video', models.CharField(max_length=255, null=True, verbose_name='#\u0412\u0438\u0434\u0435\u043e', blank=True)),
                ('view', models.IntegerField(default=0, verbose_name='#\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440\u044b', blank=True)),
                ('banned', models.BooleanField(default=False, help_text='#\u0417\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u043d', verbose_name='#\u0417\u0430\u0431\u0430\u043d\u0435\u043d')),
                ('agent', models.ForeignKey(related_name='agent_club', verbose_name='Agent', blank=True, to='account.AgentProfile', null=True)),
                ('city', models.ForeignKey(verbose_name='City', to='common.City', null=True)),
                ('country', models.ForeignKey(verbose_name='Country', to='common.Country', null=True)),
                ('genre', models.ManyToManyField(to='common.Genre', verbose_name='\u0416\u0430\u043d\u0440\u044b')),
            ],
            options={
                'ordering': ['nickname'],
                'verbose_name': 'Club',
                'verbose_name_plural': 'Clubs',
            },
        ),
        migrations.CreateModel(
            name='ClubSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('site', models.CharField(max_length=250, verbose_name='#\u0421\u0430\u0439\u0442')),
                ('club', models.ForeignKey(verbose_name='club', to='account.ClubProfile')),
            ],
            options={
                'verbose_name': '#\u0421\u0430\u0439\u0442 \u043a\u043b\u0443\u0431\u0430',
                'verbose_name_plural': '#\u0421\u0430\u0439\u0442\u044b \u043a\u0434\u0443\u0431\u0430',
            },
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', models.CharField(default=account.models.get_uuid_str, max_length=36)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e', null=True)),
                ('user', models.OneToOneField(related_name='registration', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '#\u0420\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438',
                'verbose_name_plural': '#\u041f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u0435 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='RiderAll',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=common.files.get_file_path, null=True, verbose_name='#\u0424\u0430\u0439\u043b \u0440\u0430\u0439\u0434\u0435\u0440\u0430', blank=True)),
                ('artist', models.ForeignKey(verbose_name='artist', blank=True, to='account.ArtistProfile')),
            ],
            options={
                'verbose_name': '#\u0420\u0430\u0439\u0434\u0435\u0440 \u0430\u0440\u0442\u0438\u0441\u0442\u0430',
                'verbose_name_plural': '#\u0420\u0430\u0439\u0434\u0435\u0440\u044b \u0430\u0440\u0442\u0438\u0441\u0442\u0430',
            },
        ),
        migrations.AddField(
            model_name='clubprofile',
            name='site',
            field=models.ManyToManyField(to='account.ClubSite', verbose_name='#\u0421\u0430\u0439\u0442\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='clubprofile',
            name='type_club',
            field=models.ManyToManyField(to='common.TypeClub', verbose_name='\u0422\u0438\u043f \u043f\u043b\u043e\u0449\u0430\u0434\u043a\u0438'),
        ),
        migrations.AddField(
            model_name='clubprofile',
            name='user',
            field=models.OneToOneField(related_name='user_club', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='artistprofile',
            name='rider',
            field=models.ManyToManyField(to='account.RiderAll', verbose_name='#\u0420\u0430\u0439\u0434\u0435\u0440\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='artistprofile',
            name='site',
            field=models.ManyToManyField(to='account.ArtistSite', verbose_name='#\u0421\u0430\u0439\u0442\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='artistprofile',
            name='type_format',
            field=models.ManyToManyField(db_constraint='\u0424\u043e\u0440\u043c\u0430\u0442 \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f', to='common.FormatPerformance', blank=True),
        ),
        migrations.AddField(
            model_name='artistprofile',
            name='user',
            field=models.OneToOneField(related_name='user_artist', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='agentprofile',
            name='site',
            field=models.ManyToManyField(to='account.AgentSite', verbose_name='#\u0421\u0430\u0439\u0442\u044b', blank=True),
        ),
        migrations.AddField(
            model_name='agentprofile',
            name='user',
            field=models.OneToOneField(related_name='user_agent', verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='clubsite',
            unique_together=set([('site', 'club')]),
        ),
        migrations.AlterUniqueTogether(
            name='artistsite',
            unique_together=set([('site', 'artist')]),
        ),
        migrations.AlterUniqueTogether(
            name='agentsite',
            unique_together=set([('site', 'agent')]),
        ),
    ]
