# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0018_auto_20150811_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='agentprofile',
            name='visibility',
            field=models.BooleanField(default=True, help_text='Public or Private', verbose_name='Visibility'),
        ),
        migrations.AddField(
            model_name='artistprofile',
            name='visibility',
            field=models.BooleanField(default=True, help_text='Public or Private', verbose_name='Visibility'),
        ),
        migrations.AddField(
            model_name='clubprofile',
            name='visibility',
            field=models.BooleanField(default=True, help_text='Public or Private', verbose_name='Visibility'),
        ),
    ]
