# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import common.files


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0016_auto_20150717_1719'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClubImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('img', models.ImageField(upload_to=common.files.get_file_path, null=True, verbose_name='images', blank=True)),
                ('club', models.ForeignKey(verbose_name='Venue', to='account.ClubProfile')),
            ],
            options={
                'verbose_name': 'Venue photo',
                'verbose_name_plural': 'Venue photo',
            },
        ),
        migrations.AlterField(
            model_name='artistprofile',
            name='images',
            field=models.ManyToManyField(to='account.ArtistImages', verbose_name='Images', blank=True),
        ),
        migrations.AlterField(
            model_name='clubsite',
            name='club',
            field=models.ForeignKey(verbose_name='Venue', to='account.ClubProfile'),
        ),
        migrations.AddField(
            model_name='clubprofile',
            name='images',
            field=models.ManyToManyField(to='account.ClubImages', verbose_name='Images', blank=True),
        ),
    ]
