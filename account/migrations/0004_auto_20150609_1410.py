# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_bookmarks_favorites'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookmarks',
            name='date_all',
        ),
        migrations.RemoveField(
            model_name='bookmarks',
            name='user',
        ),
        migrations.RemoveField(
            model_name='favorites',
            name='agent',
        ),
        migrations.RemoveField(
            model_name='favorites',
            name='artist',
        ),
        migrations.RemoveField(
            model_name='favorites',
            name='club',
        ),
        migrations.RemoveField(
            model_name='favorites',
            name='user',
        ),
        migrations.DeleteModel(
            name='Bookmarks',
        ),
        migrations.DeleteModel(
            name='Favorites',
        ),
    ]
