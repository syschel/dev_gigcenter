# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import common.files


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0013_auto_20150717_0150'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArtistImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('img', models.ImageField(upload_to=common.files.get_file_path, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': 'Artist photo',
                'verbose_name_plural': 'Artist photo',
            },
        ),
        migrations.AddField(
            model_name='artistprofile',
            name='images',
            field=models.ManyToManyField(to='account.ArtistImages', verbose_name='Photos', blank=True),
        ),
    ]
