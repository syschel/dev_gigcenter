# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_auto_20150610_0940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='mail_dates',
            field=models.BooleanField(default=True, verbose_name='Gig-dates'),
        ),
        migrations.AlterField(
            model_name='user',
            name='mail_messages',
            field=models.BooleanField(default=True, verbose_name='Messages'),
        ),
        migrations.AlterField(
            model_name='user',
            name='mail_time',
            field=models.IntegerField(default=1, verbose_name='#\u0413\u0440\u0430\u0444\u0438\u043a \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f', choices=[(0, "I don't want to receive notifications"), (1, 'Once a day'), (2, 'Once a month')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='mail_trade',
            field=models.BooleanField(default=True, verbose_name='Requests'),
        ),
    ]
