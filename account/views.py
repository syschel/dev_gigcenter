#-*- coding:utf-8 -*-
import re
from datetime import datetime
from django.shortcuts import render, get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.hashers import make_password
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.contrib import messages
from django.conf import settings
from django.db.models import Q
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic.edit import FormView, UpdateView
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic import DetailView, ListView, View
from django.contrib.auth import login, logout
from annoying.decorators import render_to
from account.forms import SignInForm, AccountForm, AgentForm, ArtistForm, ClubForm, MailboxAjaxForm, MailboxFromForm, RePasswordForm
from common.models import City
from account.models import User, AgentProfile, AgentSite, ArtistProfile, ArtistSite, ArtistImages, RiderAll,\
    ClubProfile, ClubSite, ClubImages, Mailbox
from trade.models import Trades
from gigdate.models import GigDate, GigTender


class MainView(UpdateView):
    template_name = 'account/profiles/user.html'
    model = User
    form_class = AccountForm
    success_url = reverse_lazy('account:main')
    active_link = 'user_url'

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        clean = form.cleaned_data
        if clean.get('new_pass2'):
            form.user.password = make_password(clean.get('new_pass2'))
            form.user.save()
            form.user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(self.request, form.user)
        if self.request.user.first_in:
            form.user.first_in = False
            form.user.save()
        messages.info(self.request, _('Profile saved successfully'))
        return super(MainView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context[self.active_link] = True
        return context


class LogoutView(RedirectView):

    permanent = False
    url = reverse_lazy('signin')

    def dispatch(self, request, *args, **kwargs):
        try:
            logout(request)
        except logout:
            pass
        messages.info(request, _('You have successfully logged out. See you next time.'))
        return super(LogoutView, self).dispatch(request, *args, **kwargs)


class SigninFormView(FormView):
    template_name = 'account/login.html'
    form_class = SignInForm
    code_error = False
    success_url = reverse_lazy('home')
    error_facebook = False

    def render_to_response(self, context):
        if self.request.user.is_authenticated():
            return HttpResponseRedirect(reverse('account:main'))
        return super(SigninFormView, self).render_to_response(context)

    def form_valid(self, form):
        if 'login' in self.request.POST:
            form.user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(self.request, form.user)
            messages.info(self.request, _('You have successfully logged in.'))
        else:
            User.objects.create_site_user(email=form.cleaned_data['email'], password=form.cleaned_data['password'], get_host=self.request.get_host())
            messages.info(self.request, _('Please check Your email to complete the registration.'))
            return HttpResponseRedirect(reverse('signin'))
        return super(SigninFormView, self).form_valid(form)

    def form_invalid(self, form):
        import json
        errors_json = json.loads(form.errors.as_json())
        errors_json = errors_json.get('__all__', False)
        if errors_json:
            errors_json = errors_json[0].get('code')
            if errors_json == 'is_active':
                self.code_error = True
        return super(SigninFormView, self).form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(SigninFormView, self).get_context_data(**kwargs)
        context['code_error'] = self.code_error
        if self.error_facebook:
            context['error_facebook'] = self.error_facebook
        return context

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form.
        """
        if request.GET:
            if 'code' in request.GET:
                self._auth_facebook()
            elif 'error' in request.GET:
                self.error_facebook = request.GET.get('error_reason')
        return self.render_to_response(self.get_context_data())

    def _auth_facebook(self):
        from account.auth.facebook import FacebookAuth
        fb_auth = FacebookAuth()
        user = fb_auth.login_fb(self.request)
        user.backend = 'account.auth.facebook.FacebookBackend'
        login(self.request, user)
        return HttpResponseRedirect(reverse('account:main'))


class IsActiveView(TemplateView):
    template_name = 'elements/is_active.html'

    def get(self, request, *args, **kwargs):
        if kwargs.get('resend'):
            message = _(u'Hi, send please') + ' http://%s/user_activation/?key=%s' % (self.request.get_host(), request.user.registration.uuid)
            request.user.email_user(_('Gigcenter: re validation email'), message)
            messages.info(self.request, _('Confirmation is sent on Your email.'))
            return HttpResponseRedirect(reverse('is_active'))
        return super(IsActiveView, self).get(request, *args, **kwargs)


class ActivationView(RedirectView):
    permanent = False
    success_url = reverse_lazy('signin')

    def get(self, request, *args, **kwargs):
        uuid = request.GET.get('key', None)
        if uuid is None:
            raise Http404
        try:
            # This line changed
            user = User.objects.get(registration__uuid=uuid)
            user.is_active = True
            user.save()
            user.registration.delete()
            messages.info(self.request, _('You successfully activated Your email'))
            return HttpResponseRedirect(reverse('signin'))
        except:
            raise Http404
        # return super(ActivationView, self).get(request, *args, **kwargs)


class RePasswordFormView(FormView):
    form_class = RePasswordForm
    template_name = 'elements/repassword.html'
    success_url = reverse_lazy('signin')

    def form_valid(self, form):
        form.user.backend = 'django.contrib.auth.backends.ModelBackend'
        new_pass = User.objects.make_random_password()
        form.user.set_password(new_pass)
        form.user.save()
        message = _('Your new password:')+' %s' % new_pass
        form.user.email_user(_('Gigcenter: re password'), message)
        messages.info(self.request, _('Please, check Your email.'))
        return super(RePasswordFormView, self).form_valid(form)


class AjaxCity(View):
    """
    GET: /ajax_city/id/
    """
    template_name = "common/city_select.html"

    def get(self, request, *args, **kwargs):
        country_id = kwargs.get('id')
        city_list = City.objects.filter(country_id=country_id)
        return render(request, self.template_name, {'success': city_list})


class AjaxArtist(View):
    """
    GET: /account/get_artist/id/
    """
    def get(self, request, *args, **kwargs):
        artist_id = kwargs.get('id')
        artists = ArtistProfile.objects.get(pk=int(artist_id))
        artist={
            'nickname': u"%s" % artists.nickname,
            'country': {
                'width': artists.country.width,
                'height': artists.country.height,
                'title': artists.country.title,
            },
            'city': u"%s" % artists.city.title

        }
        if artists.avatar:
            artist['avatar'] = u"%s%s" % (settings.MEDIA_URL, artists.avatar)
        else:
            artist['avatar'] = u"%s%s" % (settings.STATIC_URL, 'images/bg-ava.gif')
        genres = []
        for genre in artists.genre.all():
            genres.append(genre.title)
        artist['genres'] = ", ".join(genres)
        return JsonResponse(artist)


class AjaxClub(View):
    """
    GET: /account/get_artist/id/
    """
    def get(self, request, *args, **kwargs):
        club_id = kwargs.get('id')
        clubs = ClubProfile.objects.get(pk=int(club_id))
        club={
            'nickname': u"%s" % clubs.nickname,
            'country': {
                'width': clubs.country.width,
                'height': clubs.country.height,
                'title': clubs.country.title,
            },
            'city': u"%s" % clubs.city.title

        }
        if clubs.avatar:
            club['avatar'] = u"%s%s" % (settings.MEDIA_URL, clubs.avatar)
        else:
            club['avatar'] = u"%s%s" % (settings.STATIC_URL, 'images/bg-ava.gif')
        genres = []
        for genre in clubs.genre.all():
            genres.append(genre.title)
        club['genres'] = ", ".join(genres)
        return JsonResponse(club)


class MailboxAjax(FormView):
    """
    POST: /account/mailbox_ajax/
    """
    form_class = MailboxAjaxForm

    def get(self, request, *args, **kwargs):
        return JsonResponse({})

    def form_valid(self, form):
        f = form.save(commit=False)
        f.sender = self.request.user
        f.save()
        return JsonResponse({'status': 'ok'})

    def form_invalid(self, form):
        return JsonResponse({'status': 'error'})


class AgentFormView(FormView):
    """
    Обновление профиля агента
    """
    template_name = 'account/profiles/agent.html'
    form_class = AgentForm
    success_url = reverse_lazy('account:agent')
    active_link = 'agent_url'

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        try:
            agent = AgentProfile.objects.get(user=self.request.user)
            return form_class(instance=agent, **self.get_form_kwargs())
        except AgentProfile.DoesNotExist:
            return form_class(**self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(AgentFormView, self).get_context_data(**kwargs)
        context[self.active_link] = True
        return context

    def post(self, request, *args, **kwargs):
        """
        Большой бубен из-за м2м, которые не могу создавать (добавлять obj) из формы
        """
        # form = self.form_class(request.POST, request.FILES)
        try:
            agent = AgentProfile.objects.get(user=self.request.user)
            form = self.form_class(instance=agent, **self.get_form_kwargs())
        except AgentProfile.DoesNotExist:
            form = self.form_class(**self.get_form_kwargs())

        if form.is_valid():
            f = form.save(commit=False)
            f.user = self.request.user
            f.save()
            form.save_m2m()
            for site in request.POST.getlist('site'):
                if site:
                    f.site.add(AgentSite.objects.get_or_create(site=re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)",
                                                                           "", site), agent=f)[0])
            for site_clear in request.POST.getlist('site_clear'):
                f.site.remove(AgentSite.objects.get(pk=int(site_clear), agent=f))
                AgentSite.objects.get(pk=int(site_clear), agent=f).delete()
            f.save()
            messages.info(self.request, _('Profile saved.'))
            return HttpResponseRedirect(self.success_url)

        return render(request, self.template_name, {'form': form})


class ArtistFormView(FormView):
    """
    Обновление профиля агента
    """
    template_name = 'account/profiles/artist.html'
    form_class = ArtistForm
    success_url = reverse_lazy('account:artist')
    active_link = 'artist_url'
    update_form = True
    edit_url = False
    agent_user = False

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        try:
            artist = ArtistProfile.objects.get(user=self.request.user, agent=None)
            return form_class(instance=artist, **self.get_form_kwargs())
        except ArtistProfile.DoesNotExist:
            return form_class(**self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(ArtistFormView, self).get_context_data(**kwargs)
        context[self.active_link] = True
        return context

    def post(self, request, *args, **kwargs):
        """
        Большой бубен из-за м2м, которые не могу создавать (добавлять obj) из формы
        """
        if not self.update_form:
            form = self.form_class(**self.get_form_kwargs())
        else:
            try:
                if self.edit_url:
                    artist = ArtistProfile.objects.get(uuid_id=self.kwargs.get('slug'), user=self.request.user)
                else:
                    artist = ArtistProfile.objects.get(user=self.request.user, agent=None)
                form = self.form_class(instance=artist, **self.get_form_kwargs())
            except ArtistProfile.DoesNotExist:
                form = self.form_class(**self.get_form_kwargs())

        if form.is_valid():
            f = form.save(commit=False)
            f.user = self.request.user
            if self.agent_user:
                f.agent = AgentProfile.objects.get(user=self.request.user)
            f.save()
            form.save_m2m()
            for site in request.POST.getlist('site'):
                if site:
                    f.site.add(ArtistSite.objects.get_or_create(site=re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)",
                                                                           "", site), artist=f)[0])
            for site_clear in request.POST.getlist('site_clear'):
                f.site.remove(ArtistSite.objects.get(pk=int(site_clear), artist=f))
                ArtistSite.objects.get(pk=int(site_clear), artist=f).delete()
            for rider in request.FILES.getlist('rider'):
                if rider:
                    f.rider.add(RiderAll.objects.get_or_create(file=rider, artist=f)[0])
            for rider in request.POST.getlist('rider_clear'):
                f.rider.remove(RiderAll.objects.get(pk=int(rider), artist=f))
                RiderAll.objects.get(pk=int(rider), artist=f).delete()
            for photo in request.FILES.getlist('photo'):
                if photo:
                    f.images.add(ArtistImages.objects.get_or_create(img=photo, artist=f)[0])
            for photo in request.POST.getlist('photo_clear'):
                f.images.remove(ArtistImages.objects.get(pk=int(photo), artist=f))
                ArtistImages.objects.get(pk=int(photo), artist=f).delete()
            if request.POST.get('dogovor_clear'):
                f.dogovor = None
            f.save()
            if self.edit_url:
                messages.info(self.request, _('Profile saved'))
                return HttpResponseRedirect(reverse('account:agent_artist_edit', kwargs={'slug': f.uuid_id}))
            messages.info(self.request, _('Profile saved'))
            return HttpResponseRedirect(self.success_url)

        contexts = {'form': form}
        contexts[self.active_link] = True
        return render(request, self.template_name, contexts)


class ClubFormView(FormView):
    """
    Обновление профиля агента ClubProfile, ClubSite
    """
    template_name = 'account/profiles/club.html'
    form_class = ClubForm
    success_url = reverse_lazy('account:club')
    active_link = 'club_url'
    update_form = True
    edit_url = False
    agent_user = False

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        try:
            club = ClubProfile.objects.get(user=self.request.user, agent=None)
            return form_class(instance=club, **self.get_form_kwargs())
        except ClubProfile.DoesNotExist:
            return form_class(**self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(ClubFormView, self).get_context_data(**kwargs)
        context[self.active_link] = True
        return context

    def post(self, request, *args, **kwargs):
        """
        Большой бубен из-за м2м, которые не могу создавать (добавлять obj) из формы
        """
        if not self.update_form:
            form = self.form_class(**self.get_form_kwargs())
        else:
            try:
                if self.edit_url:
                    club = ClubProfile.objects.get(uuid_id=self.kwargs.get('slug'), user=self.request.user)
                else:
                    club = ClubProfile.objects.get(user=self.request.user, agent=None)
                form = self.form_class(instance=club, **self.get_form_kwargs())
            except ClubProfile.DoesNotExist:
                form = self.form_class(**self.get_form_kwargs())

        if form.is_valid():
            f = form.save(commit=False)
            f.user = self.request.user
            if self.agent_user:
                f.agent = AgentProfile.objects.get(user=self.request.user)
            f.save()
            form.save_m2m()
            for site in request.POST.getlist('site'):
                if site:
                    f.site.add(ClubSite.objects.get_or_create(site=re.sub(r"^(https:\/\/)|(http:\/\/)|(ftp:\/\/)",
                                                                          "", site), club=f)[0])
            for site_clear in request.POST.getlist('site_clear'):
                f.site.remove(ClubSite.objects.get(pk=int(site_clear), club=f))
                ClubSite.objects.get(pk=int(site_clear), club=f).delete()

            for photo in request.FILES.getlist('photo'):
                if photo:
                    f.images.add(ClubImages.objects.get_or_create(img=photo, club=f)[0])
            for photo in request.POST.getlist('photo_clear'):
                f.images.remove(ClubImages.objects.get(pk=int(photo), club=f))
                ClubImages.objects.get(pk=int(photo), club=f).delete()
            f.save()
            if self.edit_url:
                messages.info(self.request, _('Profile saved'))
                return HttpResponseRedirect(reverse('account:agent_club_edit', kwargs={'slug': f.uuid_id}))
            messages.info(self.request, _('Profile saved'))
            return HttpResponseRedirect(self.success_url)

        contexts = {'form': form}
        contexts[self.active_link] = True
        return render(request, self.template_name, contexts)


class AgentArtistListView(ListView):
    """
    Список артистов агента
    """
    context_object_name = 'artists'
    template_name = 'account/profiles/list_artist.html'
    active_link = 'agent_artists_url'
    paginate_by = 30

    def render_to_response(self, context):
        if not AgentProfile.objects.filter(user=self.request.user).exists():
            messages.info(self.request, _('You have no agent profile.'))
            return HttpResponseRedirect(reverse('account:agent'))
        return super(AgentArtistListView, self).render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(AgentArtistListView, self).get_context_data(**kwargs)
        context[self.active_link] = True
        return context

    def get_queryset(self):
        return ArtistProfile.objects.filter(user=self.request.user, agent__isnull=False)


class AgentClubListView(ListView):
    """
    Список клубов агента
    """
    context_object_name = 'clubs'
    template_name = 'account/profiles/list_club.html'
    active_link = 'agent_venues_url'
    paginate_by = 30

    def render_to_response(self, context):
        if not AgentProfile.objects.filter(user=self.request.user).exists():
            messages.info(self.request, _('You have no agent profile.'))
            return HttpResponseRedirect(reverse('account:agent'))
        return super(AgentClubListView, self).render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(AgentClubListView, self).get_context_data(**kwargs)
        context[self.active_link] = True
        return context

    def get_queryset(self):
        return ClubProfile.objects.filter(user=self.request.user, agent__isnull=False)


class AgentClubCreateView(ClubFormView):
    """
    Создание клуба агента
    """
    success_url = reverse_lazy('account:agent_club')
    active_link = 'agent_venues_url'
    update_form = False
    agent_user = True
    edit_url = True

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(**self.get_form_kwargs())

    def render_to_response(self, context):
        if not AgentProfile.objects.filter(user=self.request.user).exists():
            messages.info(self.request, _('You have no agent profile.'))
            return HttpResponseRedirect(reverse('account:agent'))
        return super(AgentClubCreateView, self).render_to_response(context)


class AgentArtistCreateView(ArtistFormView):
    """
    Создание артиста агента
    """
    success_url = reverse_lazy('account:agent_artist')
    active_link = 'agent_artists_url'
    update_form = False
    agent_user = True
    edit_url = True

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(**self.get_form_kwargs())

    def render_to_response(self, context):
        if not AgentProfile.objects.filter(user=self.request.user).exists():
            messages.info(self.request, _('You have no agent profile.'))
            return HttpResponseRedirect(reverse('account:agent'))
        return super(AgentArtistCreateView, self).render_to_response(context)


class AgentClubUpdateView(ClubFormView):
    """
    Редактирование клуба агента
    """
    success_url = reverse_lazy('account:agent_club')
    active_link = 'agent_venues_url'
    update_form = True
    agent_user = True
    edit_url = True

    def render_to_response(self, context):
        if not AgentProfile.objects.filter(user=self.request.user).exists():
            messages.info(self.request, _('You have no agent profile.'))
            return HttpResponseRedirect(reverse('account:agent'))
        return super(AgentClubUpdateView, self).render_to_response(context)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        try:
            club = ClubProfile.objects.get(uuid_id=self.kwargs.get('slug'), user=self.request.user)
            return form_class(instance=club, **self.get_form_kwargs())
        except ClubProfile.DoesNotExist:
            return form_class(**self.get_form_kwargs())


class AgentArtistUpdateView(ArtistFormView):
    """
    Создание артиста агента
    """
    success_url = reverse_lazy('account:agent_artist')
    active_link = 'agent_artists_url'
    update_form = True
    agent_user = True
    edit_url = True

    def render_to_response(self, context):
        if not AgentProfile.objects.filter(user=self.request.user).exists():
            messages.info(self.request, _('You have no agent profile.'))
            return HttpResponseRedirect(reverse('account:agent'))
        return super(AgentArtistUpdateView, self).render_to_response(context)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        try:
            artist = ArtistProfile.objects.get(uuid_id=self.kwargs.get('slug'), user=self.request.user)
            return form_class(instance=artist, **self.get_form_kwargs())
        except ArtistProfile.DoesNotExist:
            return form_class(**self.get_form_kwargs())


class ArtistListView(ListView):
    """
    Вывод списка артистов на сайте
    """
    model = ArtistProfile
    template_name = 'account/profiles/public/artists_list.html'
    context_object_name = 'users'
    paginate_by = 40


class ClubsListView(ListView):
    """
    Вывод списка клубов на сайте
    """
    model = ClubProfile
    template_name = 'account/profiles/public/artists_list.html'
    context_object_name = 'users'
    paginate_by = 40

    def get_context_data(self, **kwargs):
        context = super(ClubsListView, self).get_context_data(**kwargs)
        context['workaround'] = True
        return context


class AgentsListView(ListView):
    """
    Вывод списка агентов на сайте
    """
    model = AgentProfile
    template_name = 'account/profiles/public/artists_list.html'
    context_object_name = 'users'
    paginate_by = 40

    #  workaround
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse("users:main"))


class ArtistDetailView(DetailView):
    """
    Вывод артиста на сайте
    """
    model = ArtistProfile
    template_name = 'account/profiles/public/artist.html'
    context_object_name = 'artist'
    slug_field = 'uuid_id'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.visibility and not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('signin'))
        if request.user.is_authenticated() and not self.object.user == self.request.user:
            self.object.view += 1
            self.object.save()
        return super(ArtistDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ArtistDetailView, self).get_context_data(**kwargs)
        artist = context.get('artist')
        context['trade_count'] = Trades.objects.filter(Q(status_trade=Trades.STRADE2), Q(user_date=artist.user) | Q(user_trade=artist.user)).count()
        context['gigdates'] = GigDate.objects.filter(artist=artist, dates__gte=datetime.now()).values_list('dates', flat=True).order_by('dates')
        return context


class ClubDetailView(DetailView):
    """
    Вывод клуба на сайте
    """
    model = ClubProfile
    template_name = 'account/profiles/public/club.html'
    context_object_name = 'club'
    slug_field = 'uuid_id'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.visibility and not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('signin'))
        if request.user.is_authenticated() and not self.object.user == self.request.user:
            self.object.view += 1
            self.object.save()
        return super(ClubDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ClubDetailView, self).get_context_data(**kwargs)
        club = context.get('club')
        context['trade_count'] = Trades.objects.filter(Q(status_trade=Trades.STRADE2), Q(user_date=club.user) | Q(user_trade=club.user)).count()
        context['gigdates'] = GigTender.objects.filter(club=club, dates__gte=datetime.now()).values_list('dates', flat=True).order_by('dates')
        return context


class AgentDetailView(DetailView):
    """
    Вывод агента на сайте
    """
    model = AgentProfile
    template_name = 'account/profiles/public/agent.html'
    context_object_name = 'agent'
    slug_field = 'uuid_id'

    def get_context_data(self, **kwargs):
        context = super(AgentDetailView, self).get_context_data(**kwargs)
        agent = context.get('agent')
        context['artists'] = ArtistProfile.objects.filter(user=agent.user)
        context['clubs'] = ClubProfile.objects.filter(user=agent.user)
        context['trade_count'] = Trades.objects.filter(Q(status_trade=Trades.STRADE2), Q(user_date=agent.user) | Q(user_trade=agent.user)).count()
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.visibility and not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('signin'))
        if request.user.is_authenticated() and not self.object.user == self.request.user:
            self.object.view += 1
            self.object.save()
        return super(AgentDetailView, self).get(request, *args, **kwargs)


class MailBoxListView(ListView):
    """
    Список дат в ЛК
    """
    template_name = 'account/mailbox/mailbox_list.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(MailBoxListView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'mailbox'
        context['mailbox'] = True
        return context

    def get_queryset(self):
        return Mailbox.objects.filter(to=self.request.user).order_by('-date')


class MailBoxOutListView(ListView):
    """
    Список дат в ЛК
    """
    template_name = 'account/mailbox/mailbox_out_list.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(MailBoxOutListView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'mailbox'
        return context

    def get_queryset(self):
        return Mailbox.objects.filter(sender=self.request.user).order_by('-date')


# class MailboxDetailView(DetailView):
#     """
#     """
#     model = Mailbox
#     template_name = 'account/mailbox/mailbox_detail.html'
#
#     def get_object(self):
#         objects = super(MailboxDetailView, self).get_object()
#         if objects.to == self.request.user and not objects.read_to:
#             objects.read_to = True
#             objects.save()
#         return objects
#
#     def get(self, request, *args, **kwargs):
#         objects = self.get_object()
#         if (objects.sender != request.user) and (objects.to != request.user):
#             raise Http404
#         return super(MailboxDetailView, self).get(request, *args, **kwargs)
#
#     def get_context_data(self, **kwargs):
#         context = super(MailboxDetailView, self).get_context_data(**kwargs)
#         # context['form'] = MailboxFromForm()
#         context['submenu_url'] = 'mailbox'
#         context['mailbox'] = True if self.get_object().to == self.request.user else False
#         return context


class MailboxDetailView(FormView):
    """
    """
    form_class = MailboxFromForm
    template_name = 'account/mailbox/mailbox_detail.html'

    def get_mailbox(self):
        mailbox = get_object_or_404(Mailbox, pk=int(self.kwargs.get('pk')))
        if (mailbox.sender != self.request.user) and (mailbox.to != self.request.user):
            raise Http404
        if mailbox.to == self.request.user and not mailbox.read_to:
            mailbox.read_to = True
            mailbox.save()
        return mailbox

    def get_context_data(self, **kwargs):
        context = super(MailboxDetailView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'mailbox'
        mailbox = self.get_mailbox()
        context['object'] = mailbox
        context['mailbox'] = True if mailbox.to == self.request.user else False
        return context

    def get_success_url(self):
        return reverse('account:mailbox_view', kwargs={'pk': int(self.kwargs.get('pk'))})

    def form_valid(self, form):
        f = form.save(commit=False)
        mailbox = self.get_mailbox()
        f.title = u"Re: %s" % mailbox.title
        f.parent = mailbox.parent if mailbox.parent else mailbox
        f.sender = mailbox.to
        f.to = mailbox.sender
        f.save()
        return super(MailboxDetailView, self).form_valid(form)


class SendMail(TemplateView):
    template_name = 'account/send_mails.html'

    def get_period(self):
        from datetime import timedelta, date, datetime
        return {
            1: datetime.now() - timedelta(days=1),
            2: datetime.now() - timedelta(days=30)
        }

    def last_dates(self, period):
        from gigdate.models import DateAll
        return DateAll.objects.filter(create__gte=period)[:10]

    def last_trades(self, user, period):
        return Trades.objects.filter(Q(update__gte=period), Q(user_date=user) | Q(user_trade=user))[:10]

    def last_messges(self, user, period):
        return Mailbox.objects.filter(date__gte=period, to=user)[:10]

    def get_users(self):
        user = self.request.user
        period = self.get_period()

        if user.mail_dates:
            user.dates = self.last_dates(period.get(user.mail_time))
        if user.mail_trade:
            user.trades = self.last_trades(user, period.get(user.mail_time))
        if user.mail_messages:
            user.messages = self.last_messges(user, period.get(user.mail_time))

        return user

    def get_context_data(self, **kwargs):
        context = super(SendMail, self).get_context_data(**kwargs)
        context['user'] = self.get_users()
        return context


class ArtistRedirectView(RedirectView):
    permanent = False
    success_url = reverse_lazy('users:main')

    def get(self, request, *args, **kwargs):
        artist = get_object_or_404(ArtistProfile, pk=kwargs.get('pk'))
        return HttpResponseRedirect(reverse('users:artist', kwargs={'slug': artist.uuid_id}))


class VenueRedirectView(RedirectView):
    permanent = False
    success_url = reverse_lazy('users:main')

    def get(self, request, *args, **kwargs):
        venue = get_object_or_404(ClubProfile, pk=kwargs.get('pk'))
        return HttpResponseRedirect(reverse('users:club', kwargs={'slug': venue.uuid_id}))


class AgentRedirectView(RedirectView):
    permanent = False
    success_url = reverse_lazy('users:main')

    def get(self, request, *args, **kwargs):
        agent = get_object_or_404(AgentProfile, pk=kwargs.get('pk'))
        return HttpResponseRedirect(reverse('users:agent', kwargs={'slug': agent.uuid_id}))