#-*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.utils.translation import ugettext_lazy as _

from account.models import User, Registration, AgentProfile, ArtistProfile, ArtistImages, RiderAll, ArtistSite,\
    ClubProfile, Mailbox, SocialAuth
from account.forms import UserCreationForm, UserChangeForm, AuthLoginForm


class UserAdmin(AuthUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'country', 'city', 'birthday', 'phone', 'skype',
                                      'first_in')}),
        ('Payment status', {'fields': ('base_coin', 'paid_coin')}),
        ('Email spam', {'fields': ('mail_time', 'mail_messages', 'mail_trade', 'mail_dates')}),
        ('Permissions', {'fields': ('banned', 'is_active', 'is_staff', 'is_superuser', 'is_moderator', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {'fields': ('email', 'password1', 'password2')}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')}),
    )
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('email', 'get_full_name', 'country', 'banned', 'is_active', 'gigcoin_sum', 'diff_first_in', 'date_joined', 'last_login')
    list_editable = ('is_active', 'banned')
    list_filter = ('is_staff', 'is_moderator', 'is_superuser', 'is_active', 'banned', 'date_joined')
    search_fields = ('id', 'email', 'first_name', 'last_name')
    ordering = ('last_name', 'first_name',)

    def diff_first_in(self, obj):
        if obj.first_in is True:
            return False
        else:
            return True
    diff_first_in.boolean = True
    diff_first_in.short_description = _('First in')


class RegistrationAdmin(admin.ModelAdmin):
    list_display = ('user', 'uuid', 'create')
    search_fields = ('user__email',)


class ArtistSiteAllInline(admin.TabularInline):
    model = ArtistSite
    extra = 1


class RiderAllInline(admin.TabularInline):
    model = RiderAll
    extra = 1


class ArtistImagesInline(admin.TabularInline):
    model = ArtistImages
    extra = 1


class ArtistProfileAdmin(admin.ModelAdmin):
    inlines = [ArtistSiteAllInline, RiderAllInline, ArtistImagesInline]
    exclude = ('rider', 'site')

admin.site.register(User, UserAdmin)
admin.site.register(Registration, RegistrationAdmin)
admin.site.login_form = AuthLoginForm
admin.autodiscover()
admin.site.register(AgentProfile)
admin.site.register(ArtistProfile, ArtistProfileAdmin)
admin.site.register(ClubProfile)
admin.site.register(Mailbox)
admin.site.register(SocialAuth)