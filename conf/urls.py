#-*- coding:utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from account.views import LogoutView, SigninFormView, ActivationView, IsActiveView, RePasswordFormView
from gigdate.views import Index, DateListView, LastListView
from seo.views import YandexMailVerifed, Robotstxt
from contents.views import LandingPage, LandingPage2

urlpatterns = [

    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<type_date>\w+)/$', Index.as_view(), name='home_month'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/$', Index.as_view(), name='home_month_dates'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<type_date>\w+)/$', DateListView.as_view(), name='date_day'),
    url(r'^last/$', LastListView.as_view(), name='last_all'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^account/', include('account.urls', namespace='account')),
    url(r'^user/', include('account.pub_urls', namespace='users')),
    url(r'^d/', include('gigdate.urls', namespace='gigdate')),
    url(r'^moder/', include('moderator.urls', namespace='moderator')),
    url(r'^info/', include('contents.urls', namespace='contents')),
    url(r'^crm/', include('crm.urls', namespace='crm')),

    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^signin/$', SigninFormView.as_view(), name='signin'),
    url(r'^is_active/$', IsActiveView.as_view(), name='is_active'),
    url(r'^is_active/resend/$', IsActiveView.as_view(), {'resend': 're_activate'}, name='re_is_active'),
    url(r'^user_activation/', ActivationView.as_view(), name='activation'),
    url(r'^repassword/', RePasswordFormView.as_view(), name='repassword'),
    url(r'^start/', LandingPage.as_view(), name='landingpage'),
    url(r'^start2/', LandingPage2.as_view(), name='landingpage2'),

    url(r'^cf6a2b36ecd97bf07de3f80906e7f2e9ab6cc852917cf7e7bd639612b615520c.html$', YandexMailVerifed.as_view()),
    url(r'^robots.txt', Robotstxt.as_view()),

    url(r'^$', Index.as_view(), name='home'),

    url(r'^(?P<type_date>\w+)/$', Index.as_view(), name='home_two'),
]


# Инициализируем папку media
if settings.DEBUG:
    if settings.MEDIA_ROOT:
        from django.conf.urls.static import static
        from django.contrib.staticfiles.urls import staticfiles_urlpatterns
        urlpatterns += static(settings.MEDIA_URL,
            document_root=settings.MEDIA_ROOT)
    # Эта строка опциональна и будет добавлять url'ы только при DEBUG = True
    urlpatterns += staticfiles_urlpatterns()