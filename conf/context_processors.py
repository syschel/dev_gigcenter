# -*- coding: utf-8 -*-
from django.contrib import messages
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
import re


class ValidProfile(object):
    """

    """
    @staticmethod
    def process_request(request):
        """ Проверка заполнености профиля """

        from django.core.urlresolvers import reverse
        active_url = [reverse("is_active"), reverse("re_is_active"), reverse("activation"), reverse("repassword"),
                      reverse('logout'), reverse('signin')]
        ya_verif = ''
        content_re = re.compile(r"^/(info|user|d|social|media|static|admin|logout|login_fb|start|start2)/+")
        ya_verif = re.compile(r"^/cf6a2b36ecd97bf07de3f80906e7f2e9ab6cc852917cf7e7bd639612b615520c\.html+")

        get_full_path = request.path

        if content_re.match(get_full_path) or ya_verif.match(get_full_path):
            return None
        elif request.user.is_authenticated():

            first_in = [reverse("account:main"), reverse("repassword")]
            compiled_re = re.compile(r"^/account/ajax_city/([0-9]+)/$")

            if get_full_path not in active_url and not request.user.is_active:
                return HttpResponseRedirect(reverse("is_active"))
            elif request.user.is_active and request.user.first_in\
                    and get_full_path not in first_in\
                    and not compiled_re.match(get_full_path):
                messages.info(request, _(' Please fill in Basic profile.'))
                return HttpResponseRedirect(reverse('account:main'))
        elif not request.user.is_authenticated() and get_full_path not in active_url:
            messages.info(request, _('Please login.'))
            return HttpResponseRedirect(reverse('landingpage'))
        return None


def mailbox_context(request):
    """
    Пишем в глобальную среду количество непрочитанных писем
    """
    from account.models import Mailbox
    return {
        'mailbox_count': Mailbox.objects.filter(to=request.user, read_to=False).count() if request.user.is_authenticated() else False,
        'gigcoin_count': request.user.gigcoin_sum() if request.user.is_authenticated() else 0,
        'settings': settings
    }


def site(request):
    from seo.models import BaseSettings
    site = BaseSettings.objects.filter(template_seo=0)
    return {
        'site': site[0] if site else None
    }