# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.forms import MultipleChoiceField, ModelMultipleChoiceField
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db.models import ImageField
from django.utils.deconstruct import deconstructible
import re
from io import BytesIO


class MultipleField(MultipleChoiceField):
    """
    Поле со множественными значениями(например список строк)
    """
    def __init__(self, *args, **kwargs):
        super(MultipleField, self).__init__(*args, **kwargs)
        self.choices = ()

    def validate(self, value):
        if self.required and not value:
            raise ValidationError(self.error_messages['required'])


nickname_re = re.compile(r'^([a-zA-Z0-9]+)[-a-zA-Z0-9_ ]+([a-zA-Z0-9]+)$')
validate_nickname = RegexValidator(nickname_re, _(u"Please use English language"), code='invalid')


class M2mSiteField(ModelMultipleChoiceField):
    """
    Множественный выбор поля для сайтов
    """

    def clean(self, value):
        if self.required and not value:
            raise ValidationError(self.error_messages['required'], code='required')
        elif not self.required and not value:
            return self.queryset.none()
        if not isinstance(value, (list, tuple)):
            raise ValidationError(self.error_messages['list'], code='list')
        qs = self._check_values(value)
        # Since this overrides the inherited ModelChoiceField.clean
        # we run custom validators here
        self.run_validators(value)
        return qs


class ValidateImageField(ImageField):
    """
    Validations images format
    """
    VALID_IMAGE_EXTENSIONS = ['JPG', 'JPEG', 'PNG', 'GIF']
    message = _("Image invalid. Please upload format %s" % ', '.join(VALID_IMAGE_EXTENSIONS))
    code = 'images_invalid'

    def to_python(self, data):
        f = super(ValidateImageField, self).to_python(data)
        if f is None:
            return None
        name = u"%s" % data.name.split('.')[-1]
        if data and name.upper() not in self.VALID_IMAGE_EXTENSIONS:
            raise ValidationError(message=self.message, code=self.code)
        return f