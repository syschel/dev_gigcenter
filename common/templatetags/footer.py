# -*- coding:utf-8 -*-
from django import template
from common.models import FooterMenu


def footer_menu():
    menu = FooterMenu.objects.all()  #.select_related('subject')
    return {'menu': menu}

register = template.Library()
register.inclusion_tag('common/footer_menu.html')(footer_menu)