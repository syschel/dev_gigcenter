# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0005_agenttype'),
    ]

    operations = [
        migrations.CreateModel(
            name='FooterMenu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField(default=0, unique=True, verbose_name='level', choices=[(0, 'left 1'), (1, 'left 2'), (2, 'left 3'), (3, 'left 4')])),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'verbose_name': 'Footer Menu',
                'verbose_name_plural': 'Footer Menu',
            },
        ),
        migrations.CreateModel(
            name='FooterMenuLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('url', models.CharField(max_length=255, verbose_name='Link')),
                ('sort', models.IntegerField(default=0, verbose_name='sorting', blank=True)),
                ('subject', models.ForeignKey(verbose_name='level', to='common.FooterMenu')),
            ],
            options={
                'ordering': ('sort',),
            },
        ),
    ]
