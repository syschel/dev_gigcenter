# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0002_paymentmethod'),
    ]

    operations = [
        migrations.CreateModel(
            name='MethodsAdvert',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'Methods Advert',
                'verbose_name_plural': 'Methods Advert',
            },
        ),
    ]
