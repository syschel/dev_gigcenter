# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0006_footermenu_footermenulink'),
    ]

    operations = [
        migrations.AlterField(
            model_name='footermenu',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='footermenulink',
            name='subject',
            field=models.ForeignKey(related_name='footer_menu', verbose_name='level', to='common.FooterMenu'),
        ),
    ]
