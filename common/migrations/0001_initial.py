# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='Title')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'City',
                'verbose_name_plural': 'City',
            },
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='Title')),
                ('width', models.CharField(default=0, max_length=3, verbose_name='img width')),
                ('height', models.CharField(default=0, max_length=3, verbose_name='img height')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'Country',
                'verbose_name_plural': 'Country',
            },
        ),
        migrations.CreateModel(
            name='FormatPerformance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'Format Performance',
                'verbose_name_plural': 'Format Performance',
            },
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'Genre',
                'verbose_name_plural': 'Genres',
            },
        ),
        migrations.CreateModel(
            name='TypeClub',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
            ],
            options={
                'ordering': ('title',),
                'verbose_name': 'Type of Club',
                'verbose_name_plural': 'Type of Club',
            },
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(related_name='country_city', verbose_name='Country', to='common.Country'),
        ),
    ]