# -*- coding:utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Count
from common.models import City, Country


class Command(BaseCommand):
    help = 'Delete doubles citys'

    def handle(self, *args, **options):
        citys = City.objects.values('title', 'country_id').annotate(countsss=Count('country_id')).filter(countsss__gt=1).order_by('-countsss')
        all_city = citys.count()
        print u"All double:", all_city

        a = 0
        for city in citys:
            for dsite in City.objects.filter(title=city.get('title'), country_id=city.get('country_id'))[1:]:
                dsite.delete()
            a += 1
            print u"-- Delete(%s):" % all_city, city.get('title'),  city.get('countsss')
            all_city -= 1
        print u"Delete city: ", a

        b = 0
        for country in Country.objects.all():
            if not country.country_city.count():
                country.delete()
                b += 1
        print u"Delete country: ", b