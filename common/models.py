#-*- coding:utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Country(models.Model):
    """
    Список стран и их флаги
    """
    title = models.CharField(_(u'Title'), max_length=100)
    width = models.CharField(_(u'img width'), max_length=3, default=0)
    height = models.CharField(_(u'img height'), max_length=3, default=0)

    def city_count(self):
        return self.country_city.count()

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'Country')
        verbose_name_plural = _(u'Country')


class City(models.Model):
    """
    Список городов у страны
    """
    title = models.CharField(_(u'Title'), max_length=100)
    country = models.ForeignKey(Country, verbose_name=_(u'Country'), related_name='country_city')

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'City')
        verbose_name_plural = _(u'City')


class Genre(models.Model):
    """ Список жанров """
    title = models.CharField(_(u'Title'), max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'Genre')
        verbose_name_plural = _(u'Genres')


class FormatPerformance(models.Model):
    """ Формат выступления """
    title = models.CharField(_(u'Title'), max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'Format Performance')
        verbose_name_plural = _(u'Format Performance')


class TypeClub(models.Model):
    """ Тип площадки """
    title = models.CharField(_(u'Title'), max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'Type of Club')
        verbose_name_plural = _(u'Type of Club')


class PaymentMethod(models.Model):
    """ Форма оплаты гонорара """
    title = models.CharField(_(u'Title'), max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'Payment Method')
        verbose_name_plural = _(u'Payment Methods')


class MethodsAdvert(models.Model):
    """ Методы рекламы """
    title = models.CharField(_(u'Title'), max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'Methods Advert')
        verbose_name_plural = _(u'Methods Advert')


class FaqCategory(models.Model):
    """ Категории помощи """
    title = models.CharField(_(u'Title'), max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'FAQ category')
        verbose_name_plural = _(u'FAQ categiry')


class AgentType(models.Model):
    """ Категории помощи """
    title = models.CharField(_(u'Title'), max_length=255)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('title',)
        verbose_name = _(u'Agent type')
        verbose_name_plural = _(u'Agent type')


class FooterMenu(models.Model):
    """ Профиль домена """

    LEVEL0, LEVEL1, LEVEL2, LEVEL3 = range(0, 4)
    LEVELS = (
        (LEVEL0, _(u'left 1')),
        (LEVEL1, _(u'left 2')),
        (LEVEL2, _(u'left 3')),
        (LEVEL3, _(u'left 4')),

    )

    level = models.IntegerField(_(u'level'), default=LEVEL0, choices=LEVELS, unique=True)
    name = models.CharField(_(u'Name'), max_length=255)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        verbose_name = _(u'Footer Menu')
        verbose_name_plural = _(u'Footer Menu')


class FooterMenuLink(models.Model):
    subject = models.ForeignKey(FooterMenu, verbose_name=_(u'level'), related_name='footer_menu')
    name = models.CharField(_(u'Name'), max_length=255)
    url = models.CharField(_(u'Link'), max_length=255)
    sort = models.IntegerField(_(u"sorting"), default=0, blank=True,)

    def __unicode__(self):
        return u"%s" % self.name

    class Meta:
        ordering = ('sort',)