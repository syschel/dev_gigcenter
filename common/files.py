# -*- coding: utf-8 -*-
import os
import uuid
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.template.loader import get_template
from django.template import Context
import cStringIO as StringIO
import xhtml2pdf.pisa as pisa

import hashlib


def get_file_path(instance, filename):
    """
    Изменяем имя файла и уникализируем путь до него
    :param instance: Объект из которого была запущена функция
    :param filename: Текущее имя файла. Нужно только для расширения
    :return: Имя файла с путём до директории
    """
    # Переименовываем файл
    ext = filename.split('.')[-1]
    # Генерируем рандомный md5(md5+md5)
    md5 = hashlib.md5(uuid.uuid4().get_hex().encode() + uuid.uuid4().get_hex().encode()).hexdigest()

    filename = u"%s.%s" % (md5[:-4], ext)
    # Создаём подпапки из начала имени файла
    path = u'/%s/%s/' % (md5[:2], md5[2:4])

    # Определяем от какого класса у нас загрузка файла
    name = instance.__class__.__name__

    return os.path.join("upload/md/" + name + path, filename)


def render_to_pdf(template_src, context_dict, filename='contract.pdf'):
    """ Отдаю PDF файл """
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode('utf-8')), result, encoding='UTF-8', show_error_as_pdf=True)
    if not pdf.err:
        response = HttpResponse(result.getvalue(), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response
    return HttpResponse(_(u'We had some errors!'))


def promo_code():
    return uuid.uuid4()
