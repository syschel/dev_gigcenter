#-*- coding:utf-8 -*-
from django.contrib import admin
from common.models import Country, City, Genre, FormatPerformance, TypeClub, PaymentMethod, MethodsAdvert, FaqCategory,\
    AgentType, FooterMenu, FooterMenuLink


class CountryAdmin(admin.ModelAdmin):
    list_display = ('pk', 'title', 'city_count', 'width', 'height', )
    list_display_links = ('title', )
    list_editable = ('width', 'height',)
    # list_filter = ('city_count',)
    search_fields = ['title']
    ordering = ('title',)


class FooterMenuLinkInline(admin.TabularInline):
    model = FooterMenuLink
    extra = 1


class FooterMenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'level',)
    list_display_links = ('name', 'level',)
    inlines = [FooterMenuLinkInline]

admin.site.register(Country, CountryAdmin)
admin.site.register(City)
admin.site.register(Genre)
admin.site.register(FormatPerformance)
admin.site.register(TypeClub)
admin.site.register(PaymentMethod)
admin.site.register(MethodsAdvert)
admin.site.register(AgentType)
admin.site.register(FaqCategory)
admin.site.register(FooterMenu, FooterMenuAdmin)
