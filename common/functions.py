# -*- coding: utf-8 -*-
from account.models import Mailbox


def send_mailbox(user, title, text):
    """
    Отправка сообщения пользователю
    """
    mails = Mailbox()
    mails.to = user
    mails.title = title
    mails.text = text
    mails.save()
    return True