#-*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class BaseSettings(models.Model):
    """ Профиль домена """
    DEFAULT = ((0, u'base'),)
    template_seo    = models.IntegerField(verbose_name=u'unique', default=0, choices=DEFAULT, unique=True)

    meta            = models.TextField(verbose_name=u'Настройки meta-тегов', null=True, blank=True)
    robots          = models.TextField(verbose_name=u'Настройки robots.txt', null=True, blank=True)
    footer          = models.TextField(verbose_name=u'Вывод кода перед </body>', null=True, blank=True)

    name            = models.CharField(verbose_name=u'name', max_length=255, blank=True, null=True)
    title           = models.CharField(verbose_name=u'title', max_length=255, blank=True, null=True)
    keywords        = models.CharField(verbose_name=u'keywords', max_length=255, blank=True, null=True)
    description     = models.CharField(verbose_name=u'description', max_length=255, blank=True, null=True)
    about           = models.TextField(verbose_name=u'Описание главной', null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.template_seo

    class Meta:
        verbose_name = u'Base Settings'
        verbose_name_plural = u'Base Settings'