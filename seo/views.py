#-*- coding:utf-8 -*-
from django.shortcuts import render
from django.views.generic.base import RedirectView, TemplateView
from seo.models import BaseSettings


class YandexMailVerifed(TemplateView):
    template_name = 'seo/yandex_mail_verifed.html'
    content_type = 'text/plain; charset=utf-8'


class Robotstxt(TemplateView):
    content_type = 'text/plain; charset=utf-8'
    template_name = 'seo/robots.txt'

    def get_context_data(self, **kwargs):
        context = super(Robotstxt, self).get_context_data(**kwargs)
        robots = BaseSettings.objects.filter(template_seo=0).values('robots')
        context['content'] = robots[0] if robots else None
        return context