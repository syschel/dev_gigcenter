#-*- coding:utf-8 -*-
from django.contrib import admin
from django.db import models
from django.forms import TextInput
from seo.models import BaseSettings


class ModelAdmin(admin.ModelAdmin):
    formfield_overrides = {models.CharField: {'widget': TextInput(attrs={'size': '75'})}}


class BaseSettingsAdmin(ModelAdmin):
    list_display = ('template_seo',)
    list_display_links = ('template_seo',)
    fieldsets = (
        (None, {'fields': ('template_seo',)}),
        (u'Информация', {'fields': ('meta', 'footer', 'robots',)}),
        (u'SEO', {'fields': ('name', 'about', 'title', 'keywords', 'description',)}),
    )

admin.site.register(BaseSettings, BaseSettingsAdmin)