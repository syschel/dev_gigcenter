#-*- coding:utf-8 -*-
import random
from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db import models


class TransactionManager(models.Manager):

    def _transaction(self, user, coin, status, movement, messages, **extra_fields):
        trans = self.model(user=user, coin=coin, status=status, movement=movement,
                           messages=messages, **extra_fields)
        trans.save(using=self._db)
        return trans

    def transaction_debit(self, user=None, coin=0, movement=True, messages=None, **extra_fields):
        return self._transaction(user, coin, 0, movement, messages, **extra_fields)

    def transaction_credit(self, user=None, coin=0, status=3, movement=True, messages=None, **extra_fields):
        return self._transaction(user, coin, status, movement, messages, **extra_fields)


class TransactionCoin(models.Model):
    STAT0, STAT1, STAT2, STAT3 = range(0, 4)
    STATUS = (
        (STAT0, _(u'Charged')),
        (STAT1, _(u'Promo code')),
        (STAT2, _(u'Registration')),
        (STAT3, _(u'Bonus')),
    )
    create = models.DateTimeField(_(u"Created"), auto_now_add=True, blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_(u'User'), blank=True)
    coin = models.PositiveIntegerField(_(u"Coin"), default=0, blank=True, null=True)
    status = models.IntegerField(_(u'Status'), help_text=_(u"Debited or credited coins"),
                                 default=STAT0, choices=STATUS)
    movement = models.BooleanField(_(u'Movement'), default=True, help_text=_(u"The type of movement of the coins"),
                                   choices=((True, _(u'Free'),), (False, _(u'Paid'),)))
    messages = models.CharField(_(u'Messages'), max_length=500, null=True, blank=True)

    objects = TransactionManager()

    def __unicode__(self):
        return u"%s" % self.create

    class Meta:
        ordering = ['-pk']
        verbose_name = _(u"Transaction")
        verbose_name_plural = _(u"Transactions")


def generate_codes(prefix="", segmented=settings.SEGMENTED_CODES):
    code = "".join(random.choice(settings.CODE_CHARS) for i in range(settings.CODE_LENGTH))
    if segmented:
        code = settings.SEGMENT_SEPARATOR.join([code[i:i + settings.SEGMENT_LENGTH] for i in range(0, len(code), settings.SEGMENT_LENGTH)])
        return prefix + code
    else:
        return prefix + code


class ActionCoin(models.Model):
    """ ddf
    """
    create = models.DateTimeField(_(u"Created"), auto_now_add=True, blank=True, null=True)
    name = models.CharField(_(u'Name'), max_length=250, null=True, blank=False)
    promo_code = models.CharField(_(u'Promo code'), max_length=30, null=True, blank=False, unique=True, default=generate_codes)
    coin = models.PositiveIntegerField(_(u"Coin"), default=0, blank=False, null=True)
    valid_until = models.DateField(_(u"Valid until"), blank=True, null=True,
                                   help_text=_(u"Leave empty for coupons that never expire"))
    multi = models.BooleanField(_(u"Multiple usage"), default=False)
    active = models.BooleanField(_(u"Active"), default=True)
    user = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name=_(u'User'), blank=True, related_name=r"action_users",
                                  help_text=_(u"You may specify a user you want to restrict this coupon to."))

    @classmethod
    def generate_code(cls, prefix="", segmented=settings.SEGMENTED_CODES):
        return generate_codes(prefix=prefix, segmented=segmented)

    def save(self, *args, **kwargs):
        if not self.promo_code:
            self.promo_code = ActionCoin.generate_code()
        super(ActionCoin, self).save(*args, **kwargs)

    def expired(self):
        return self.valid_until is not None and self.valid_until < timezone.now()

    def __unicode__(self):
        return self.promo_code

    class Meta:
        ordering = ['-create']
        verbose_name = _(u"Coupon")
        verbose_name_plural = _(u"Coupons")