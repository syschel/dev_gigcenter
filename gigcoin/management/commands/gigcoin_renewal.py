# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.management.base import BaseCommand
from datetime import date
from django.conf import settings
from calendar import monthrange

from account.models import User
from gigcoin.models import TransactionCoin


class Command(BaseCommand):
    help = 'Update base coin in profile users'

    def handle(self, *args, **options):
        print _(u"Subscriptions: update base_coins all users, count:"), self.get_users()

    @staticmethod
    def get_users():
        today = date.today()
        last_day = monthrange(today.year, today.month)[1]
        # 27, 28, ... 29, 30, 31
        if today.day == last_day and today.day < 31:
            params = {'date_joined__day__in': range(last_day, 32)}
        else:
            params = {'date_joined__day': today.day}

        users = User.objects.filter(**params).only('date_joined', 'pk', 'email', 'base_coin')
        count = 0
        for user in users:
            if date(user.date_joined.year, user.date_joined.month, user.date_joined.day) == today:
                continue
            TransactionCoin.objects.transaction_credit(user=user, coin=user.base_coin, status=TransactionCoin.STAT0,
                                                       messages=_(u"Remaining balance charged to credit monthly bonus"))
            user.base_coin = settings.BASE_GIGCOIN
            user.save(update_fields=['base_coin'])
            TransactionCoin.objects.transaction_credit(user=user, coin=settings.BASE_GIGCOIN,
                                                       status=TransactionCoin.STAT3,
                                                       messages=_(u"Monthly GigCoin balance"))
            count += 1
        return count