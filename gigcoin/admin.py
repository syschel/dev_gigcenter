# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from gigcoin.models import TransactionCoin, ActionCoin


class TransactionCoinAdmin(admin.ModelAdmin):
    list_display = ['user', 'coin', 'status', 'movement', 'create']
    list_filter = ['status', 'movement', 'create']


class CouponAdmin(admin.ModelAdmin):
    list_display = ['name', 'promo_code', 'coin', 'num_users', 'valid_until', 'multi', 'active', 'create']
    list_filter = ['active', 'multi',  'create', 'valid_until']
    raw_id_fields = ('user',)
    search_fields = ('user__username', 'user__email', 'promo_code')

    def num_users(self, obj):
        return obj.user.count()
    num_users.short_description = _("users")

admin.site.register(TransactionCoin, TransactionCoinAdmin)
admin.site.register(ActionCoin, CouponAdmin)