# -*- coding: utf-8 -*-
from django.apps import AppConfig


class GigcoinConfig(AppConfig):
    name = 'gigcoin'
