# -*- coding: utf-8 -*-
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from gigcoin.models import TransactionCoin
from gigcoin.forms import PromoCodeForm


class TransactionCoinList(ListView):
    template_name = 'gigcoin/account/billing_list.html'
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super(TransactionCoinList, self).get_context_data(**kwargs)
        context['submenu_url'] = 'billing'
        context['transaction_url'] = True
        return context

    def get_queryset(self):
        return TransactionCoin.objects.filter(user=self.request.user)


class PromoCodeFormView(FormView):
    form_class = PromoCodeForm
    template_name = 'gigcoin/account/promo_code.html'
    success_url = reverse_lazy('account:billing')

    def get_initial(self):
        initial = super(PromoCodeFormView, self).get_initial()
        initial['user'] = self.request.user
        return initial

    def form_valid(self, form):
        coin = form.cleaned_data.get('coin', 0)
        name = form.cleaned_data.get('name', 0)
        messages_transaction = _(u"GigCoins received for promo code \"%s\"" % name)
        TransactionCoin.objects.create(user=self.request.user, coin=coin, status=1, movement=False, messages=messages_transaction)
        self.request.user.paid_coin = self.request.user.paid_coin + coin
        self.request.user.save(update_fields=['paid_coin'])
        messages.info(self.request, _(u'Promo code successfully activated'))
        return super(PromoCodeFormView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PromoCodeFormView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'billing'
        context['promocode_url'] = True
        return context


class PaidView(TemplateView):
    template_name = 'gigcoin/account/paid_coin.html'

    def get_context_data(self, **kwargs):
        context = super(PaidView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'billing'
        context['paid_url'] = True
        return context