# -*- coding: utf-8 -*-
from django import forms
from datetime import date
from django.utils.translation import ugettext_lazy as _
from gigcoin.models import ActionCoin


class PromoCodeForm(forms.Form):
    promo_code = forms.CharField(label='Promo code', required=True)

    def __init__(self, *args, **kwargs):
        super(PromoCodeForm, self).__init__(*args, **kwargs)
        self.user = kwargs.get('initial').get('user', None)

    def clean(self):
        promo_code = self.cleaned_data.get('promo_code', '')

        actions = ActionCoin.objects.filter(promo_code=promo_code)
        if actions:
            action = actions[0]
            if not action.active:
                raise forms.ValidationError({'promo_code': _(u'Promo code has expired')}, code='not_active')
            elif action.valid_until and action.valid_until < date.today():
                raise forms.ValidationError({'promo_code': _(u'Promo code has expired')}, code='valid_until')
            elif not action.multi and action.user.count() >= 1:
                raise forms.ValidationError({'promo_code': _(u'Promo code has already been used')}, code='multi')
            elif action.multi and action.user.filter(pk=self.user.pk):
                raise forms.ValidationError({'promo_code': _(u'You have already used this promo code')}, code='my_used')
            else:
                action.user.add(self.user)
                self.cleaned_data['coin'] = action.coin
                self.cleaned_data['name'] = action.name
        else:
            raise forms.ValidationError({'promo_code': _(u'Promo code not found')}, code='not_found')

        return self.cleaned_data