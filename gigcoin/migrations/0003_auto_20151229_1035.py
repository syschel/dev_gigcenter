# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-29 10:35
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gigcoin', '0002_auto_20151229_0917'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actioncoin',
            name='user',
            field=models.ManyToManyField(blank=True, help_text='You may specify a user you want to restrict this coupon to.', related_name='action_users', to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
    ]
