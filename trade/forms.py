# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from trade.models import Trades, Replys, Chat
from account.models import ArtistProfile, ClubProfile


class TradesForm(forms.ModelForm):
    object_data = None
    object_user = None
    regulations_accept = forms.BooleanField(initial=True, required=True)
    mony_edit = forms.BooleanField(required=False, widget=forms.HiddenInput())
    fee_event_type = forms.ChoiceField(choices=Trades.EVENT_TYPE, required=False, initial=Trades.EVENT0, widget=forms.HiddenInput())
    queries_view = False

    def __init__(self, *args, **kwargs):
        super(TradesForm, self).__init__(*args, **kwargs)

        self.initial = kwargs.get('initial')
        self.object_user = self.initial.get('user', None)
        self.object_data = self.initial.get('date_all', None)

        self.fields['club_trade'] = forms.ModelChoiceField(queryset=ClubProfile.objects.filter(user=self.object_user), empty_label=None)

        if self.object_data:
            # Формируем выпадайку выбора договора
            contracts = self.object_data.contract_artist + self.object_data.contract_site
            if contracts > 1:
                self.fields['contracts'].required = True
            CHOE = ()
            if self.object_data.contract_artist:
                CHOE += Trades.CON0
            if self.object_data.contract_site:
                CHOE += Trades.CON2
            self.fields['contracts'] = forms.ChoiceField(choices=CHOE)

            # Формируем выпадайку типов мероприятия
            choess = [('', '----------')]
            class_price = []
            id_price = []
            if not self.object_data.event_public == None:
                if not self.object_data.event_public == 1:
                    class_price.append('event_public')
                    id_price.append('0')
                choess.append((Trades.EVENT_TYPE[0][0], Trades.EVENT_TYPE[0][1]))
            if not self.object_data.event_private == None:
                if not self.object_data.event_private == 1:
                    class_price.append('event_private')
                    id_price.append('1')
                choess.append((Trades.EVENT_TYPE[1][0], Trades.EVENT_TYPE[1][1]))
            if not self.object_data.event_presentation == None:
                if not self.object_data.event_presentation == 1:
                    class_price.append('event_presentation')
                    id_price.append('2')
                choess.append((Trades.EVENT_TYPE[2][0], Trades.EVENT_TYPE[2][1]))
            self.fields['event_type'].choices = choess
            self.fields['event_type'].required = True

            # Поле ввода цены
            if class_price:
                self.fields['mony_fix'].widget.attrs['class'] = " ".join(class_price)

            data = kwargs.get('data')
            if data and data.get('event_type') and data.get('event_type') in id_price:
                self.fields['mony_fix'].widget.attrs['visible_div'] = True
            self.id_price = id_price

            # Формируем отображение вопросов
            if not self.object_data.queries1:
                del self.fields['queries1']
            if not self.object_data.queries2:
                del self.fields['queries2']
            if not self.object_data.queries3:
                del self.fields['queries3']
            if not self.object_data.queries4:
                del self.fields['queries4']
            if not self.object_data.queries5:
                del self.fields['queries5']
            if not self.object_data.queries6:
                del self.fields['queries6']
            if not self.object_data.queries7:
                del self.fields['queries7']
            if not self.object_data.queries8:
                del self.fields['queries8']
            if not self.object_data.queries9:
                del self.fields['queries9']
            if not self.object_data.queries10:
                del self.fields['queries10']
            if not self.object_data.queries11:
                del self.fields['queries11']
            if not self.object_data.queries12:
                del self.fields['queries12']
            if not self.object_data.queries13:
                del self.fields['queries13']
            else:
                self.fields['queries13'].widget.attrs['class'] = 'nostyle'
            if not self.object_data.queries14:
                del self.fields['queries14']
            if not self.object_data.queries15:
                del self.fields['queries15']
            if not self.object_data.queries16:
                del self.fields['queries16']
            if not self.object_data.queries17:
                del self.fields['queries17']
            if not self.object_data.queries18:
                del self.fields['queries18']
            if not self.object_data.queries19:
                del self.fields['queries19']
            if not self.object_data.queries20:
                del self.fields['queries20']
            if not self.object_data.queries21:
                del self.fields['queries21']

            for key in self.fields:
                if not key in ['mony_fix', 'mony_edit']:
                    self.fields[key].required = True
                    if 'queries' in key:
                        self.queries_view = True

    def clean(self):
        super(TradesForm, self).clean()
        event_type = self.cleaned_data.get('event_type')
        contracts = self.object_data.contract_artist + self.object_data.contract_site
        if contracts < 2:
            if self.object_data.contract_artist:
                self.cleaned_data['contracts'] = 0
            else:
                self.cleaned_data['contracts'] = 2
        if event_type >= 0 and '%s' % event_type in self.id_price:
            if not self.cleaned_data.get('mony_fix'):
                self._errors["mony_fix"] = self.error_class([_('This field is required')])
        else:
            if self._errors.get('mony_fix'):
                del self._errors['mony_fix']

        if not event_type == None:
            if event_type == 0:
                self.cleaned_data['fee_event_type'] = self.object_data.event_public
                if self.object_data.event_public == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_publ_fix
                    self.cleaned_data['mony_edit'] = True
            elif event_type == 1:
                self.cleaned_data['fee_event_type'] = self.object_data.event_private
                if self.object_data.event_private == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_priv_fix
                    self.cleaned_data['mony_edit'] = True
            elif event_type == 2:
                self.cleaned_data['fee_event_type'] = self.object_data.event_presentation
                if self.object_data.event_presentation == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_pres_fix
                    self.cleaned_data['mony_edit'] = True
            else:
                pass
        return self.cleaned_data

    class Meta:
        model = Replys
        fields = ['club_trade', 'contracts', 'event_type', 'fee_event_type', 'mony_fix', 'mony_edit',
                  'queries1', 'queries2', 'queries3', 'queries4', 'queries5', 'queries6', 'queries7', 'queries8',
                  'queries9', 'queries10', 'queries11', 'queries12', 'queries13', 'queries14', 'queries15',
                  'queries16', 'queries17', 'queries18', 'queries19', 'queries20', 'queries21']
        # widgets = {'mony_edit': forms.HiddenInput()}


class TradesTenderForm(forms.ModelForm):
    object_data = None
    object_user = None
    regulations_accept = forms.BooleanField(initial=True, required=True)
    mony_edit = forms.BooleanField(required=False, widget=forms.HiddenInput())
    fee_event_type = forms.ChoiceField(choices=Trades.EVENT_TYPE, required=False, initial=Trades.EVENT0, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(TradesTenderForm, self).__init__(*args, **kwargs)

        self.initial = kwargs.get('initial')
        self.object_user = self.initial.get('user', None)
        self.object_data = self.initial.get('date_all', None)

        self.fields['artist_trade'] = forms.ModelChoiceField(queryset=ArtistProfile.objects.filter(user=self.object_user), empty_label=None)

        if self.object_data:
            # Формируем выпадайку выбора договора
            contracts = self.object_data.contract_artist + self.object_data.contract_site #+ self.object_data.contract_club
            if contracts > 1:
                self.fields['contracts'].required = True
            CHOE = ()
            if self.object_data.contract_artist:
                CHOE += Trades.CON0
            # if self.object_data.contract_club:
            #     CHOE += Trades.CON1
            if self.object_data.contract_site:
                CHOE += Trades.CON2
            self.fields['contracts'] = forms.ChoiceField(choices=CHOE)

            # Формируем выпадайку типов мероприятия
            choess = [('', '----------')]
            class_price = []
            id_price = []
            if not self.object_data.event_public == None:
                if not self.object_data.event_public == 1:
                    class_price.append('event_public')
                    id_price.append('0')
                choess.append((Trades.EVENT_TYPE[0][0], Trades.EVENT_TYPE[0][1]))
            if not self.object_data.event_private == None:
                if not self.object_data.event_private == 1:
                    class_price.append('event_private')
                    id_price.append('1')
                choess.append((Trades.EVENT_TYPE[1][0], Trades.EVENT_TYPE[1][1]))
            if not self.object_data.event_presentation == None:
                if not self.object_data.event_presentation == 1:
                    class_price.append('event_presentation')
                    id_price.append('2')
                choess.append((Trades.EVENT_TYPE[2][0], Trades.EVENT_TYPE[2][1]))
            self.fields['event_type'].choices = choess
            self.fields['event_type'].required = True

            # Поле ввода цены
            if class_price:
                self.fields['mony_fix'].widget.attrs['class'] = " ".join(class_price)

            data = kwargs.get('data')
            if data and data.get('event_type') and data.get('event_type') in id_price:
                self.fields['mony_fix'].widget.attrs['visible_div'] = True
            self.id_price = id_price

    def clean(self):
        super(TradesTenderForm, self).clean()
        event_type = self.cleaned_data.get('event_type')
        contracts = self.object_data.contract_artist + self.object_data.contract_site  # + self.object_data.contract_club
        if contracts < 2:
            if self.object_data.contract_artist:
                self.cleaned_data['contracts'] = 0
            if self.object_data.contract_site:
                self.cleaned_data['contracts'] = 2
            # if self.object_data.contract_club:
            #     self.cleaned_data['contracts'] = 2
        if event_type >= 0 and '%s' % event_type in self.id_price:
            if not self.cleaned_data.get('mony_fix'):
                self._errors["mony_fix"] = self.error_class([_('This field is required')])
        else:
            if self._errors.get('mony_fix'):
                del self._errors['mony_fix']
        if not event_type == None:
            if event_type == 0:
                self.cleaned_data['fee_event_type'] = self.object_data.event_public
                if self.object_data.event_public == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_publ_fix
                    self.cleaned_data['mony_edit'] = True
            elif event_type == 1:
                self.cleaned_data['fee_event_type'] = self.object_data.event_private
                if self.object_data.event_private == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_priv_fix
                    self.cleaned_data['mony_edit'] = True
            elif event_type == 2:
                self.cleaned_data['fee_event_type'] = self.object_data.event_presentation
                if self.object_data.event_presentation == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_pres_fix
                    self.cleaned_data['mony_edit'] = True
            else:
                pass
        return self.cleaned_data

    class Meta:
        model = Trades
        fields = ['artist_trade', 'contracts', 'event_type', 'fee_event_type', 'mony_fix', 'mony_edit']


class TradesBarterForm(forms.ModelForm):
    object_data = None
    object_user = None
    regulations_accept = forms.BooleanField(initial=True, required=True)
    mony_edit = forms.BooleanField(required=False, widget=forms.HiddenInput())
    b_mony_edit = forms.BooleanField(required=False, widget=forms.HiddenInput())
    fee_event_type = forms.ChoiceField(choices=Trades.EVENT_TYPE, required=False, initial=Trades.EVENT0, widget=forms.HiddenInput())
    b_fee_event_type = forms.ChoiceField(choices=Trades.EVENT_TYPE, required=False, initial=Trades.EVENT0, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(TradesBarterForm, self).__init__(*args, **kwargs)

        self.initial = kwargs.get('initial')
        self.object_user = self.initial.get('user', None)
        self.object_data = self.initial.get('date_all', None)

        self.fields['artist_trade'] = forms.ModelChoiceField(queryset=ArtistProfile.objects.filter(user=self.object_user), empty_label=None)
        self.fields['club_trade'] = forms.ModelChoiceField(queryset=ClubProfile.objects.filter(user=self.object_user), empty_label=None)

        if self.object_data:
            # Формируем выпадайку выбора договора
            contracts = self.object_data.contract_artist + self.object_data.contract_site
            if contracts > 1:
                self.fields['contracts'].required = True
            b_contracts = self.object_data.barter_contract_artist + self.object_data.barter_contract_site
            if b_contracts > 1:
                self.fields['b_contracts'].required = True
            CHOE = ()
            if self.object_data.contract_artist:
                CHOE += Trades.CON0
            if self.object_data.contract_site:
                CHOE += Trades.CON2
            self.fields['contracts'] = forms.ChoiceField(choices=CHOE)
            CHOE2 = ()
            if self.object_data.barter_contract_artist:
                CHOE2 += Trades.CON0
            if self.object_data.barter_contract_site:
                CHOE2 += Trades.CON2
            self.fields['b_contracts'] = forms.ChoiceField(choices=CHOE2)

            # Формируем выпадайку типов мероприятия
            choess = [('', '----------')]
            class_price = []
            id_price = []
            if not self.object_data.event_public == None:
                if not self.object_data.event_public == 1:
                    class_price.append('event_public')
                    id_price.append('0')
                choess.append((Trades.EVENT_TYPE[0][0], Trades.EVENT_TYPE[0][1]))
            if not self.object_data.event_private == None:
                if not self.object_data.event_private == 1:
                    class_price.append('event_private')
                    id_price.append('1')
                choess.append((Trades.EVENT_TYPE[1][0], Trades.EVENT_TYPE[1][1]))
            if not self.object_data.event_presentation == None:
                if not self.object_data.event_presentation == 1:
                    class_price.append('event_presentation')
                    id_price.append('2')
                choess.append((Trades.EVENT_TYPE[2][0], Trades.EVENT_TYPE[2][1]))
            self.fields['event_type'].choices = choess
            self.fields['event_type'].required = True

            # Поле ввода цены
            if class_price:
                self.fields['mony_fix'].widget.attrs['class'] = " ".join(class_price)

            data = kwargs.get('data')
            if data and data.get('event_type') and data.get('event_type') in id_price:
                self.fields['mony_fix'].widget.attrs['visible_div'] = True
            self.id_price = id_price


            b_choess = [('', '----------')]
            b_class_price = []
            b_id_price = []
            if not self.object_data.barter_event_public == None:
                if not self.object_data.barter_event_public == 1:
                    b_class_price.append('barter_event_public')
                    b_id_price.append('0')
                b_choess.append((Trades.EVENT_TYPE[0][0], Trades.EVENT_TYPE[0][1]))
            if not self.object_data.barter_event_private == None:
                if not self.object_data.barter_event_private == 1:
                    b_class_price.append('barter_event_private')
                    b_id_price.append('1')
                b_choess.append((Trades.EVENT_TYPE[1][0], Trades.EVENT_TYPE[1][1]))
            if not self.object_data.barter_event_presentation == None:
                if not self.object_data.barter_event_presentation == 1:
                    b_class_price.append('barter_event_presentation')
                    b_id_price.append('2')
                b_choess.append((Trades.EVENT_TYPE[2][0], Trades.EVENT_TYPE[2][1]))
            self.fields['b_event_type'].choices = b_choess
            self.fields['b_event_type'].required = True

            if b_class_price:
                self.fields['b_mony_fix'].widget.attrs['class'] = " ".join(b_class_price)

            if data and data.get('b_event_type') and data.get('b_event_type') in b_id_price:
                self.fields['b_mony_fix'].widget.attrs['visible_div'] = True
            self.b_id_price = b_id_price

    def clean(self):
        super(TradesBarterForm, self).clean()
        event_type = self.cleaned_data.get('event_type')
        b_event_type = self.cleaned_data.get('b_event_type')
        contracts = self.object_data.contract_artist + self.object_data.contract_site
        if contracts < 2:
            if self.object_data.contract_artist:
                self.cleaned_data['contracts'] = 0
            else:
                self.cleaned_data['contracts'] = 2
        b_contracts = self.object_data.barter_contract_artist + self.object_data.barter_contract_site
        if b_contracts < 2:
            if self.object_data.barter_contract_artist:
                self.cleaned_data['b_contracts'] = 0
            else:
                self.cleaned_data['contracts'] = 2

        if event_type >= 0 and '%s' % event_type in self.id_price:
            if not self.cleaned_data.get('mony_fix'):
                self._errors["mony_fix"] = self.error_class([_('This field is required')])
        else:
            if self._errors.get('mony_fix'):
                del self._errors['mony_fix']
        if b_event_type >= 0 and '%s' % b_event_type in self.b_id_price:
            if not self.cleaned_data.get('b_mony_fix'):
                self._errors["b_mony_fix"] = self.error_class([_('This field is required')])
        else:
            if self._errors.get('b_mony_fix'):
                del self._errors['b_mony_fix']

        if not event_type == None:
            if event_type == 0:
                self.cleaned_data['fee_event_type'] = self.object_data.event_public
                if self.object_data.event_public == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_publ_fix
                    self.cleaned_data['mony_edit'] = True
            elif event_type == 1:
                self.cleaned_data['fee_event_type'] = self.object_data.event_private
                if self.object_data.event_private == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_priv_fix
                    self.cleaned_data['mony_edit'] = True
            elif event_type == 2:
                self.cleaned_data['fee_event_type'] = self.object_data.event_presentation
                if self.object_data.event_presentation == 1:
                    self.cleaned_data['mony_fix'] = self.object_data.mony_pres_fix
                    self.cleaned_data['mony_edit'] = True
            else:
                pass
        if not b_event_type == None:
            if b_event_type == 0:
                self.cleaned_data['b_fee_event_type'] = self.object_data.barter_event_public
                if self.object_data.barter_event_public == 1:
                    self.cleaned_data['b_mony_fix'] = self.object_data.barter_mony_publ_fix
                    self.cleaned_data['b_mony_edit'] = True
            elif b_event_type == 1:
                self.cleaned_data['b_fee_event_type'] = self.object_data.barter_event_private
                if self.object_data.barter_event_private == 1:
                    self.cleaned_data['b_mony_fix'] = self.object_data.barter_mony_priv_fix
                    self.cleaned_data['b_mony_edit'] = True
            elif b_event_type == 2:
                self.cleaned_data['b_fee_event_type'] = self.object_data.barter_event_presentation
                if self.object_data.barter_event_presentation == 1:
                    self.cleaned_data['b_mony_fix'] = self.object_data.barter_mony_pres_fix
                    self.cleaned_data['b_mony_edit'] = True
            else:
                pass
        return self.cleaned_data

    class Meta:
        model = Trades
        fields = ['artist_trade', 'club_trade', 'contracts', 'event_type', 'fee_event_type', 'mony_fix', 'mony_edit',
                  'b_mony_edit', 'b_contracts', 'b_event_type', 'b_fee_event_type', 'b_mony_fix']


class TradesChatForm(forms.ModelForm):
    """
    Форма чата
    """

    class Meta:
        model = Chat
        fields = ['comment', 'files']


class MonyEditForm(forms.Form):
    mony_fix = forms.IntegerField(required=False, min_value=0)
    b_mony_fix = forms.IntegerField(required=False, min_value=0)