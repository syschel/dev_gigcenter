#-*- coding:utf-8 -*-
from django.conf import settings
from datetime import datetime
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
from annoying.decorators import render_to
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView
from common.functions import send_mailbox
from common.files import render_to_pdf
from trade.models import Trades, Chat, ContractText
from trade.forms import TradesChatForm, MonyEditForm
from account.models import ArtistProfile, ClubProfile


class FilterList(ListView):

    def get(self, request, *args, **kwargs):
        return super(FilterList, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FilterList, self).get_context_data(**kwargs)
        get = self.request.GET.copy()
        if get:
            if 'artist' in get:
                context['artist_pk'] = int(get.get('artist'))
            if 'club' in get:
                context['club_pk'] = int(get.get('club'))
        return context

    def get_queryset(self):
        queryset = super(FilterList, self).get_queryset()
        get = self.request.GET.copy()
        if get:
            if 'artist' in get:
                queryset = queryset.filter(Q(artist_date=get.get('artist')) | Q(artist_trade=get.get('artist')))
            if 'club' in get:
                queryset = queryset.filter(Q(club_date=get.get('club')) | Q(club_trade=get.get('club')))
            if 'sort' in get:
                if get.get('sort') == 'inbox':
                    queryset = queryset.filter(user_trade=self.request.user)
                if get.get('sort') == 'outbox':
                    queryset = queryset.filter(user_date=self.request.user)
                if get.get('sort') == 'deals':
                    queryset = queryset.filter(status_trade=Trades.STRADE2)
                if get.get('sort') == 'rejected':
                    queryset = queryset.filter(status_trade=Trades.STRADE4)
        return queryset


class TradeListView(FilterList):
    template_name = 'trade/account/trade_list.html'
    context_object_name = 'trands'
    model = Trades
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super(TradeListView, self).get_context_data(**kwargs)
        context['submenu_url'] = 'trade'
        context['artists'] = ArtistProfile.objects.filter(user=self.request.user)
        context['clubs'] = ClubProfile.objects.filter(user=self.request.user)
        return context

    def get_queryset(self):
        queryset = super(TradeListView, self).get_queryset()
        queryset = queryset.filter(Q(user_date=self.request.user) | Q(user_trade=self.request.user))
        for trand in queryset:
            trand.status = trand.get_status_trade_display()

            if trand.user_date == self.request.user:
                trand.bold = True if trand.view_date_user else False
                trand.recipient = '-'
                if trand.type_date == Trades.TYPEDATE0:
                    trand.data_url = trand.date_all.gigdate.get_absolute_url()
                    trand.sender = trand.club_trade.nickname
                    trand.type_user = True
                elif trand.type_date == Trades.TYPEDATE1:
                    trand.data_url = trand.date_all.gigbarter.get_absolute_url()
                    trand.sender = trand.artist_trade.nickname
                    trand.type_user = True
                elif trand.type_date == Trades.TYPEDATE2:
                    trand.data_url = trand.date_all.gigtender.get_absolute_url()
                    trand.sender = trand.artist_trade.nickname
                    trand.type_user = False

            if trand.user_trade == self.request.user:
                trand.bold = True if trand.view_trade_user else False
                trand.sender = '-'
                if trand.type_date == Trades.TYPEDATE0:
                    trand.data_url = trand.date_all.gigdate.get_absolute_url()
                    trand.recipient = trand.artist_date.nickname
                    trand.type_user = False
                elif trand.type_date == Trades.TYPEDATE1:
                    trand.data_url = trand.date_all.gigbarter.get_absolute_url()
                    trand.recipient = trand.artist_date.nickname
                    trand.type_user = False
                elif trand.type_date == Trades.TYPEDATE2:
                    trand.data_url = trand.date_all.gigtender.get_absolute_url()
                    trand.recipient = trand.club_date.nickname
                    trand.type_user = True
        return queryset


@render_to('trade/account/trades_detail.html')
def tradedetai(request, pk):
    """
    Выводим выбранный трейд
    """
    artists = ArtistProfile.objects.filter(user=request.user)
    clubs = ClubProfile.objects.filter(user=request.user)
    trade = get_object_or_404(Trades, Q(user_date=request.user) | Q(user_trade=request.user), pk=pk)

    def active_user(request, trades):
        """ Для активной ссылке в меню профилей ищем владельца заявки """
        if trades.type_date == 1:
            if trades.user_date == request.user:
                key = u'artist_pk'
                value = trades.artist_date.pk
            else:
                key = u'artist_pk'
                value = trades.artist_trade.pk
        elif trades.type_date == 2:
            if trades.user_date == request.user:
                key = u'club_pk'
                value = trades.club_date.pk
            else:
                key = u'artist_pk'
                value = trades.artist_trade.pk
        else:
            if trades.user_date == request.user:
                key = u'artist_pk'
                value = trades.artist_date.pk
            else:
                key = u'club_pk'
                value = trades.club_trade.pk
        return key, value
    keys, values = active_user(request, trade)

    # меняем маркер "не просмотренно"
    if trade.user_date == request.user and trade.view_date_user:
        trade.view_date_user = False
        trade.save()
    if trade.user_trade == request.user and trade.view_trade_user:
        trade.view_trade_user = False
        trade.save()

    if 'apply' in request.GET:
        chat = Chat()
        chat.trands = trade
        chat.trades_author = True if trade.user_trade == request.user else False
        if trade.user_date == request.user\
                and not trade.trade_date_user\
                and trade.trade_trade_user1:
            trade.trade_date_user = True
            trade.view_trade_user = True
            trade.save()
            chat.comment = _(u"Accepted request and signed a deal")
            chat.save()
            send_mailbox(trade.user_trade, u"Request accepted", u"Your request accepted <a href='"+reverse('account:trade_view', args=[trade.pk])+u"'>"+reverse('account:trade_view', args=[trade.pk])+u"<a/>")
            messages.info(request, _('You accepted request %s and signed a deal' % trade.pk))
        if trade.user_trade == request.user\
                and trade.trade_date_user\
                and trade.trade_trade_user1\
                and not trade.trade_trade_user2:
            trade.trade_trade_user2 = True
            trade.status_trade = Trades.STRADE2
            trade.view_date_user = True
            trade.create_deal = datetime.today()
            trade.save()
            chat.comment = _(u"Confirmed request, signed a deal. The deal is created. Please check the details of your Deal.")
            chat.save()
            if trade.date_all.get_sub_model().count_trade >= Trades.objects.filter(date_all=trade.date_all, status_trade=Trades.STRADE2).count():
                trades = Trades.objects.filter(Q(date_all=trade.date_all), ~Q(status_trade=Trades.STRADE2), ~Q(pk=trade.pk))
                for chat_trade in trades:
                    if not chat_trade.user_trade == trade.user_trade and not chat_trade.status_trade == Trades.STRADE2:
                        sp_chat = Chat()
                        sp_chat.trands = chat_trade
                        sp_chat.trades_author = True if trade.user_trade == request.user else False
                        sp_chat.comment = _(u"This date has already been booked. Please, try again later or choosing another date.")
                        sp_chat.save()

            send_mailbox(trade.user_date, u"Request accepted", u"Your request accepted <a href='"+reverse('account:trade_view', args=[trade.pk])+u"'>"+reverse('account:trade_view', args=[trade.pk])+u"<a/>")
            messages.info(request, _('You accepted request %s and signed a deal' % trade.pk))
        return HttpResponseRedirect(reverse('account:trade_view', args=[trade.pk]))
    if 'deny' in request.GET:
        if trade.user_date == request.user or trade.user_trade == request.user:
            trade.status_trade = Trades.STRADE4
            if trade.user_date == request.user:
                trade.view_trade_user = True
            if trade.user_trade == request.user:
                trade.view_date_user = True
            trade.save()
            chat = Chat()
            chat.trands = trade
            chat.trades_author = True if trade.user_trade == request.user else False
            chat.comment = _(u"Rejected the request")
            chat.save()
            send_mailbox(trade.user_date if trade.user_trade == request.user else trade.user_trade, u"Rejected the request", u"Rejected the request <a href='"+reverse('account:trade_view', args=[trade.pk])+u"'>"+reverse('account:trade_view', args=[trade.pk])+u"<a/>")
        messages.info(request, _('You rejected request %s' % trade.pk))
        return HttpResponseRedirect(reverse('account:trade_view', args=[trade.pk]))

    if request.method == 'POST':
        if 'mony_edit' in request.POST:
            form1 = MonyEditForm(request.POST)
            if form1.is_valid():
                valid = False
                if form1.cleaned_data.get('mony_fix'):
                    trade.mony_fix = form1.cleaned_data.get('mony_fix')
                    valid = True
                if form1.cleaned_data.get('b_mony_fix'):
                    trade.b_mony_fix = form1.cleaned_data.get('b_mony_fix')
                    valid = True
                if valid:
                    trade.save()
                    chat = Chat()
                    chat.trands = trade
                    chat.comment = _(u"Changed the fee to") + u" %s" % form1.cleaned_data.get('mony_fix')
                    chat.trades_author = True if trade.user_trade == request.user else False
                    chat.save()
                    messages.success(request, _(u"The fee is changed"))
                    send_mailbox(trade.user_date if trade.user_trade == request.user else trade.user_trade, u"Changed the fee to" + u" %s" % form1.cleaned_data.get('mony_fix'), u"Request <a href='"+reverse('account:trade_view', args=[trade.pk])+u"'>"+reverse('account:trade_view', args=[trade.pk])+u"<a/>")
                else:
                    messages.success(request, _(u"This is don`t fee sum"))
                return HttpResponseRedirect(reverse('account:trade_view', args=[trade.pk]))
        else:
            form1 = MonyEditForm(initial={'mony_fix': trade.mony_fix})

        if 'chat_add' in request.POST:
            form2 = TradesChatForm(request.POST, request.FILES)
            if form2.is_valid():
                chat = form2.save(commit=False)
                chat.trands = trade
                chat.trades_author = True if trade.user_trade == request.user else False
                chat.save()
                messages.success(request, _(u"Message sent"))
                send_mailbox(trade.user_date if trade.user_trade == request.user else trade.user_trade, u"New message on Your %s request: %s" % (trade.get_type_date_display(), trade.pk), u"Request <a href='"+reverse('account:trade_view', args=[trade.pk])+u"'>"+reverse('account:trade_view', args=[trade.pk])+u"<a/>")
                return HttpResponseRedirect(reverse('account:trade_view', args=[trade.pk]))
        else:
            form2 = TradesChatForm()
    else:
        initial = {'mony_fix': trade.mony_fix, 'b_mony_fix': trade.b_mony_fix} if trade.type_date == Trades.TYPEDATE1 else {'mony_fix': trade.mony_fix}
        form1 = MonyEditForm(initial=initial)
        form2 = TradesChatForm()

    block_apply = False
    if trade.user_date == request.user and trade.trade_trade_user1 and trade.trade_date_user:
        block_apply = True
    if trade.user_trade == request.user and trade.trade_trade_user1 and trade.trade_trade_user2:
        block_apply = True

    return {'artists': artists, 'clubs': clubs, 'submenu_url': 'trade', 'object': trade, keys: values,
            'form1': form1, 'form2': form2, 'chat': trade.trades_chat.all(), 'block_apply': block_apply}


class DealsDetailView(DetailView):
    template_name = 'trade/account/deals_base.html'
    model = Trades

    def contract_text(self):
        """ Достаём текст договора """
        contract_text = ContractText.objects.filter(contract_type=self.object.type_date)
        if contract_text:
            contract_text = contract_text[0]
        return contract_text

    def get_context_data(self, **kwargs):
        context = super(DealsDetailView, self).get_context_data(**kwargs)

        context['submenu_url'] = 'trade'
        context['contract_text'] = self.contract_text()
        context['chat'] = self.object.trades_chat.all()
        context['hidden_bottom'] = True
        return context

    def get_object(self, queryset=None):
        queryset = get_object_or_404(self.model, Q(user_date=self.request.user) | Q(user_trade=self.request.user), pk=self.kwargs.get("pk"))
        return queryset


class DealsDetailViewPrint(DealsDetailView):
    template_name = 'trade/account/print.html'

    def get_context_data(self, **kwargs):
        context = super(DealsDetailViewPrint, self).get_context_data(**kwargs)
        context['chat'] = False
        context['hidden_bottom'] = False
        return context


class DealsDetailViewPDF(DealsDetailView):
    template_name = 'trade/account/pdf.html'

    def get_context_data(self, **kwargs):
        context = super(DealsDetailViewPDF, self).get_context_data(**kwargs)
        context['chat'] = False
        context['hidden_bottom'] = False
        context['TEMPLATE'] = self.template_name
        context['STATIC_ROOT'] = settings.STATIC_ROOT
        return context

    def render_to_response(self, context, **response_kwargs):
        """ render_to_pdf(template_src, context_dict, filename='contract.pdf') """
        return render_to_pdf(self.template_name, context, 'Gigcenter.com_contract_%s_%s.pdf' % (self.object.get_type_date_display(), self.object.pk))