# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0014_auto_20150812_1056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='replys',
            name='queries2',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0422\u0438\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, 'Public'), (1, 'Private'), (2, 'Other')]),
        ),
        migrations.AlterField(
            model_name='replys',
            name='queries3',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, '\u0421commercial'), (1, '\u0421haritable'), (2, 'Noncommercial'), (3, 'Other')]),
        ),
        migrations.AlterField(
            model_name='trades',
            name='b_event_type',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0422\u0438\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f \u0431\u0430\u0440\u0442\u0435\u0440', choices=[(0, 'Public'), (1, 'Private'), (2, 'Other')]),
        ),
        migrations.AlterField(
            model_name='trades',
            name='event_type',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0422\u0438\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, 'Public'), (1, 'Private'), (2, 'Other')]),
        ),
    ]
