# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0007_auto_20150710_0650'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trades',
            name='status_date',
        ),
    ]
