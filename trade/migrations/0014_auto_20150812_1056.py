# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0013_auto_20150716_1918'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trades',
            name='b_contracts',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0414\u043e\u0433\u043e\u0432\u043e\u0440 \u0431\u0430\u0440\u0442\u0435\u0440\u0430', choices=[(0, 'Artists contract'), (2, 'Gigcenter contract form')]),
        ),
        migrations.AlterField(
            model_name='trades',
            name='contracts',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0424\u043e\u0440\u043c\u0430 \u0414\u043e\u0433\u043e\u0432\u043e\u0440\u0430', choices=[(0, 'Artists contract'), (2, 'Gigcenter contract form')]),
        ),
    ]
