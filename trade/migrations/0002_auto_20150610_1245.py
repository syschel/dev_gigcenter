# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trades',
            name='fee_event_type',
        ),
        migrations.AlterField(
            model_name='trades',
            name='contracts',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0424\u043e\u0440\u043c\u0430 \u0414\u043e\u0433\u043e\u0432\u043e\u0440\u0430', choices=[(0, 'Artists contract'), (1, 'Customers contract'), (2, 'Gigcenter contract form')]),
        ),
        migrations.AlterField(
            model_name='trades',
            name='date_all',
            field=models.ForeignKey(related_name='trade_date', to='gigdate.DateAll', null=True),
        ),
        migrations.AlterField(
            model_name='trades',
            name='user_date',
            field=models.ForeignKey(related_name='trade_user_date', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='trades',
            name='user_trade',
            field=models.ForeignKey(related_name='trade_user_trade', to=settings.AUTH_USER_MODEL),
        ),
    ]
