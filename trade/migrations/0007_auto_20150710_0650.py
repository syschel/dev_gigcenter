# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import common.files


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0006_auto_20150708_0727'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('comment', models.TextField(null=True, verbose_name='#\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('files', models.FileField(upload_to=common.files.get_file_path, null=True, verbose_name='#\u0424\u0430\u0439\u043b', blank=True)),
                ('user_date', models.BooleanField(default=True, verbose_name='#\u0410\u0432\u0442\u043e\u0440')),
            ],
            options={
                'verbose_name': 'Chat',
                'verbose_name_plural': 'Chats',
            },
        ),
        migrations.AlterField(
            model_name='trades',
            name='b_fee_event_type',
            field=models.IntegerField(default=0, null=True, verbose_name='#\u0422\u0438\u043f \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430 \u0431\u0430\u0440\u0442\u0435\u0440', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, 'Another fee')]),
        ),
        migrations.AlterField(
            model_name='trades',
            name='fee_event_type',
            field=models.IntegerField(default=0, null=True, verbose_name='#\u0422\u0438\u043f \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, 'Another fee')]),
        ),
        migrations.AddField(
            model_name='chat',
            name='trands',
            field=models.ForeignKey(related_name='trades_chat', verbose_name='#\u0417\u0430\u044f\u0432\u043a\u0430', blank=True, to='trade.Trades', null=True),
        ),
    ]
