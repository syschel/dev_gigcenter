# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0005_auto_20150614_1817'),
    ]

    operations = [
        migrations.AddField(
            model_name='trades',
            name='fee_event_type',
            field=models.IntegerField(default=0, null=True, verbose_name='#\u0422\u0438\u043f \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, '#Another fee')]),
        ),
        migrations.AlterField(
            model_name='trades',
            name='view_date_user',
            field=models.BooleanField(default=True, verbose_name='#\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0432\u043b\u0430\u0434\u0435\u043b\u044c\u0446\u0435\u043c \u0434\u0430\u0442\u044b'),
        ),
        migrations.AlterField(
            model_name='trades',
            name='view_trade_user',
            field=models.BooleanField(default=False, verbose_name='#\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u0441\u043e\u0437\u0434\u0430\u0442\u0435\u043b\u0435\u043c \u0437\u0430\u044f\u0432\u043a\u0438'),
        ),
    ]
