# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('account', '0006_auto_20150610_0940'),
        ('gigdate', '0005_auto_20150609_1545'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trades',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create', models.DateTimeField(auto_now_add=True)),
                ('update', models.DateTimeField(auto_now=True)),
                ('type_date', models.IntegerField(default=0, blank=True, verbose_name='#\u0422\u0438\u043f \u0434\u0430\u0442\u044b', choices=[(0, 'gigdate'), (1, 'barter'), (2, 'tender')])),
                ('view_date_user', models.BooleanField(default=False)),
                ('view_trade_user', models.BooleanField(default=True)),
                ('trade_trade_user1', models.BooleanField(default=True, verbose_name='#\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c \u0437\u0430\u044f\u0432\u043a\u0430 1')),
                ('trade_trade_user2', models.BooleanField(default=False, verbose_name='#\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c \u0437\u0430\u044f\u0432\u043a\u0430 2')),
                ('trade_date_user', models.BooleanField(default=False, verbose_name='#\u041f\u0440\u043e\u0434\u0430\u0432\u0435\u0446 \u0437\u0430\u044f\u0432\u043a\u0430 1')),
                ('status_date', models.IntegerField(default=0, verbose_name='#\u0421\u0442\u0430\u0442\u0443\u0441 \u043f\u0440\u043e\u0434\u0430\u0432\u0446\u0430', choices=[(0, 'Expectation'), (1, 'Request'), (2, 'Trade'), (3, 'Finish'), (4, 'Rejected')])),
                ('status_trade', models.IntegerField(default=1, verbose_name='#\u0421\u0442\u0430\u0442\u0443\u0441 \u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044f', choices=[(0, 'Expectation'), (1, 'Request'), (2, 'Trade'), (3, 'Finish'), (4, 'Rejected')])),
                ('event_type', models.IntegerField(blank=True, null=True, verbose_name='#\u0422\u0438\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, 'Public'), (1, 'Private'), (2, 'Presentation')])),
                ('fee_event_type', models.IntegerField(default=0, null=True, verbose_name='#\u0422\u0438\u043f \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, '#Another fee')])),
                ('mony_fix', models.IntegerField(null=True, verbose_name='#\u0421\u0443\u043c\u043c\u0430 \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', blank=True)),
                ('mony_edit', models.BooleanField(default=False, verbose_name='#\u0417\u0430\u043f\u0440\u0435\u0442 \u0438\u0437\u043c\u0435\u043d\u044f\u0442\u044c \u0433\u043e\u043d\u043e\u0440\u0430\u0440')),
                ('contracts', models.IntegerField(blank=True, null=True, verbose_name='#\u0414\u043e\u0433\u043e\u0432\u043e\u0440', choices=[(0, 'Artists contract'), (1, 'Customers contract'), (2, 'Gigcenter contract form')])),
                ('b_event_type', models.IntegerField(blank=True, null=True, verbose_name='#\u0422\u0438\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f \u0431\u0430\u0440\u0442\u0435\u0440', choices=[(0, 'Public'), (1, 'Private'), (2, 'Presentation')])),
                ('b_fee_event_type', models.IntegerField(default=0, null=True, verbose_name='#\u0422\u0438\u043f \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430 \u0431\u0430\u0440\u0442\u0435\u0440', blank=True, choices=[(0, 'Contractual fee'), (1, 'Fixed fee'), (2, '#Another fee')])),
                ('b_mony_fix', models.IntegerField(null=True, verbose_name='#\u0421\u0443\u043c\u043c\u0430 \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430 \u0431\u0430\u0440\u0442\u0435\u0440', blank=True)),
                ('b_mony_edit', models.BooleanField(default=False, verbose_name='#\u0417\u0430\u043f\u0440\u0435\u0442 \u0438\u0437\u043c\u0435\u043d\u044f\u0442\u044c \u0433\u043e\u043d\u043e\u0440\u0430\u0440')),
                ('b_contracts', models.IntegerField(blank=True, null=True, verbose_name='#\u0414\u043e\u0433\u043e\u0432\u043e\u0440 \u0431\u0430\u0440\u0442\u0435\u0440\u0430', choices=[(0, 'Artists contract'), (1, 'Customers contract'), (2, 'Gigcenter contract form')])),
                ('artist_date', models.ForeignKey(related_name='trade_artist_date', blank=True, to='account.ArtistProfile', null=True)),
                ('artist_trade', models.ForeignKey(related_name='trade_artist_trade', blank=True, to='account.ArtistProfile', null=True)),
                ('club_date', models.ForeignKey(related_name='trade_club_date', blank=True, to='account.ClubProfile', null=True)),
                ('club_trade', models.ForeignKey(related_name='trade_club_trade', blank=True, to='account.ClubProfile', null=True)),
                ('date_all', models.ForeignKey(related_name='trade_date', blank=True, to='gigdate.DateAll', null=True)),
                ('user_date', models.ForeignKey(related_name='trade_user_date', blank=True, to=settings.AUTH_USER_MODEL)),
                ('user_trade', models.ForeignKey(related_name='trade_user_trade', blank=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-update'],
                'verbose_name': 'Trade',
                'verbose_name_plural': 'Trades',
            },
        ),
    ]
