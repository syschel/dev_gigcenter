# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0009_auto_20150710_1639'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chat',
            name='date_author',
        ),
        migrations.AddField(
            model_name='chat',
            name='trades_author',
            field=models.BooleanField(default=True, verbose_name='#\u0410\u0432\u0442\u043e\u0440'),
        ),
    ]
