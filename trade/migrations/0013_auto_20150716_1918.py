# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0012_trades_create_deal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contracttext',
            name='contract_type',
            field=models.IntegerField(default=0, verbose_name='Type date', choices=[(0, 'gigdate'), (1, 'barter'), (2, 'tender')]),
        ),
        migrations.AlterField(
            model_name='trades',
            name='create_deal',
            field=models.DateTimeField(help_text='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f \u0441\u0434\u0435\u043b\u043a\u0438', verbose_name='#\u0421\u0434\u0435\u043b\u043a\u0430', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='trades',
            name='status_trade',
            field=models.IntegerField(default=1, verbose_name='#\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(0, 'Expectation'), (1, 'Request'), (2, 'Deals'), (3, 'Finish'), (4, 'Rejected')]),
        ),
    ]
