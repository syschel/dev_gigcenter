# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0010_auto_20150712_1607'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chat',
            name='trades_author',
            field=models.BooleanField(default=True, verbose_name='\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446 \u0437\u0430\u044f\u0432\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='trades',
            name='b_mony_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0421\u0443\u043c\u043c\u0430 \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430 \u0431\u0430\u0440\u0442\u0435\u0440', blank=True),
        ),
        migrations.AlterField(
            model_name='trades',
            name='mony_fix',
            field=models.PositiveIntegerField(null=True, verbose_name='#\u0421\u0443\u043c\u043c\u0430 \u0433\u043e\u043d\u043e\u0440\u0430\u0440\u0430', blank=True),
        ),
    ]
