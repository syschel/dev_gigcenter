# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0008_remove_trades_status_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='chat',
            old_name='user_date',
            new_name='date_author',
        ),
        migrations.AlterField(
            model_name='trades',
            name='status_trade',
            field=models.IntegerField(default=1, verbose_name='#\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(0, 'Expectation'), (1, 'Request'), (2, 'Trade'), (3, 'Finish'), (4, 'Rejected')]),
        ),
    ]
