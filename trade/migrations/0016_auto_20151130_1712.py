# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0015_auto_20150918_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='replys',
            name='queries3',
            field=models.IntegerField(blank=True, null=True, verbose_name='#\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, 'Commercial'), (1, 'Noncommercial'), (2, 'Charitable'), (3, 'Other')]),
        ),
    ]
