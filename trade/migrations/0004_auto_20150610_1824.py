# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0003_replys'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='replys',
            options={'verbose_name': 'Reply', 'verbose_name_plural': 'Replys'},
        ),
    ]
