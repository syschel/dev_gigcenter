# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0011_auto_20150712_1635'),
    ]

    operations = [
        migrations.AddField(
            model_name='trades',
            name='create_deal',
            field=models.DateTimeField(null=True, verbose_name='#\u0421\u043e\u0437\u0434\u0430\u043d\u0430 \u0441\u0434\u0435\u043b\u043a\u0430', blank=True),
        ),
    ]
