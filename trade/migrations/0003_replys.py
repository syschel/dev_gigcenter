# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0004_faqcategory'),
        ('trade', '0002_auto_20150610_1245'),
    ]

    operations = [
        migrations.CreateModel(
            name='Replys',
            fields=[
                ('trades_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='trade.Trades')),
                ('queries1', models.CharField(max_length=255, null=True, verbose_name='#\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', blank=True)),
                ('queries2', models.IntegerField(blank=True, null=True, verbose_name='#\u0422\u0438\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, 'Public'), (1, 'Private'), (2, 'Presentation')])),
                ('queries3', models.IntegerField(blank=True, null=True, verbose_name='#\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', choices=[(0, '\u0421commercial'), (1, '\u0421haritable')])),
                ('queries4', models.IntegerField(blank=True, null=True, verbose_name='#\u0417\u0430\u043a\u0440\u044b\u0442\u043e\u0435 \u0438\u043b\u0438 \u043e\u0442\u043a\u0440\u044b\u0442\u043e\u0439 \u043c\u0435\u0441\u0442\u043e \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f', choices=[(0, 'Outdoor'), (1, 'Indoor')])),
                ('queries5', models.CharField(max_length=255, null=True, verbose_name='#\u0420\u0430\u0437\u043c\u0435\u0440 \u0441\u0446\u0435\u043d\u044b', blank=True)),
                ('queries6', models.CharField(max_length=255, null=True, verbose_name='#\u041d\u0430\u043b\u0438\u0447\u0438\u0435 \u0441\u043f\u043e\u043d\u0441\u043e\u0440\u043e\u0432', blank=True)),
                ('queries7', models.CharField(max_length=255, null=True, verbose_name='#\u0426\u0435\u043d\u0430 \u0432\u0445\u043e\u0434\u043d\u043e\u0433\u043e \u0431\u0438\u043b\u0435\u0442\u0430', blank=True)),
                ('queries8', models.CharField(max_length=255, null=True, verbose_name='#\u041c\u0435\u0441\u0442\u0430 \u0440\u0430\u0441\u043f\u0440\u043e\u0441\u0442\u0440\u0430\u043d\u0435\u043d\u0438\u044f \u0431\u0438\u043b\u0435\u0442\u043e\u0432', blank=True)),
                ('queries9', models.CharField(max_length=255, null=True, verbose_name='#\u0412\u0440\u0435\u043c\u044f \u043d\u0430\u0447\u0430\u043b\u0430 \u0438 \u043a\u043e\u043d\u0446\u0430', blank=True)),
                ('queries10', models.CharField(max_length=255, null=True, verbose_name='#\u0412\u0440\u0435\u043c\u044f \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f \u0430\u0440\u0442\u0438\u0441\u0442\u0430', blank=True)),
                ('queries11', models.CharField(max_length=255, null=True, verbose_name='#\u0412\u0440\u0435\u043c\u044f \u0441\u0430\u0443\u043d\u0434\u0447\u0435\u043a\u0430', blank=True)),
                ('queries12', models.CharField(max_length=255, null=True, verbose_name='#\u041b\u0430\u0439\u043d\u0430\u043f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', blank=True)),
                ('queries14', models.CharField(max_length=255, null=True, verbose_name='#\u041d\u0430\u043b\u0438\u0447\u0438\u0435 \u0442\u0430\u043d\u0446\u043e\u0440\u043e\u0432/go go/\u043f\u0435\u0440\u0444\u043e\u0440\u043c\u0430\u043d\u0441', blank=True)),
                ('queries15', models.CharField(max_length=255, null=True, verbose_name='#\u0412\u043e\u0437\u0440\u0430\u0441\u0442 \u0430\u0443\u0434\u0438\u0442\u043e\u0440\u0438\u0438', blank=True)),
                ('queries16', models.CharField(max_length=255, null=True, verbose_name='#\u0422\u0438\u043f \u0430\u0443\u0434\u0438\u0442\u043e\u0440\u0438\u0438', blank=True)),
                ('queries17', models.IntegerField(blank=True, null=True, verbose_name='#\u0410\u0440\u0435\u043d\u0434\u0430 \u043f\u043b\u043e\u0449\u0430\u0434\u043a\u0438', choices=[(0, 'Owned'), (1, 'Long-term rent'), (2, 'One-time rent')])),
                ('queries18', models.TextField(null=True, verbose_name='#\u0410\u0440\u0442\u0438\u0441\u0442\u044b \u0438 \u0430\u0433\u0435\u043d\u0442\u044b, \u0441 \u043a\u043e\u0442\u043e\u0440\u044b\u043c\u0438 \u0437\u0430\u043a\u0430\u0437\u0447\u0438\u043a \u0443\u0436\u0435 \u0440\u0430\u0431\u043e\u0442\u0430\u043b', blank=True)),
                ('queries19', models.TextField(null=True, verbose_name='#\u0410\u0440\u0442\u0438\u0441\u0442\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u044b \u043d\u0430 \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u0435 \u0432 \u0431\u043b\u0438\u0436\u0430\u0439\u0448\u0435\u043c \u0431\u0443\u0434\u0443\u0449\u0435\u043c \u043d\u0430 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f\u0445 \u0437\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u0430', blank=True)),
                ('queries20', models.TextField(null=True, verbose_name='#\u0410\u0440\u0442\u0438\u0441\u0442\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0443\u0436\u0435 \u0432\u044b\u0441\u0442\u0443\u043f\u0430\u043b\u0438 \u043d\u0430 \u043f\u043b\u043e\u0449\u0430\u0434\u043a\u0435 \u043f\u0440\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u044f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', blank=True)),
                ('queries21', models.TextField(null=True, verbose_name='#\u0410\u0440\u0442\u0438\u0441\u0442\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u044b \u043d\u0430 \u0432\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u0435 \u0432 \u0431\u043b\u0438\u0436\u0430\u0439\u0448\u0435\u043c \u0431\u0443\u0434\u0443\u0449\u0435\u043c \u043d\u0430 \u043f\u043b\u043e\u0449\u0430\u0434\u043a\u0435 \u043f\u0440\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u044f \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f', blank=True)),
                ('queries13', models.ManyToManyField(to='common.MethodsAdvert', verbose_name='#\u041c\u0435\u0442\u043e\u0434\u044b \u0440\u0435\u043a\u043b\u0430\u043c\u044b', blank=True)),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0432\u0435\u0442\u044b',
                'verbose_name_plural': '#\u041e\u0442\u0432\u0435\u0442\u044b',
            },
            bases=('trade.trades',),
        ),
    ]
