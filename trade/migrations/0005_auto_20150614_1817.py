# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('trade', '0004_auto_20150610_1824'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContractText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contract_type', models.IntegerField(default=0, verbose_name='Type date', choices=[(0, 'GigDate'), (1, 'Barter'), (2, 'Tender')])),
                ('text_before', tinymce.models.HTMLField(null=True, verbose_name='Before', blank=True)),
                ('text_after', tinymce.models.HTMLField(null=True, verbose_name='After', blank=True)),
            ],
            options={
                'verbose_name': 'Contract text',
                'verbose_name_plural': 'Contract texts',
            },
        ),
        migrations.AlterUniqueTogether(
            name='contracttext',
            unique_together=set([('contract_type',)]),
        ),
    ]
