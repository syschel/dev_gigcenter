#-*- coding:utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse
from account.models import User, ArtistProfile, ClubProfile
from common.models import MethodsAdvert
from gigdate.models import DateAll
from common.files import get_file_path


class Trades(models.Model):
    """
        Заявки
    """
    create          = models.DateTimeField(auto_now_add=True)
    update          = models.DateTimeField(auto_now=True)
    create_deal     = models.DateTimeField(_(u'#Сделка'), blank=True, null=True, editable=False, help_text=_(u"Дата создания сделки"))

    TYPEDATE0, TYPEDATE1, TYPEDATE2,  = range(0, 3)
    # Не менять порядок и формат написания. Используется в имени ПДФ файла, в моделе ContractText
    TYPE_DATES = (
        (TYPEDATE0, _(u'gigdate')),
        (TYPEDATE1, _(u'barter')),
        (TYPEDATE2, _(u'tender')),
    )
    date_all        = models.ForeignKey(DateAll, blank=False, null=True, related_name="trade_date")
    type_date       = models.IntegerField(_(u'#Тип даты'), default=TYPEDATE0, choices=TYPE_DATES, blank=True)

    user_date       = models.ForeignKey(User, blank=False, related_name="trade_user_date")
    user_trade      = models.ForeignKey(User, blank=False, related_name="trade_user_trade")
    artist_date     = models.ForeignKey(ArtistProfile, blank=True, null=True, related_name="trade_artist_date")
    artist_trade    = models.ForeignKey(ArtistProfile, blank=True, null=True, related_name="trade_artist_trade")
    club_date       = models.ForeignKey(ClubProfile, blank=True, null=True, related_name="trade_club_date")
    club_trade      = models.ForeignKey(ClubProfile, blank=True, null=True, related_name="trade_club_trade")

    view_date_user  = models.BooleanField(_(u'#Просмотр владельцем даты'), default=True)
    view_trade_user = models.BooleanField(_(u'#Просмотр создателем заявки'), default=False)

    # Статус бронирования
    trade_trade_user1 = models.BooleanField(_(u'#Покупатель заявка 1'), default=True)
    trade_trade_user2 = models.BooleanField(_(u'#Покупатель заявка 2'), default=False)
    trade_date_user = models.BooleanField(_(u'#Продавец заявка 1'), default=False)

    STRADE0, STRADE1, STRADE2, STRADE3, STRADE4 = range(0, 5)
    STATUS_TRADES = (
        (STRADE0, _(u'Expectation')),
        (STRADE1, _(u'Request')),
        (STRADE2, _(u'Deals')),
        (STRADE3, _(u'Finish')),
        (STRADE4, _(u'Rejected')),
    )
    # status_date    = models.IntegerField(_(u'#Статус продавца'), default=STRADE0, choices=STATUS_TRADES)
    status_trade   = models.IntegerField(_(u'#Статус'), default=STRADE1, choices=STATUS_TRADES)

    # тип и сумма гонорара
    EVENT0, EVENT1, EVENT2 = range(0, 3)
    EVENT_TYPE = (
        (EVENT0, _(u'Public')),
        (EVENT1, _(u'Private')),
        (EVENT2, _(u'Other')),
    )
    FEEEVENT0, FEEEVENT1, FEEEVENT2 = range(0, 3)
    FEE_EVENT_TYPE = (
        (EVENT0, _(u'Contractual fee')),
        (EVENT1, _(u'Fixed fee')),
        (EVENT2, _(u'Another fee')),
    )
    CONTRAC0, CONTRAC1, CONTRAC2 = range(0, 3)
    CONTRACTS = (
        (CONTRAC0, _(u'Artists contract')),
        # (CONTRAC1, _(u'Customers contract')),
        (CONTRAC2, _(u'Gigcenter contract form')),
    )

    CON0 = (CONTRAC0, _(u'Artists contract')),
    # CON1 = (CONTRAC1, _(u'Customers contract')),
    CON2 = (CONTRAC2, _(u'Gigcenter contract form')),
    event_type        = models.IntegerField(_(u'#Тип мероприятия'), choices=EVENT_TYPE, blank=True, null=True)
    fee_event_type    = models.IntegerField(_(u'#Тип гонорара'), default=FEEEVENT0, choices=FEE_EVENT_TYPE, blank=True, null=True)
    mony_fix          = models.PositiveIntegerField(_(u"#Сумма гонорара"), blank=True, null=True)
    mony_edit         = models.BooleanField(_(u'#Запрет изменять гонорар'), default=False)
    contracts         = models.IntegerField(_(u'#Форма Договора'), choices=CONTRACTS, blank=True, null=True)

    b_event_type      = models.IntegerField(_(u'#Тип мероприятия бартер'), choices=EVENT_TYPE, blank=True, null=True)
    b_fee_event_type  = models.IntegerField(_(u'#Тип гонорара бартер'), default=FEEEVENT0, choices=FEE_EVENT_TYPE, blank=True, null=True)
    b_mony_fix        = models.PositiveIntegerField(_(u"#Сумма гонорара бартер"), blank=True, null=True)
    b_mony_edit         = models.BooleanField(_(u'#Запрет изменять гонорар'), default=False)
    b_contracts       = models.IntegerField(_(u'#Договор бартера'), choices=CONTRACTS, blank=True, null=True)

    def __unicode__(self):
        return u"%s: %s" % (self.pk, self.date_all)

    def get_absolute_url(self):
        return reverse('account:trade_view', args=[self.pk])

    def get_absolute_url_deals(self):
        return reverse('account:deals_view', args=[self.pk])

    class Meta:
        verbose_name = _('Trade')
        verbose_name_plural = _('Trades')
        ordering = ['-update']


class Replys(Trades):
    """
        Ответы
    """
    EVENT0, EVENT1, EVENT2 = range(0, 3)
    EVENT_TYPE = (
        (EVENT0, _(u'Public')),
        (EVENT1, _(u'Private')),
        (EVENT2, _(u'Other')),
    )
    ACT0, ACT1, ACT2, ACT3 = range(0, 4)
    TYPE_ACTIONS = (
        (ACT0, _(u'Commercial')),
        (ACT1, _(u'Noncommercial')),
        (ACT2, _(u'Charitable')),
        (ACT3, _(u'Other'))
    )

    VE0, VE1 = range(0, 2)
    VENUES = (
        (VE0, _(u'Outdoor')),
        (VE1, _(u'Indoor'))
    )

    RENT0, RENT1, RENT2 = range(0, 3)
    RENTS = (
        (RENT0, _(u'Owned')),
        (RENT1, _(u'Long-term rent')),
        (RENT2, _(u'One-time rent'))
    )
    queries1        = models.CharField(_(u'#Название мероприятия'), max_length=255, null=True, blank=True)
    queries2        = models.IntegerField(_(u'#Тип мероприятия'), choices=EVENT_TYPE, blank=True, null=True)
    queries3        = models.IntegerField(_(u'#Характер мероприятия'), choices=TYPE_ACTIONS, blank=True, null=True)
    queries4        = models.IntegerField(_(u'#Закрытое или открытой место выступления'), choices=VENUES, blank=True, null=True)
    queries5        = models.CharField(_(u'#Размер сцены'), max_length=255, null=True, blank=True)
    queries6        = models.CharField(_(u'#Наличие спонсоров'), max_length=255, null=True, blank=True)
    queries7        = models.CharField(_(u'#Цена входного билета'), max_length=255, null=True, blank=True)
    queries8        = models.CharField(_(u'#Места распространения билетов'), max_length=255, null=True, blank=True)
    queries9        = models.CharField(_(u'#Время начала и конца'), max_length=255, null=True, blank=True)
    queries10       = models.CharField(_(u'#Время выступления артиста'), max_length=255, null=True, blank=True)
    queries11       = models.CharField(_(u'#Время саундчека'), max_length=255, null=True, blank=True)
    queries12       = models.CharField(_(u'#Лайнап мероприятия'), max_length=255, null=True, blank=True)
    queries13       = models.ManyToManyField(MethodsAdvert, verbose_name=_(u'#Методы рекламы'), blank=True)
    queries14       = models.CharField(_(u'#Наличие танцоров/go go/перформанс'), max_length=255, null=True, blank=True)
    queries15       = models.CharField(_(u'#Возраст аудитории'), max_length=255, null=True, blank=True)
    queries16       = models.CharField(_(u'#Тип аудитории'), max_length=255, null=True, blank=True)
    queries17       = models.IntegerField(_(u'#Аренда площадки'), choices=RENTS, blank=True, null=True)
    queries18       = models.TextField(_(u'#Артисты и агенты, с которыми заказчик уже работал'), null=True, blank=True)
    queries19       = models.TextField(_(u'#Артисты, которые подтверждены на выступление в ближайшем будущем на мероприятиях заказчика'), null=True, blank=True)
    queries20       = models.TextField(_(u'#Артисты, которые уже выступали на площадке проведения мероприятия'), null=True, blank=True)
    queries21       = models.TextField(_(u'#Артисты, которые подтверждены на выступление в ближайшем будущем на площадке проведения мероприятия'), null=True, blank=True)

    def __unicode__(self):
        return u"Ответы: %s" % self.pk

    class Meta:
        verbose_name = _(u'Reply')
        verbose_name_plural = _(u'Replys')


class ContractText(models.Model):
    """
    Текст договора
    """
    contract_type       = models.IntegerField(_(u'Type date'), default=Trades.TYPEDATE0, choices=Trades.TYPE_DATES)
    text_before         = HTMLField(_(u'Before'), null=True, blank=True)
    text_after          = HTMLField(_(u'After'), null=True, blank=True)

    def __unicode__(self):
        return u"%s" % self.get_contract_type_display()

    class Meta:
        verbose_name = _(u'Contract text')
        verbose_name_plural = _(u'Contract texts')
        unique_together = ('contract_type',)


class Chat(models.Model):
    """
        Чат в заявке
    """
    trands          = models.ForeignKey(Trades, verbose_name=_(u'#Заявка'), blank=True, null=True, related_name="trades_chat")
    create          = models.DateTimeField(_(u"#Создано"), auto_now_add=True)
    comment         = models.TextField(_(u'#Комментарий'), null=True, blank=True)
    files           = models.FileField(_(u'#Файл'), upload_to=get_file_path, blank=True, null=True)
    trades_author     = models.BooleanField(_(u'Владелец заявки'), default=True)

    def __unicode__(self):
        return "%s: %s" % (self.trands.pk, self.trades_author)

    class Meta:
        verbose_name = _(u'Chat')
        verbose_name_plural = _(u'Chats')