#-*- coding:utf-8 -*-
from django.contrib import admin
from trade.models import Trades, Replys, ContractText, Chat


class ChatInline(admin.TabularInline):
    model = Chat
    extra = 1


class ReplysInline(admin.StackedInline):
    model = Replys
    extra = 1


class TradesAdmin(admin.ModelAdmin):
    list_display = ('pk', 'date_all', 'type_date', 'status_trade', 'create', 'update', 'create_deal')
    list_display_links = ('pk', 'date_all', 'type_date', 'status_trade')
    list_filter = ('type_date', 'status_trade')
    inlines = [ReplysInline, ChatInline]
    save_on_top = True
    readonly_fields = ('create', 'update', 'create_deal')
    # save_as = True


admin.site.register(Trades, TradesAdmin)
admin.site.register(Replys)
admin.site.register(ContractText)
admin.site.register(Chat)