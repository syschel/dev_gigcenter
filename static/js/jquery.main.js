$(document).ready(function(){
    initHorizontalScroll();             // инициализация скроллбаров
//    $('#action-select').selectmenu();
    $('.checkbox, .radio').checkBox();
    initPopups();                       // инициализация попапов
    initTabs();                         // инициализация табов
    initDropSelect ();                  // инициализация выбора дроп селекта
    initCalendarTable ();               // инициализацяи расставления дней недель в календаре
    initSlide();
    initAutocomplete();
    initNotes();
//    initDelsel();
});

function initCalendarTable () {
    $('.calendar-table-wrapper').each(function(){
        var _items = $('.bay',$(this));
        var _table = $('table',$(this));
        _items.css({
            'height':_table.height(),
            'position':'relative'
        });
        var _height = parseInt(_items.css('padding-top'));
        _items.find('li').each(function(i){
            if (i == 0) return;
            _height += _table.find('tr').eq(i-1).height();
            $(this).css({
                'position':'absolute',
                'top':_height
            });
        });
    });
}
function initDropSelect () {
    $('.valuta .drop a').click(function(){
        $(this).parents('.drop').parent().find('.opener').html($(this).html());
        $(this).parents('.drop').css('right','-9999px');
        return false;
    });
    $('.country .drop a').click(function(){
        $(this).parents('.drop').parent().find('.opener img').attr('src',$(this).find('img').attr('src'));
        $(this).parents('.drop').css('right','-9999px');
        return false;
    });
}
function initTabs () {
    $('.input-tab-wrapper').makeTabset({
        control_holder : '.tab-wrap',
        control: '.tab-wrap a',
        tab : '.tab',
        animate : false,
        animate_holder : '.tab-holder'
    });
//    $('.tab-set-holder .tab-set li').click(function(e){
//        $(this).parent().find('.active').removeClass('active');
//        $(this).addClass('active');
//        $(this).parents('.tab-set-holder').next().find('.tab').filter('.tab:visible').removeClass('active-tab').end().eq($(this).index()).addClass('active-tab');
//        e.preventDefault();
//    });
//    $('.inner-tab-holder li').click(function(e){
//        $(this).parent().find('.active').removeClass('active');
//        $(this).addClass('active');
//        $(this).closest('.tab').find('.inner-tab-wrapper').find('.inner-tab').filter('.inner-tab.active-inner-tab').removeClass('active-inner-tab').end().eq($(this).index()).addClass('active-inner-tab');
//        e.preventDefault();
//    });
    $('.main-tab-wrapper .inner-tab-set li').click(function(e){
        $(this).parent().find('.active').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.main-tab-wrapper').find('.tab-holder .tab').filter('.active-tab').removeClass('active-tab').end().eq($(this).index()).addClass('active-tab');
        e.preventDefault();
    });
}
function initHorizontalScroll(){
    var _scrollable = ".events-holder, calendar-table";
    if ($(_scrollable).length){
        var _w = 0;
        $(_scrollable).each(function(){
            _w = 0;
            $(this).find('.list li, .calendar-table table').each(function(){
            _w+=$(this).outerWidth(true);
        });
            $(this).find('.list, .calendar-table table').width(_w);
        })
    }
    if('jScrollPane' in $.fn){
        $('.events-holder, .calendar-table, .popup-news .list, .world-avtokomplit .list, .input-avtokomplit-holder .list').jScrollPane({horizontalDragMinWidth: 60, horizontalDragMaxWidth: 60, verticalDragMinHeight: 36, verticalDragMaxHeight: 36});
    }
    var api=$('.events-holder').data('jsp');
    $('.events-holder').bind(
                'jsp-scroll-x',
                function(event, scrollPositionX, isAtLeft, isAtRight)
                {
                    if(isAtLeft){
                        $(this).parent().find('.prev').addClass('end-list')
                    }else if(isAtRight){
                        $(this).parent().find('.next').addClass('end-list')
                    }else{
                        $(this).parent().find('.prev,.next').removeClass('end-list');
                    }
                }
            ).parent().find('.prev,.next').click(function(e){
                if($(this).is('.next')){
                    api.scrollByX(50);
                }else{
                    api.scrollByX(-50);
                }
                e.preventDefault();
            });
}
function initPopups () {
    //popupBg ();
    // открытие попапов
    $('.info .btn_more').click(function(){
        $('#about-the-company').fadeIn(200);
        if ($('#about-the-company .popup').height() < $(window).height()) {
            $('#about-the-company .popup').css({
                top: $(window).scrollTop() + $(window).height() / 2 - $('#about-the-company .popup').height() / 2 + 'px'
            });
        } else {
            $('#about-the-company .popup').css({
                top: $(window).scrollTop() + 20 + 'px'
            });
        }
        return false;
    });
    $('.country .opener, .valuta .opener').click(function(){
        if (parseInt($(this).parent().find('.drop').css('right')) != '0') {
            $(this).parent().find('.drop').css('right','0')
        } else {
            $(this).parent().find('.drop').css('right','-9999px')
        }
        return false;
    });
    // закрытие попапов
    $('.popup-holder .bg, .popup-holder .close').click(function(){
        if ($(this).parents('.popup-holder').attr('id') == 'add-date-popup') {
            $(this).parents('.popup-holder').css({visibility:'hidden'});
        } else {
            $(this).parents('.popup-holder').fadeOut(200);
        }
        return false;
    });
    $('.popup-notes .bg, .popup-notes .close').click(function(){
        $(this).parents('.popup-notes').fadeOut(200);
        active='';
        return false;
    });
    $('.popup-message .close').click(function(){
        $(this).parents('.popup-message').fadeOut(400);
        active='';
        return false;
    });
    $(document).click(function(event) {
        if ($(event.target).closest(".country .drop").length || $(event.target).closest(".valuta .drop").length) return;
        $(".drop").css('right','-9999px');
//        event.stopPropagation();
    });
    var notes_popup=$('.popup-notes:first'),
        active='';
    $('.events-holder .list li').click(function(e){
        showTip($(this).find('.popup-text').clone(),$(this).find('.visual'))
        e.preventDefault();
    });
    $('.calendar-table .list li .name').click(function(e){
        showTip($(this).closest('li').find('.popup-text').clone(),$(this));
        e.preventDefault();
    });
    var container=$('.w2'),
        bindResize=true;
    function showTip(content,alignBy){
        active=active instanceof jQuery ? active.get(0):active;
        if(active!==alignBy.get(0)){
            notes_popup.find('.popup-text').remove();
            notes_popup.find('>.popup-notes-holder').append(content);
            notes_popup.css('visibility','hidden').show();
            notes_popup.removeClass('align-right').addClass('align-center');
            var position={"top":alignBy.offset().top+alignBy.height()+14,"left":((alignBy.offset().left+parseInt(alignBy.css('padding-left')))-(notes_popup.find('.arrow').offset().left-notes_popup.offset().left))+(alignBy.width()-notes_popup.find('.arrow').width())/2};
            if(position.left<container.offset().left){
                notes_popup.removeClass('align-right').removeClass('align-center');
                position.left=((alignBy.offset().left+parseInt(alignBy.css('padding-left')))-(notes_popup.find('.arrow').offset().left-notes_popup.offset().left))+(alignBy.width()-notes_popup.find('.arrow').width())/2;
            }else if(position.left+notes_popup.width()>container.offset().left+container.width()){
                notes_popup.addClass('align-right');
                position.left=((alignBy.offset().left+parseInt(alignBy.css('padding-left')))-(notes_popup.find('.arrow').offset().left-notes_popup.offset().left))+(alignBy.width()-notes_popup.find('.arrow').width())/2;
            }
            notes_popup.removeAttr('style').hide().fadeIn(300).css({'top':position.top,'left':position.left});
            active=alignBy;
        }
        if(bindResize){
            bindResize=false;
            $(window).resize(function(){
                if(active){
                    position={"top":alignBy.offset().top+alignBy.height()+14,"left":((alignBy.offset().left+parseInt(alignBy.css('padding-left')))-(notes_popup.find('.arrow').offset().left-notes_popup.offset().left))+(alignBy.width()-notes_popup.find('.arrow').width())/2};
                    notes_popup.css({'top':position.top,'left':position.left});
                }
            });
        }
    }
    $('body').popup({
        "opener":".add-date",
        "popup_holder":"#add-date-popup",
        "popup":".my-date-wrap",
        "close_btn":".close"
    });
    $('.popup-message .close').click(function(e){
        $(this).parents('.popup-message').fadeOut(300);
        e.preventDefault();
    });
    $('.popup-news .opener').click(function(e){
        if(!$(this).parent().find('.list').is(':animated')){

            if($(this).parent().is('.open-news-list')){
                $(this).parent().find('.list').slideUp(400,function(){
                    $(this).parent().removeClass('open-news-list');
                });
            }else{
                $(this).parent().find('.list').css({'position':'relative','top':0,'left':0}).hide().slideDown(400,function(){
                    $(this).removeAttr('style').parent().addClass('open-news-list');
                });
            }
        }
        e.preventDefault();
    });
};
function popupBg () {
    $('.popup-holder').css('height','auto');
    if ($(document).height() < $(window).height()) {
        $('.popup-holder').css('height',$(window).height());
    } else {
        $('.popup-holder').css('height',$(document).height());
    }
};
$(window).resize(function(){
    //popupBg ();
});
function initSlide(){
//    $('.main-calendar-wrapper .opener-filter').click(function(e){
//        if(!$(this).parents('.main-calendar-wrapper').find('.filter-form').is(':animated')){
//            if($(this).text()=='показать фильтры'){
//                $(this).text('скрыть фильтры');
//            }else{
//                $(this).text('показать фильтры');
//            }
//
//            $(this).parents('.main-calendar-wrapper').find('.filter-form').slideToggle(400,function(){
//                $(this).parents('.main-calendar-wrapper').toggleClass('open-filter-form');
//            });
//        }
//        e.preventDefault();
//    });
//    $('.choose-style .opener').click(function(e){
//        if (!$(this).parents('.search-filter').find('.style-row').is(':animated')) {
//            $(this).parents('.search-filter').find('.style-row').slideToggle(400, function(){
//                $(this).parents('.search-filter').toggleClass('active-style-row');
//            });
//        }
//        e.preventDefault();
//    });
    $(window).load(function(){
        $('.help-menu>li>a').click(function(e){
        if($(this).parent().is('.active')){
            $(this).parent().find('ul').slideUp(400,function(){
                $(this).parent().removeClass('active')
            })
        }else{
            $(this).parent().find('>ul').slideDown(400,function(){
                $(this).parent().addClass('active')
            });
        }
        e.preventDefault();
    }).parent().find('ul>li>a').click(function(e){
        if($(this).parent().is('.active')){
            $(this).parent().find('>ul').slideUp(400,function(){
                $(this).parent().removeClass('active')
            });
        }else{
            $(this).parent().find('>ul').slideDown(400,function(){
                $(this).parent().addClass('active')
            });
        }
        if($(this).parent().find('>ul').size()){
            e.preventDefault();
        }
    });
    });
    /*
    // Выпадайка профиля
    $('.profile-form .side-profile-nav li').click(function(e){
        if(!$(this).find('>ul').is(':animated')){
            $(this).find('>ul').slideToggle(400,function(){
                $(this).parent().toggleClass('open-submenu');
            });
        }
        e.preventDefault();
    });
    */
}
function initAutocomplete(){
    $('.input-avtokomplit input').keyup(function(){
        if($(this).val().trim().length && this.defaultValue!=$(this).val()){
            $(this).parent().addClass('active-input-avtokomplit-holder');
        }else{
            $(this).parent().removeClass('active-input-avtokomplit-holder');
        }
    }).blur(function(){
        $(this).parent().removeClass('active-input-avtokomplit-holder');
    }).focus(function(){
        if ($(this).val().trim().length && this.defaultValue!=$(this).val()) {
            $(this).parent().addClass('active-input-avtokomplit-holder');
        }
    });
    $('.world-holder .opener').click(function(e){
        $(this).parent().toggleClass('active-world-avtokomplit')
        e.preventDefault();
    });
    $('body').click(function(e){
        if(!$(e.target).closest('.world-holder').size()){
            $('.world-holder').removeClass('active-world-avtokomplit');
        }
    });
}
function initNotes(){
    if ('datepicker' in $.fn && ($('.calendar').size() || $('.gray-calendar').size())) {
        $.datepicker._selectDate=function(){return false};
            $.datepicker.tooltipTemplate='<div class="tooltip-holder"><div class="tooltip"><div class="hold">'+
                                            '<p>{text}</p>'+
                                            '<a class="more" href="{link}">подробнее</a>'+
                                        '</div></div></div>';
            $.datepicker.checkTooltips=function(data){
                if(typeof data == 'object'){
                    if('dates' in data && 'text' in data && 'links' in data && 'dpDiv' in data){
                        var months=this.regional['ru'].monthNames,
                        calendarDays=data.dpDiv.find('.ui-datepicker-calendar td'),
                        currMonth=$('.ui-datepicker-month').text(),
                        currYear=$('.ui-datepicker-year').text(),
                        template=$.datepicker.tooltipTemplate,
                        prevMonthIndex='',
                        nextMonthIndex='',
                        prevDays=[],
                        nextDays=[];
                        $.each(months,function(i,name){
                            if(currMonth==name){
                                prevMonthIndex=i;
                                nextMonthIndex=i+2;
                                if(nextMonthIndex>12){
                                    nextMonthIndex=12;
                                }
                            }
                        });
                        data.dpDiv.parent().find('.heading .date').text($.datepicker.formatDate('dd MM yy',new Date()).toLowerCase());
                        prevDays=calendarDays.filter(":first").nextUntil('td:not(.ui-datepicker-other-month)').addClass('prev-month');
                        if(calendarDays.filter(":first").text()!='1'){
                            prevDays=prevDays.add(calendarDays.filter(":first").addClass('prev-month'));
                        }
                        nextDays=calendarDays.filter('.ui-datepicker-other-month:last').prevUntil('td:not(.ui-datepicker-other-month)').addClass('next-month');
                        if (calendarDays.filter(":first").text() == '1') {
                            nextDays = nextDays.add(calendarDays.filter('.ui-datepicker-other-month:last').addClass('next-month'));
                        }
                        $.each(data.dates,function(i){
                            var date=this.split('.');
                            if(currMonth==months[parseFloat(date[1])-1] && currYear==date[2]){
                                var days=calendarDays.filter(':not(.ui-datepicker-other-month)').find('a:contains('+(parseInt(date[0]))+')'),
                                    result=[];
                                days.each(function(){
                                    if((new RegExp('^'+parseInt(date[0])+'$','g')).test($(this).text())){
                                        result.push(this);
                                    }
                                });
                                $(result).addClass('ui-state-event').parent().removeAttr('onclick');
                            }
                            if(prevMonthIndex==parseFloat(date[1])){
                                var _days=prevDays.find('span:contains('+(parseInt(date[0]))+')'),
                                    result=[];
                                _days.each(function(){
                                    if((new RegExp('^'+parseInt(date[0])+'$','g')).test($(this).text())){
                                        result.push(this);
                                    }
                                });
                                $(result).addClass('ui-state-event').parent().removeAttr('onclick');
                            }
                            if(nextMonthIndex+1==parseFloat(date[1])){
                                var _days=nextDays.find('span:contains('+(parseInt(date[0]))+')'),
                                    result=[];
                                _days.each(function(){
                                    if((new RegExp('^'+parseInt(date[0])+'$','g')).test($(this).text())){
                                        result.push(this);
                                    }
                                });
                                $(result).addClass('ui-state-event').parent().removeAttr('onclick');
                            }
                        })
                    }else{
                        alert('Error: missing object property');
                    }
                }else{
                    alert('Error: invalid type of argument');
                }
            }
    }
}
jQuery.fn.makeTabset = function(o) {
    o = $.extend( {
        tab : '.tab',
        control : '.tabset a',
        control_holder : '.tabset',
        animate : false,
        animate_holder : '.holder'
    }, o || {});
    return this.each(function(){
        var _tabset = jQuery(this);
        var _control = jQuery(o.control,_tabset);
        var _control_holder = jQuery(o.control_holder,_tabset);

        _control_holder.find('a').each(function(){
            $(this).attr('href','tab'+($(this).index()+1));
        });
        var index = 1;
        _tabset.find(o.tab).each(function(){
            $(this).addClass('tab'+index);
            index += 1;
        });
        _control.bind('click',function(){
            if (!$(this).parent().is('.active')) {
                if (o.animate == true) {
                    _tabset.find(o.animate_holder).height(_tabset.find(o.animate_holder).height());
                    _tabset.find(o.tab + '.' + _tabset.find(o.control + ' .active').attr('href')).fadeOut(200);
                    _control_holder.find('.active').removeClass('active');
                    $(this).parent().addClass('active');
                    _tabset.find(o.tab + '.' + $(this).attr('href')).css('width', _tabset.find(o.animate_holder).width()).css('position', 'absolute').css('left', '-9999px').show();
                    var heightTo = _tabset.find(o.tab + '.' + _tabset.find(o.control + ' .active').attr('href')).height();
                    _tabset.find(o.animate_holder).animate({
                        height: heightTo
                    }, 500, function(){
                        _tabset.find(o.tab + '.' + _tabset.find(o.control + ' .active').attr('href')).css('width','100%').css('position', 'static').css('left', '0').hide();
                        _tabset.find(o.tab + '.' + _tabset.find(o.control + ' .active').attr('href')).fadeIn(200);
                        $(this).css('height','auto');
                    });
                }
                else {
                    _control_holder.find('.active').removeClass('active');
                    _tabset.find(o.tab).hide();
                    $(this).addClass('active');
                    _tabset.find(o.tab + '.' + $(this).attr('href')).show();
                }
            }
            return false;
        });
    });
};
$.fn.popup = function(o){
    var o = $.extend({
                "opener":".call-back a",
                "popup_holder":"#call-popup",
                "popup":".popup",
                "close_btn":".close",
                "close":function(){},
                "beforeOpen":function(){}
            },o);
    return this.each(function(){
        var container=$(this),
            opener=$(o.opener,container),
            popup_holder=$(o.popup_holder,container),
            popup=$(o.popup,popup_holder),
            close=$(o.close_btn,popup),
            bg=$('.bg',popup_holder);
            popup.css('margin',0);
            opener.unbind('click');
            opener.click(function(e){
                o.beforeOpen.apply(this,popup_holder);
                popup_holder.fadeIn(400);
                alignPopup();
                bgResize();
                e.preventDefault();
            });
        function alignPopup(){
                if((($(window).height() / 2) - (popup.outerHeight() / 2))+ $(window).scrollTop()<0){
                    popup.css({'top':0,'left': (($(window).width() - popup.outerWidth())/2) + $(window).scrollLeft()});
                    return false;
                }
                if(popup.is(':animated')){
                    popup.stop(true,true)
                }
                popup.css({
                    'top': (($(window).height()-popup.outerHeight())/2) + $(window).scrollTop(),
                    'left': (($(window).width() - popup.outerWidth())/2) + $(window).scrollLeft()
                });
        }
        function bgResize(){
            var _w=$(window).width(),
                _h=$(document).height();
            bg.css({"height":_h,"width":_w+$(window).scrollLeft()});
        }
        if(popup_holder.is(":visible")){
                bgResize();
                alignPopup();
        }
        $(window).resize(function(){
            if(popup_holder.is(":visible")){
                bgResize();
                alignPopup();
            }
        });
        close.add(bg).unbind('click');
        close.add(bg).click(function(e){
            var closeEl=this;
            popup_holder.fadeOut(400,function(){
                o.close.apply(closeEl,popup_holder);
            });
            e.preventDefault();
        });
        $('body').keydown(function(e){
            if(e.keyCode=='27'){
                popup_holder.fadeOut(400);
            }
        })
    });
}

//$(".noselect select").removeAttr("multiple");
//$("select#id_city").removeAttr("style");

$("#add_site").click(function(e){
    var inputs = $("div[rel=input_site] div.input-holder").html();  // Получили элемент
    $('<div class="input-holder">'+ inputs +'</div>').fadeIn('slow').appendTo("div.site_inputs"); // склонировали его вниз списка
    $("div[rel=input_site] div.input-holder input").last().removeAttr("value"); // удалили валуе

    return false;
});

$("#add_files").click(function(e){
    var inputs = $("div[rel=input_files] div.files_input").html();  // Получили элемент
    $('<div class="clear"></div>' + inputs).fadeIn('slow').appendTo("div.files_input_all"); // склонировали его вниз списка
    $("div[rel=input_files] div.input-holder input").last().removeAttr("value"); // удалили валуе

    return false;
});

// id_country
$('select#id_country').change(function() {

    var val = $(this).attr("value");

    if (val)
    {
        $.ajax({
            type: "GET",
            url: '/account/ajax_city/'+val+'/',
            success: function(msg){
                $('select#id_city').html(msg);
            },
            error: function(msg) {
                alert('error ajax');
            }
        });

    }
    return false;
});
$('select#id_city').on( "selectmenuchange", function( event, ui ) {a;ert('Change');} );

// id_country2
$('select#id_country_job, select#id_second_country').change(function() {

    var val = $(this).attr("value");
    if (val)
    {
        $('select#id_city_job, select#id_second_city').html("<option>-Search city-</option>");

        $.ajax({
            type: "GET",
            url: '/account/ajax_city/'+val+'/',
            success: function(msg){
//                alert(msg);
                $('select#id_city_job, select#id_second_city').html(msg);
            },
            error: function() {
                alert('error ajax');
            }
        });

    }
    return false;
});

// id_country2
$('select#id_profile, select#id_artist').change(function() {
    var val = $(this).attr("value");
    if (val)
    {
        $.ajax({
            type: "GET",
            url: '/account/get_artist/'+val+'/',
            dataType: 'json',
            //data: {'userid': val},
            success: function(msg){
                $("#avatar_01").attr("src", msg['avatar']);
                $("#flag_01").css({"background-position":"-"+msg['country']['width']+"px -"+msg['country']['height']+"px"});
                $("#fromlocation_01").html(msg['country']['title']+", "+msg['city']);
                $("#genres_01").html(msg['genres']);
            },
            error: function() {

            }
        });

    }
    return false;
});
// id_club
$('select#id_club').change(function() {
    var val = $(this).attr("value");
    if (val)
    {
        $.ajax({
            type: "GET",
            url: '/account/get_club/'+val+'/',
            dataType: 'json',
            success: function(msg){
                $("#avatar_01").attr("src", msg['avatar']);
                $("#flag_01").css({"background-position":"-"+msg['country']['width']+"px -"+msg['country']['height']+"px"});
                $("#fromlocation_01").html(msg['country']['title']+", "+msg['city']);
                $("#genres_01").html(msg['genres']);
            },
            error: function() {

            }
        });

    }
    return false;
});


$('#main .contemt_artist .artist_menu a').click(function(e){
    var rel = $(this).attr("rel");
    $('#main .contemt_artist .artist_menu a').removeClass("active");
    $(this).addClass("active");


    $('#main .contemt_artist .userinfo').addClass("hide");
    $('#main .contemt_artist .userinfo').removeClass("nohide");
    $('#user_' + rel).removeClass("hide");
    $('#user_' + rel).addClass("nohide");

    return false;
});


//Open add menu
$('#add_gigdate span').click(function(){
    $("#add_gigdate #add_gigdate_list").toggle();
});

$('#add_gigdate #add_gigdate_list a').click(function(){
    var atr = $(this).attr("href");
    if(atr=='#'){
        $('#add_gigdate #add_gigdate_list').hide();
        return false;
    }
});



$('.add_barters select[name=fee]').change(function () {
    var atr = $(this).attr("value");
    if(atr=='2'){
        $(".add_barters #fixmoney").hide();
        $(".add_barters #contractualmoney").show();
    } else {
        $(".add_barters #contractualmoney").hide();
        $(".add_barters #fixmoney").show();
    }
});
$('.add_barters select[name=scheme_variants]').change(function () {
    var atr = $(this).attr("value");
    if(atr=='2'){
        $(".add_barters #payment").show();
    } else {
        $(".add_barters #payment").hide();
    }
});

/* Свернуть-развернуть */
$(".text_block-action").click(function(){
    var t=$(".text_block-overlay");
    t.toggle(),t.is(":visible")?($(".text_block").css("height","170px"),$(".readmore").text("Читать далее")):($(".text_block").css("height","auto"),$(".readmore").text("hiden"))
});


$('li.geograph_hide_next').click(function(){
//    $("li.geograph_hide").toggle();
    var elem = $("li.geograph_hide");
    elem.toggle();
//    elem.is(":visible")?($("li.geograph_hide_next").css("background-image","/static/images/arrow02_1.png")):($("li.geograph_hide_next").css("background-image","/static/arrow02.png"))
    elem.is(":visible")?($("li.geograph_hide_next").css("background-image","170px")):($("li.geograph_hide_next").css("background-image","30px"))
});


//$('a#texh_support').click(function(){
//    $(".forms_tickets").show();
//    return false;
//});

/// Появление и исчезание сообщения
$('.popup-message').delay(1000).fadeIn(1000);
$('.popup-message').delay(2000).fadeOut(4000);


$('a#add_favorites').click(function(){
    var rev = $(this).attr("rev");
    var rel = $(this).attr("rel");
    $('#message_text').html('')
    $.ajax({
        url: '/info/add_favorites/'+rel+'_'+rev+'/',
        dataType: 'json',
        type: 'GET',
        success: function(data) {
            if (data['status'] == 'ok') {
                if (data['message'] == 'new_user'){$('#message_text').html('Favorites added');}else{$('#message_text').html('Already added favorites');}
            }
            else if (data['status'] == 'error') {
                $('#message_text').html('An error occured!');
            }
            $('#favorit').fadeIn(1000);
            $('#favorit').delay(2000).fadeOut(4000);
        },
            error: function() {
                $('#message_text').html('An error occured!');
                $('#favorit').fadeIn(1000);
                $('#favorit').delay(2000).fadeOut(4000);
            }
    });
    return false;
});


$('a#add_bookmarks').click(function(){
    var rev = $(this).attr("rev");
    var rel = $(this).attr("rel");
    $('#message_text').html('')
    $.ajax({
        url: '/info/add_bookmarks/'+rel+'_'+rev+'/',
        dataType: 'json',
        type: 'GET',
        success: function(data) {
            if (data['status'] == 'ok') {
                if (data['message'] == 'new_user'){$('#message_text').html('Bookmarks added');}else{$('#message_text').html('Already added bookmarks');}
            }
            else if (data['status'] == 'error') {
                $('#message_text').html('An error occured!');
            }
            $('#favorit').fadeIn(1000);
            $('#favorit').delay(2000).fadeOut(4000);
        },
            error: function() {
                $('#message_text').html('An error occured!');
                $('#favorit').fadeIn(1000);
                $('#favorit').delay(2000).fadeOut(4000);
            }
    });
    return false;
});
