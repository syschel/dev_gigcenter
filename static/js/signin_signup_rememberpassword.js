$(document).ready(function(){
    jQuery.validator.addMethod("not_placeholder_email", function(value, element) {
        return element.value != 'youremail@me.com';
    }, "Please specify the correct email(not youremail@me.com)");
    jQuery.validator.addMethod("email_less75", function(value, element) {
        return element.value.length <= 75;
    }, "Email must be less then 75 symbols");
    jQuery.validator.addMethod("pass_less128", function(value, element) {
        return element.value.length <= 128;
    }, "Password must be less then 128 symbols");
////////////////////////////////////////////////////////////////////////////////
//  signUp form
////////////////////////////////////////////////////////////////////////////////
    $('#register_by_email').validate({
        rules: {
            email: {
                required: true,
                email: true,
                not_placeholder_email: true,
                email_less75: true
            }
        },
        errorPlacement: function(error, element) {
            if ($(element).attr('name') != 'agree'){
                $('#error_place').html('');
                $('#error_place').append($(error));
            }
            else {
                $('#error_place').html('');
                $('#error_place').hide();
            }
        },
        highlight : function (element) {
            $('#error_place').show();
        },
        unhighlight : function (element) {
            $('#error_place').hide();
        },
        messages: {
            email: {
                required: "This email is required."
                //email: "Проверьте правильность адреса электронной почты",
                //not_placeholder_email: "Пожалуйста, введите адрес (не youremail@me.com)",
                //email_less75: "Email должен быть меньше 75 символов."
            }
        },

        submitHandler: function(form) {
            var agent = false;
            var artist = false;
            var customer = false;
            if ($('#checkbox_artist').hasClass('ui-checkbox-checked')) {artist=true;}
            if ($('#checkbox_agent').hasClass('ui-checkbox-checked'))  {agent=true;}
            if ($('#checkbox_customer').hasClass('ui-checkbox-checked')) {customer=true;}
            $.ajax({
                url: '/register/',
                dataType: 'json',
                type: 'POST',
                data: {
                    'email': form.email.value,
                    'password': form.password.value,
                    'csrfmiddlewaretoken': form.csrfmiddlewaretoken.value,
                },
                success: function(data) {
                    if (data['status'] == 'ok') {
                        $('#error_place').html(data['message']);
                        $('#error_place').show();
                    }
                    else if (data['status'] == 'error') {
                        $('#error_place').html(data['message']);
                        $('#error_place').show();
                    }
                }
            });
            return false;
        }
    });
////////////////////////////////////////////////////////////////////////////////
//  signIn form
////////////////////////////////////////////////////////////////////////////////
    $('#signin_by_email').validate({
        rules: {
            email: {
                required: true,
                email: true,
                not_placeholder_email: true,
                email_less75: true
            },
            password: {
                required: true,
                pass_less128: true
            }
        },
        errorPlacement: function(error, element) {
            if ($(element).attr('name') != 'agree'){
                $('#error_place_signin').html('');
                $('#error_place_signin').append($(error));
            }
        },
        highlight : function (element) {
            $('#error_place_signin').show();
        },
        unhighlight : function (element) {
            $('#error_place_signin').hide();
        },
        messages: {
            email: {
                required: "This email is required."
                //not_placeholder_email: "Пожалуйста, введите адрес (не youremail@me.com)",
                //email_less75: "Email должен быть меньше 75 символов."
            },
            password: {
                required: "This password is required."
                //pass_less128: "Пароль слишком длинный"
            }
        },
        submitHandler: function(form) {

            $.ajax({
                url: '/signin/',
                dataType: 'json',
                type: 'POST',
                data: {
                    'login': form.email.value,
                    'password': form.password.value,
                    'csrfmiddlewaretoken': form.csrfmiddlewaretoken.value
                },
                success: function(data) {
                    if (data['status'] == 'ok') {
                        window.location.href = window.location.href;
                    }
                    else if (data['status'] == 'error') {
                        $('#error_place_signin').html(data['message']);
                        $('#error_place_signin').show();
                    }
                }
            });
            return false;
        }
    });



////////////////////////////////////////////////////////////////////////////////
//  mails form
////////////////////////////////////////////////////////////////////////////////
    $('a.mailbox_open_modal').click(function(){
        $("#mailbox_popup").show();
        return false;
    });

    $('#mailbox_send_ajax').validate({
        rules: {
            title: {
                required: true
            },
            text: {
                required: true
            }
        },
        submitHandler: function(form) {

            $.ajax({
                url: '/account/mailbox_ajax/',
                dataType: 'json',
                type: 'POST',
                data: {
                    'title': form.title.value,
                    'text': form.text.value,
                    'to': form.to.value,
                    'csrfmiddlewaretoken': form.csrfmiddlewaretoken.value
                },
                success: function(data) {
                    if (data['status'] == 'ok') {
                        $('#error_place_mailbox').show();
                        $('#mailbox_popup').delay(500).fadeOut(1500);
                    }
                    else if (data['status'] == 'error') {
                        //alert('Временно не работает отправка #275MB');
                    }
                }
            });
        }
    });

});


