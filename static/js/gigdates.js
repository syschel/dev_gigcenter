$('.datepicker').each(function() {
        $(this).datepicker({
            'beforeShow':function(){
                setTimeout(function(){
                    $('#ui-datepicker-div').addClass('blue-calendar').find('.ui-datepicker-header').addClass('top').end().find('>*').wrapAll($('<div class="blue-calendar-holder"></div>'));
                },10);
            },
            'onChangeMonthYear':function(){
                setTimeout(function(){
                    $('#ui-datepicker-div').addClass('blue-calendar').find('.ui-datepicker-header').addClass('top').end().find('>*').wrapAll($('<div class="blue-calendar-holder"></div>'));
                },10);
            },
            'showOtherMonths':true,
            /*'changeMonth': true,*/
            /*'changeYear': true,*/
            'minDate':0,
            'dateFormat':'mm/dd/yy'
        });
        $(this).parent().find('.open-datepicker').click(function(e){
            if(!$(this).parent().find('input').is('.focus')){
                $(this).parent().find('input').focus();
            }
            return false;
        });
    });

$('input[name=secondary_data]').change(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('tr.secondary_data').show(); }else{
        $('tr.secondary_data').hide();
        $("#id_second_country [value='']").attr("selected", "selected");
    }

});
/* Выделяем все страны */
$('#all_country').click(function(){
    var check = $(this).attr('checked');
     if(check=='checked'){
         $('select#id_geography_queries option').prop('selected', 'selected');
     }else{
         $('select#id_geography_queries option').prop('selected', '');
     }
});
$('select#id_country option').click(function(){
        $('#all_country').attr('checked', false);
});
/* дата */
$('#id_contract_site').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#form_dogovor').show(); }else{$('#form_dogovor').hide();}
});

/* типы мероприятия - публичное */
$('.row input[name=type_event_public]').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#type_event_public').show(); }else{$('#type_event_public').hide();}
});
$('#type_event_public input[name=event_public]').change(function(){
    var val = $(this).attr('value');
    if (val==0){
        $(".public_fix_gon_mon").hide();
        $(".public_other_gon_mon").hide();
    }
    if (val==1){
        $(".public_fix_gon_mon").show();
        $(".public_other_gon_mon").hide();
    }
    if (val==2){
        $(".public_fix_gon_mon").hide();
        $(".public_other_gon_mon").show();
    }

});

/* типы мероприятия - Приватное */
$('.row input[name=type_event_private]').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#type_event_private').show(); }else{$('#type_event_private').hide();}
});
$('#type_event_private input[name=event_private]').change(function(){
    var val = $(this).attr('value');
    if (val==0){
        $(".private_fix_gon_mon").hide();
        $(".private_other_gon_mon").hide();
    }
    if (val==1){
        $(".private_fix_gon_mon").show();
        $(".private_other_gon_mon").hide();
    }
    if (val==2){
        $(".private_fix_gon_mon").hide();
        $(".private_other_gon_mon").show();
    }

});


/* типы мероприятия - Презентация */
$('.row input[name=type_event_presentation]').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#type_event_presentation').show(); }else{$('#type_event_presentation').hide();}
});
$('#type_event_presentation input[name=event_presentation]').change(function(){
    var val = $(this).attr('value');
    if (val==0){
        $(".presentation_fix_gon_mon").hide();
        $(".presentation_other_gon_mon").hide();
    }
    if (val==1){
        $(".presentation_fix_gon_mon").show();
        $(".presentation_other_gon_mon").hide();
    }
    if (val==2){
        $(".presentation_fix_gon_mon").hide();
        $(".presentation_other_gon_mon").show();
    }

});

$('input#id_all_dopinfo').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){
        $('#id_all_dopinfo input:checkbox:enabled').prop('checked', true);
        $('#id_all_dopinfo span.ui-checkbox').addClass('ui-checkbox-state-checked ui-checkbox-checked');
    }else{
        $('#id_all_dopinfo input:checkbox:enabled').prop('checked', false);
        $('#id_all_dopinfo span.ui-checkbox').removeClass('ui-checkbox-state-checked ui-checkbox-checked');
    }
});
/* Добавим всем чекбоксам класс стиля */
$('input[type=checkbox]').addClass("checkbox");
$('input[type=radio]').addClass("radio");


/* ###############################
        бартер
############################### */

$('#id_barter_contract_site').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#form2_dogovor').show(); }else{$('#form2_dogovor').hide();}
});

/* типы мероприятия - публичное */
$('.row input[name=barter_type_event_public]').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#gigbarter_type_event_public').show(); }else{$('#gigbarter_type_event_public').hide();}
});
$('#gigbarter_type_event_public input[name=barter_event_public]').change(function(){
    var val = $(this).attr('value');
    if (val==0){
        $(".gigbarter_public_fix_gon_mon").hide();
        $(".gigbarter_public_other_gon_mon").hide();
    }
    if (val==1){
        $(".gigbarter_public_fix_gon_mon").show();
        $(".gigbarter_public_other_gon_mon").hide();
    }
    if (val==2){
        $(".gigbarter_public_fix_gon_mon").hide();
        $(".gigbarter_public_other_gon_mon").show();
    }

});

/* типы мероприятия - Приватное */
$('.row input[name=barter_type_event_private]').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#gigbarter_type_event_private').show(); }else{$('#gigbarter_type_event_private').hide();}
});
$('#gigbarter_type_event_private input[name=barter_event_private]').change(function(){
    var val = $(this).attr('value');
    if (val==0){
        $(".gigbarter_private_fix_gon_mon").hide();
        $(".gigbarter_private_other_gon_mon").hide();
    }
    if (val==1){
        $(".gigbarter_private_fix_gon_mon").show();
        $(".gigbarter_private_other_gon_mon").hide();
    }
    if (val==2){
        $(".gigbarter_private_fix_gon_mon").hide();
        $(".gigbarter_private_other_gon_mon").show();
    }

});


/* типы мероприятия - Презентация */
$('.row input[name=barter_type_event_presentation]').click(function(){
    var check = $(this).attr('checked');
    if(check=='checked'){ $('#gigbarter_type_event_presentation').show(); }else{$('#gigbarter_type_event_presentation').hide();}
});
$('#gigbarter_type_event_presentation input[name=barter_event_presentation]').change(function(){
    var val = $(this).attr('value');
    if (val==0){
        $(".gigbarter_presentation_fix_gon_mon").hide();
        $(".gigbarter_presentation_other_gon_mon").hide();
    }
    if (val==1){
        $(".gigbarter_presentation_fix_gon_mon").show();
        $(".gigbarter_presentation_other_gon_mon").hide();
    }
    if (val==2){
        $(".gigbarter_presentation_fix_gon_mon").hide();
        $(".gigbarter_presentation_other_gon_mon").show();
    }

});
$('#add_club input').click(function(){
    var check = $(this).attr('checked');
    $('#new_club').toggle();
});

// Открываем или скрываем поле ввода гонорара в бронировании гигдаты
$('select#id_event_type').change(function() {
    var val = $(this).attr("value");
    if (val)
    {
        if(val == '0' && $("#id_mony_fix").hasClass("event_public")){$('div#mony_fix').show();}
        else if(val == '1' && $("#id_mony_fix").hasClass("event_private")){$('div#mony_fix').show();}
        else if(val == '2' && $("#id_mony_fix").hasClass("event_presentation")){$('div#mony_fix').show();}
        else{$('div#mony_fix').hide();}
    }
    else{ $('div#mony_fix').hide(); }
    return false;
});
// Открываем или скрываем поле ввода гонорара в бронировании гигдаты
$('select#id_b_event_type').change(function() {
    var val = $(this).attr("value");
    if (val)
    {
        if(val == '0' && $("#id_b_mony_fix").hasClass("barter_event_public")){$('div#b_mony_fix').show();}
        else if(val == '1' && $("#id_b_mony_fix").hasClass("barter_event_private")){$('div#b_mony_fix').show();}
        else if(val == '2' && $("#id_b_mony_fix").hasClass("barter_event_presentation")){$('div#b_mony_fix').show();}
        else{$('div#b_mony_fix').hide();}
    }
    else{ $('div#b_mony_fix').hide(); }
    return false;

});
$('#.line a.hide1').click(function(){
    $('#hidden_data').toggle('show');
    return false;
});


$('#.line a.hide2').click(function(){
    $('#hidden_data2').toggle('show');
    return false;
});