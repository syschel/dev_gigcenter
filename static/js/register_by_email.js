$(document).ready(function(){
    $('#register_by_email').validate({

        rules: {
            email: {
                required: function(element) {
                    return element.value != 'youremail@me.com2';
                },
                email: true
            }
        },
        messages: {
            email: {
                required: "Введите адрес электронной почты",
                email: "Проверьте правильность адреса электронной почты"
            }
        },

        submitHandler: function(form) {

            $.ajax({
                url: '/account/register/',
                dataType: 'json',
                type: 'POST',
                data: {
                    'email': form.email.value,
                    'message': form.message.value,
                    'csrfmiddlewaretoken': form.csrfmiddlewaretoken.value
                },
                success: function(data) {
                    if (data['status'] == 'ok') {
                        $('#error_place').html(data['message']);
                        $('#error_place').show();
                    }
                    else if (data['status'] == 'error') {
                        $('#error_place').html(data['message']);
                        $('#error_place').show();
                    }
                },
                error: function() {
                    alert('Error');
                }
            });
        }
    });
});
