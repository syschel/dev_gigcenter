//Снятие выбора с пункта "все" при снятии выбора с 1 из пунктов
function unselectOneCB(object, classname, show_id) {
	if (!$('.'+$(object).attr('class')).is(":checked")) {
		$('.'+classname).removeAttr('checked')
		$('.'+classname).css("opacity","1")
		if (show_id != null) {
			$('#'+show_id).hide()
		}
	} else if (!$('.'+$(object).attr('class')).is(":not(:checked)")) {
		$('.'+classname).attr("checked", "checked")
		$('.'+classname).css("opacity","1")
		if (show_id != null) {
			$('#'+show_id).show()
		}
	} else if (object.checked) {
		$('.'+classname).attr("checked", "checked")
		$('.'+classname).css("opacity","0.5")
	} else if (!object.checked) {
		$('.'+classname).css("opacity","0.5")
	}
}

//Отметить\снять отметку всех записей с определенный классом
function selectAllCB(object, classname, show_id) {
	if (object.checked) {
		$('.'+classname).not(':checked').attr("checked", "checked")
		$(object).css("opacity","1")
		if (show_id != null) {
			$('#'+show_id).show()
		}
	} else {
		$('.'+classname).removeAttr('checked')
		$(object).css("opacity","1")
		if (show_id != null) {
			$('#'+show_id).hide()
		}
	}
}

$('a.send_message').click(function(event) {
    var href_obj = $(this).attr('href')
    load_popup_window(href_obj)
    event.preventDefault();
})

function load_popup_window(url){
    $.get(url+'?ajax=1', {}, function(data) {
		if (data != '') {
			$('#popup_inner').html(data);
            $('#id_form').submit(function( event ){
                form_message_send(url)
                event.preventDefault();
            })
            
		} else {
			alert('Не удалось открыть окно отправки сообщения, попробуйте позже!');
		}
        $('#input-message-popup').show();
		return true;
	});

}

function form_message_send(url) {
    var qString = $("#id_form").formSerialize();
    $.post(url+'?ajax=1', qString, function(data){
        if (data == '') {
            $('#input-message-popup').hide();
        } else {
            $('#popup_inner').html(data);
            $('#id_form').submit(function( event ){
                form_message_send(url)
                event.preventDefault();
            })
        }
    })
    return false;
}

