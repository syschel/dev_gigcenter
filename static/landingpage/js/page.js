Page = {
	sliderTimer: 5000,
	act: false,
	index: 0,
	
	init: $(function() {
		$('header .bg').eq(0).show("slow");
		
		if($('#slider .slide').eq(0).find('.read_more').height() > 126) {
			$('#slider .slide').eq(0).find('.read_more').addClass('active');
		}
		
		$('#slider .left').click(function() {
			$(this).find('.txt').toggleClass('active');
			if($(this).find('.read_more').hasClass('active')) {
				$(this).find('.read_more.active').removeClass('active').addClass('not_active');
			} else {
				$(this).find('.read_more.not_active').removeClass('not_active').addClass('active');
			}
		});
		
		$('nav li a').click(function() {
			if($(this).attr('href') == '') {
				if($('#toggle').hasClass('active')) {
					$('nav ul').removeClass('active');
					$('#toggle').removeClass('active');
					$('body').css('overflow', 'auto');
				}
				$("html, body").animate({ scrollTop: $('section').eq($(this).parent('li').index()).offset().top-69}, 500);
				return false;
			}
		});
		
		$('#slider .slide').width($('#slider').width()-40);
		$(window).resize(function() {
			$('#slider .slide').width($('#slider').width()-40);
		});
		
		$('#toggle').click(function() {
			$('nav ul').toggleClass('active');
			if($('nav ul').hasClass('active')) {
				$('#toggle').addClass('active');
				$('body').css('overflow', 'hidden');
			} else {
				$('#toggle').removeClass('active');
				$('body').css('overflow', 'auto');
			}
		});
		
		$('header .change .item').click(function() {
			if($(this).hasClass('active'))
				return false;
			
			i = $(this).index();
			Page.nextSlide(i);
		});
		
		timer = setInterval(Page.nextSlide,Page.sliderTimer);
		
		$('#about button').click(function() {
			$('#about button').removeClass('selected');
			$(this).addClass('selected');
			$('#about .item').removeClass('active').eq($(this).index()).addClass('active');
			$('#about .head').removeClass('active').eq($(this).index()).addClass('active');
		});
		
		$('#slider2 button').click(function() {
			$('#slider2 button').removeClass('selected');
			$(this).addClass('selected');
			$('#slider2 .slide').removeClass('active').eq($(this).index()).addClass('active');
			$('#slider2 .text p').removeClass('active').eq($(this).index()).addClass('active');
		});
		
		$('#slider ul li').click(function() {
			if($(this).hasClass('active'))
				return false;
			
			index = $(this).index();
			$('#slider .left .txt').removeClass('active');
			$('#slider .slide').removeClass('active').eq(index).addClass('active');			
			$('#slider ul li').removeClass('active');
			$(this).addClass('active');
			if($('#slider .slide').eq(index).find('.read_more').height() > 126) {
				$('#slider .slide').eq(index).find('.read_more').addClass('active');
			}
		});
	}),
	
	nextSlide: function(i) {
		if(Page.act)
			return false;
		
		Page.act = true;
		clearInterval(timer);
		i = ($('header .change .item').eq(0).hasClass('active')) ? 1 : 0;
		$('header .change .item').removeClass('active').eq(i).addClass('active');
		
		if(i!=1) {
			$('header .bg').eq(1).fadeOut(500);
			$('header .slide').eq(1).fadeOut(500);
			$('header .bg').eq(0).fadeIn(500, function() {
				$('header .slide').eq(0).fadeIn(200);
				Page.act = false;
			});
		} else {
			$('header .bg').eq(0).fadeOut(500);
			$('header .slide').eq(0).fadeOut(500);
			$('header .bg').eq(1).fadeIn(500, function() {
				$('header .slide').eq(1).fadeIn(200);
				Page.act = false;
			});
		}
		
		timer = setInterval(Page.nextSlide,Page.sliderTimer);
	}
}